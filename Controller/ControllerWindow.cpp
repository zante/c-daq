/* 
 * File:   Window.cpp
 * Author: lemelino
 * 
 * Created on November 15, 2014, 10:49 PM
 */

#include <glog/logging.h>
#include "ControllerWindow.h"
#include "gtkmm/main.h"
#include "gtkmm/label.h"

ControllerWindow::ControllerWindow(std::shared_ptr<ControllerCommunicator> 
        controllerCommunicator) : 
  _cStartButton("Start"),
  _cPauseButton("Pause"),
  _cStopButton("Stop"),
  _cStateLabel("Not recording")
  
{
    // Stores a copy of the shared pointer that is received in a local shared
    // pointer.  This is going to be used in order to transmit signal upon
    // pressing the buttons.
    
    _cControllerCommunicator = controllerCommunicator;
    
    // Sets the title of the window.
    set_title("Acquisition Controller");
    
    // Sets the border width of the window
    set_border_width(10);
    
    // Adds the box that will contain all of the buttons.
    add(_cBox);
    
    // Packs the buttons into the box, which lives inside the window.
    _cBox.pack_start(_cStartButton);
    _cBox.pack_start(_cStopButton);
    _cBox.pack_start(_cPauseButton);
    _cBox.pack_start(_cStateLabel);
    
    // Sends a START signal when the start button is clicked.
    _cStartButton.signal_clicked().connect(sigc::mem_fun(*this, 
            &ControllerWindow::on_startButton_clicked));
    
    // Sends a STOP signal when the start button is clicked.
    _cStopButton.signal_clicked().connect(sigc::mem_fun(*this, 
            &ControllerWindow::on_stopButton_clicked));
    
    // Sends a PAUSE signal when the start button is clicked.
    _cPauseButton.signal_clicked().connect(sigc::mem_fun(*this, 
            &ControllerWindow::on_pauseButton_clicked));

    // Creates the label to indicate in which mode we currently are.
    _cStateLabel.override_color(Gdk::RGBA("Red"), 
                        Gtk::STATE_FLAG_NORMAL);
    _cStateLabel.set_label("Not recording");
    _cControllerState = STOPPED;
    
    //Displays the buttons.
    _cStartButton.show();
    _cStopButton.show();
    _cPauseButton.show();
    _cStateLabel.show();
    
    // Displays the box.
    _cBox.show();

}

ControllerWindow::~ControllerWindow() {
    LOG(INFO) << "Close button has been clicked!  Quitting!";
    _cControllerCommunicator->sendExitMessage();
}

void ControllerWindow::on_startButton_clicked()
{
    if(changeState(RECORDING)) {
        LOG(INFO) << "Start button clicked.";
        _cControllerCommunicator->sendStartMessage();
    }
}

void ControllerWindow::on_stopButton_clicked()
{
    if(changeState(STOPPED)) {
        LOG(INFO) << "Stop button clicked.";
        _cControllerCommunicator->sendStopMessage();
    }
}

void ControllerWindow::on_pauseButton_clicked()
{
    if(changeState(PAUSED)) {
        LOG(INFO) << "Pause button clicked.";
        _cControllerCommunicator->sendPauseMessage();
    }
}

bool ControllerWindow::canChangeState(ControllerState newState) {
    switch(_cControllerState) {
        case STOPPED:
            return (newState == RECORDING) ? true : false;
            break;
        case PAUSED:
            return (newState == STOPPED || newState == RECORDING) ? true : false;
            break;
        case RECORDING:
            return (newState == STOPPED || newState == PAUSED) ? true : false;
            break;
        default:
            throw std::string("Wrong controller state!");
            break;
    }
}

bool ControllerWindow::changeState(ControllerState newState) {
    
    if(canChangeState(newState)) {
        switch(newState) {
            case STOPPED:
                _cStateLabel.override_color(Gdk::RGBA("Red"), 
                        Gtk::STATE_FLAG_NORMAL);
                _cStateLabel.set_label("Not recording");
                _cControllerState = STOPPED;
                break;
            case PAUSED:
                _cStateLabel.override_color(Gdk::RGBA("Blue"), 
                        Gtk::STATE_FLAG_NORMAL);
                _cStateLabel.set_label("Paused");
                _cControllerState = PAUSED;
                break;
            case RECORDING:
                _cStateLabel.override_color(Gdk::RGBA("Green"), 
                        Gtk::STATE_FLAG_NORMAL);
                _cStateLabel.set_label("Recording");
                _cControllerState = RECORDING;
                break;
            default:
                throw std::string("Wrong controller state!");
                break;
        } 
        
        return true;
        
    } else {
        LOG(INFO) << "Cannot change from state " << _cControllerState <<
                " to state " << newState << ".";
        
        return false;
    }
}