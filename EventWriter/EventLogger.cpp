/* 
 * File:   EventLogger.cpp
 * Author: lemelino
 * 
 * Created on January 13, 2015, 4:29 PM
 */

#include "EventLogger.h"
#include "../Common/ConfigurationManager.h"

#include <glog/logging.h>


EventLogger::EventLogger(const std::string& fileToOpen) {
    
    std::string fullPathToFile = ConfigurationManager::getInstance()->
            getString("RECORDING_DIRECTORY") + fileToOpen;

    outFileStream.open(fullPathToFile.c_str(), std::ofstream::out);
    
    if(outFileStream.is_open() && outFileStream.good()) {
        LOG(INFO) << "Event file '" << fileToOpen << "' opened successfully.";
    }
    else {
        LOG(FATAL) << "Event file '" << fileToOpen << "' could not be opened "
                "for writing.  Exiting.";
    }
}

EventLogger::~EventLogger() {
    
    outFileStream.flush();
    
    if(!outFileStream.good()) {
        LOG(FATAL) << "Problem finalizing output file stream.";
    }
    
    outFileStream.close();
    
    if(outFileStream.is_open()) {
        LOG(FATAL) << "Problem closing output file stream.";
    }
}

