/* 
 * File:   EventModel.h
 * Author: lemelino
 *
 * Created on January 21, 2015, 2:49 PM
 */

#ifndef EVENTMODEL_H
#define	EVENTMODEL_H

#include <list>
#include "Event.h"

class EventModel {
public:
    EventModel(NanosecondsTime maxNanosToLive);
    EventModel(const EventModel& orig);
    virtual ~EventModel();
    
    void addEvent(Event &event);
    void purgeOldEvents(NanosecondsTime currentTime);
    std::list<Event>::const_iterator getIterator();
    std::list<Event>::const_iterator getEnd();
    
private:

    std::list<Event> _eventsList;
    NanosecondsTime _maxNanosToLive;
    
};

#endif	/* EVENTMODEL_H */

