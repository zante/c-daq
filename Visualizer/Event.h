/* 
 * File:   Event.h
 * Author: lemelino
 *
 * Created on January 21, 2015, 2:50 PM
 */

#ifndef EVENT_H
#define	EVENT_H

#include "../Common/CommonDef.h"

class Event {
public:
    Event(int eventCode, NanosecondsTime timestamp);
    Event(const Event& orig);
    virtual ~Event();
    
    NanosecondsTime getTimestamp() const;
    int getEventCode() const;
private:

    NanosecondsTime _timestamp;
    int _eventCode;
};

#endif	/* EVENT_H */

