/* 
 * File:   DataProcessor.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:31 PM
 */

#include <glog/logging.h>
#include <iostream>

#include "../Common/Timestamper.h"
#include "../Common/ConfigurationManager.h"
#include "../Common/CommonDef.h"
#include "EventProcessor.h"
#include "MatlabEventLogger.h"

EventProcessor::EventProcessor(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress) :
_cSubscriberSocket(cZmqContext, ZMQ_SUB)
{    
    setupEventPipe(cDataSocketAddress);
    
    _eventLogger = NULL;
    _isRecording = false;
}

EventProcessor::~EventProcessor() {
    stop();
}

void EventProcessor::process() {
    
    int receivedEventCode = -1;
    
    int sizeOfMessage = sizeof(int);

    // Allocates message with given size.
    zmq::message_t msg(sizeOfMessage);
    
    // Tries to receive a message.
    bool hasMessage = _cSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    
    // While there are messages to process...
    while(hasMessage) {
        
        // Copy the contents of the message to a buffer.
        memcpy((void*)(&receivedEventCode), msg.data(), sizeOfMessage);
        
        LOG(INFO) << "Received: <" << receivedEventCode << ">";
        
        // Process the received contents.
        processSingleEvent(receivedEventCode);
        
        // Tries to receive a new message.
        hasMessage = _cSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    }
    
    // Error testing.
    if( !hasMessage && errno == EAGAIN) {
        //Ignore, there was nothing to receive.
    }
    else if(!hasMessage){
        //We have encountered an error.
        throw std::string("Problem receiving events.");
    }
}

void EventProcessor::setupEventPipe(const std::string& dataSocketAddress) {
    
    _cSubscriberSocket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    
    // Connect to the Publisher.    
    _cSubscriberSocket.connect(dataSocketAddress.c_str());
}

bool EventProcessor::isFilesystemOK(std::string fileNameToTest) {

    // Gets the recordings directory.
    std::string recordingDir = ConfigurationManager::getInstance()->
            getString("RECORDING_DIRECTORY");
    
    // Verifies that the recording directory exists.
    if(access(recordingDir.c_str(), F_OK ) != 0) {
        LOG(FATAL) << "Error: recording directory '" << recordingDir << "' does "
                "not exist.  It will need to be created manually.";
        return false;
    } 
    
    //Verifies that the directory is writeable.
    if(access(recordingDir.c_str(), R_OK | W_OK) != 0) {
        LOG(FATAL) << "Error: recording directory '" << recordingDir << "' is "
                "not writeable.  Permissions will need to be adjusted.";
        return false;
    } 
    
    // Checks to see if file name already exists.
    if( access( fileNameToTest.c_str(), F_OK ) != -1 ) {
        
        //File already exists!
        return false;
        
    } else {
        
        // File does not already exist, OK to create!
        return true;
    }
}

void EventProcessor::processSingleEvent(int eventCode) {

    // We only process the event if we are recording and the data 
    // accumulator exists!
    if(_isRecording) {
        
        //TODO: remove!
        NanosecondsTime nanoTimestamp = _timestamper.getCurrentNanoTime();
        LOG(INFO) << "Timestamp: " << nanoTimestamp;

        _eventLogger->writeEvent(eventCode, nanoTimestamp);
    }
}

void EventProcessor::handleStateChange(StateMessage state) {
    switch(state) {
        case START:
            start();
            break;
        case PAUSE:
            _isRecording = false;
            break;
        case STOP:
            stop();
            break;
        case EXIT:
            stop();
            break;
        default:
            LOG(FATAL) << "Impossible state.";
    }
}

std::string EventProcessor::generateFileName() {
    
    // Obtains the local time (now)
    time_t t = time(NULL);
    struct tm *tmptr = localtime(&t);
    
    // Verify that the time was obtained correctly.
    if (tmptr == NULL)
    {
       LOG(FATAL) << "localtime() failed.";
    }
    
    // Seeds the RNG.
    srand(t);
    
    // Allocates objects to build name
    std::stringstream generatedFilename;
    const int MAX_LENGTH = 80;
    char buffer[MAX_LENGTH];
    
    // Builds name
    strftime(buffer, MAX_LENGTH, FILE_NAME_FORMAT, tmptr);
    generatedFilename << "events_" << buffer << "_" << rand() % 100 << ".log";
    std::string completeGeneratedFilename = generatedFilename.str();
    
    LOG(INFO) << "Generated file name for events: '" << 
            completeGeneratedFilename << "'.";
    
    return completeGeneratedFilename;
}

void EventProcessor::finalizeResults() {
    
    if(_eventLogger != NULL) {
        delete _eventLogger;
        _eventLogger = NULL;
    }
}

void EventProcessor::start() {
    
    _timestamper.startTimer();
    
    // Since with are beginning to record, we set the flag to true.
    _isRecording = true;

    // We generate a name for the new file.
    generatedFileName = generateFileName();
    
    // We check that the filesystem is able to receive the new file:
    // Enough space on disk, file does not already exist, directory is writeable...
    if(!isFilesystemOK(generatedFileName)) {
        LOG(FATAL) << "Error with filesystem: file with same name probably "
                "exists."; 
    }
    
    // If the data writer does not already exist, we create it.
    //TODO: Reenable!
    if(_eventLogger == NULL) {
        _eventLogger = new MatlabEventLogger(generatedFileName);
    }
}

void EventProcessor::pause() {
    // We stop recording, but do not delete the data accumulator, as we will
    // continue recording later.
    _isRecording = false;
}

void EventProcessor::stop() {
    // We set the recording flag to false to stop handling received frames.
    _isRecording = false;
    
    //We save the recording session.
    finalizeResults();
}
