/* 
 * File:   DataProcessor.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:31 PM
 */

#include "DataAcquirer.h"
#include <glog/logging.h>
#include <vector>
#include <string>
#include <sstream>
#include "../Common/ConfigurationManager.h"

DataAcquirer::DataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress) :
_cDataPublisherSocket(cZmqContext, ZMQ_PUB)
{
    // Bind to the data socket.
    
    int rc = _cDataPublisherSocket.bind(cDataSocketAddress.c_str());
    if(rc == EADDRINUSE) {
        LOG(INFO) << "Cannot bind to address '" << cDataSocketAddress <<
                "'...";
    }
    
    //Configures the data acquisition.
    
    setupDataAcquisition();
}

DataAcquirer::~DataAcquirer() {
}

void DataAcquirer::setupDataAcquisition() {
    
    /**
     * # Number of channels / electrodes to use.  
     * Make sure that they are are sequentially plugged into the
     * # amplifier beforehand, starting from '0'.
     */
    numberOfChannels = ConfigurationManager::getInstance()->
        getInt("NUMBER_OF_CHANNELS");
    
    LOG(INFO) << "Number of DAQ channels: " << numberOfChannels;
    
    /**
     * # Measures the maximal/minimal values that should be measured. (in Volts)
     */
    const int minVoltage = ConfigurationManager::getInstance()->
        getInt("MIN_VOLTAGE");
    const int maxVoltage = ConfigurationManager::getInstance()->
        getInt("MAX_VOLTAGE");
    
    LOG(INFO) << "Max/Min voltages: " << minVoltage << "/" << 
            maxVoltage << " Volts";
    
    /**
     * # Sampling rate used.
     */
    samplesPerFrame = ConfigurationManager::getInstance()->
        getInt("SAMPLES_PER_FRAME");
    LOG(INFO) << "Samples per frame: " << samplesPerFrame;
    
    /**
     * Samples per second.
     */
    const int samplesPerSecond = ConfigurationManager::getInstance()->
            getInt("SAMPLES_PER_SECOND");
    LOG(INFO) << "Samples per second: " << samplesPerSecond; 
    
    /**
     * # If more than one National Instruments devices are plugged in, 
     * you might need to specify 'Dev2' or 'Dev3'
     */
    const std::string deviceToUse = ConfigurationManager::getInstance()->
            getString("NI_DEVICE_TO_USE");
    
    LOG(INFO) << "Device in use: " << samplesPerFrame;
    
    /* We generate the event acquisition channels string.  
     * This is required, as we do not know how many channels
     * will be used beforehand. */
    
    // The last iteration should not have an additional ','.
    int i = 0;
    std::ostringstream configString;
    for(; i < numberOfChannels - 1; i++) {
        configString << deviceToUse << "/ai" << i << ','; 
    }
    configString << deviceToUse << "/ai" << numberOfChannels - 1;
    std::string listOfChannels = configString.str();
    
    LOG(INFO) << "NiDAQ Configuration string: " << listOfChannels;
    
    // Creates DAQmx task for the acquisition process.
    DAQmxBaseCreateTask("", &taskHandle);

    // We configure the Voltage acquisition channels. We merge many 
    // hardware channels in one software "channel".
    DAQmxBaseCreateAIVoltageChan(taskHandle, listOfChannels.c_str(), 
            "", DAQmx_Val_RSE, minVoltage, maxVoltage,
            DAQmx_Val_Volts, NULL);

    // We configure the sampling clock
    DAQmxBaseCfgSampClkTiming(taskHandle, NULL, samplesPerSecond, 
            DAQmx_Val_Rising, DAQmx_Val_ContSamps, samplesPerFrame);
    
    // Start the task! Avast!
    DAQmxBaseStartTask(taskHandle);
}

void DataAcquirer::process() {
    
    // Retrieves the transmission buffer from the function.
    double* transmissionBuffer = prepareTransmissionBuffer();
    
    // Fills the transmission buffer with the data retrieved from the 
    // acquisition card.
    fillTransmissionBuffer(transmissionBuffer);
    
    // Sends the acquired data.
    sendDataFrame(transmissionBuffer);
    
    // deletes the buffer to evade memory leaks.
    delete[] transmissionBuffer;
}

double* DataAcquirer::prepareTransmissionBuffer() {
    
    // Creates a buffer able to store data for a single frame.
    //TODO: This might need to be optimized.
    double* transmissionBuffer = new double[numberOfChannels * samplesPerFrame];

    // Checks if the buffer was correctly assigned (Not NULL).
    if(transmissionBuffer == 0) {
        LOG(FATAL) << "Could not allocate memory for transmission buffer.";
    }
    
    // Sets the buffer to all zeros.
    // TODO: Remove this it it takes too much CPU.
    memset(transmissionBuffer, 0.0, numberOfChannels * samplesPerFrame);
    
    // Returns prepared buffer.
    return transmissionBuffer;
}

void DataAcquirer::fillTransmissionBuffer(double* transmissionBuffer) {
    
    // Here:
    // We read <samplesPerFrame> per channel
    // Wait indefinitely to obtain all of the samples (We do not want unfinished
    // frames)
    // We group values per channel. (thus, 10 values for channel 1, 
    // 10 values for channel 2))
    // We send data to the transmissionBuffer
    // We indicate that the size of the transmissionBuffer is of 
    // <samplesPerFrame * numberOfChannels>.
    // We retrieve the number of retrieved samples into <numberOfSamplesRead>.
    // Pass null, as it is a reserved field (unused).
    
    long numberOfSamplesRead = 0;
    DAQmxBaseReadAnalogF64(taskHandle, samplesPerFrame, -1, 
            DAQmx_Val_GroupByChannel, transmissionBuffer, 
            samplesPerFrame * numberOfChannels, &numberOfSamplesRead, NULL);
    
    // Verifies that we read as many samples as we needed to.
    // TODO: Can be optimized out someday.
    if(numberOfSamplesRead != samplesPerFrame) {
        LOG(FATAL) << "Wrong number of samples read: <" << numberOfSamplesRead <<
                "> read, but expected to have read <" << samplesPerFrame << ">.";
    }
}

void DataAcquirer::sendDataFrame(double* transmissionBuffer) {
    LOG(INFO) << "Sending data frame...";
    
    // Creates the message, reserving enough space for the whole buffer that 
    // was received.
    
    int bufferSizeInBytes = sizeof(double) * samplesPerFrame * numberOfChannels;
    
    zmq::message_t msg((std::size_t) bufferSizeInBytes);
    
    // Copies the integer inside the state message.
    
    memcpy(msg.data(), transmissionBuffer, (std::size_t) bufferSizeInBytes);
    
    // Send away!
    
    _cDataPublisherSocket.send(msg);
}



