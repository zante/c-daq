/* 
 * File:   VisualizerWindow.cpp
 * Author: lemelino
 * 
 * Created on January 15, 2015, 3:14 PM
 */

#include <glog/logging.h>
#include <sstream>

#include "VisualizerWindow.h"
#include "VisualizationCompressor.h"
#include "../Common/ConfigurationManager.h"


VisualizerWindow::VisualizerWindow() {
    //The window we'll be rendering to
    gWindow = NULL;

    //The surface contained by the window
    gScreenSurface = NULL;
    
    renderer = NULL;
    
    _dataModel = NULL;
    _eventModel = NULL;
    
    _redrawRequired = true;
    
    loadElectrodesNames();
    
    // Tests that there are as many named electrodes as there are channels.
    _nbChannels = ConfigurationManager::getInstance()
            ->getInt("NUMBER_OF_CHANNELS");
    
    if(electrodes.size() != _nbChannels) {
        LOG(FATAL) << "There are " << electrodes.size() << " named electrodes" <<
                " while there are " << _nbChannels << " declared channels.";
    }
    
    _maxVoltValue = ConfigurationManager::getInstance()->getDouble("MAX_VOLTAGE");
    _minVoltValue = ConfigurationManager::getInstance()->getDouble("MIN_VOLTAGE");
    
    int screenWidth = ConfigurationManager::getInstance()->getInt("SCREEN_WIDTH");
    int screenHeight = ConfigurationManager::getInstance()->getInt("SCREEN_HEIGHT");
    
    int samplesPerSecond = ConfigurationManager::getInstance()->getInt("SAMPLES_PER_SECOND");
    
    _nbChannels = ConfigurationManager::getInstance()->getInt("NUMBER_OF_CHANNELS");
    _visualizationPointsCompressionRatio = ConfigurationManager::getInstance()
            ->getInt("VISUALIZATION_POINTS_COMPRESSION_RATIO");
    int samplesPerFrame = ConfigurationManager::getInstance()
            ->getInt("SAMPLES_PER_FRAME");

    // Make sure that we can adequately compress all the points in a single frame.
    // Thus, we check that if by dividing the number of elements in a channel
    // by the compression ratio, we have no remainder.
    
    if(samplesPerFrame % _visualizationPointsCompressionRatio != 0) {
        LOG(FATAL) << "It would currently be impossible to compress the whole data "
                "array for the visualization module.  Please choose a compression "
                "ratio and a number of samples per frame per channel that would "
                "allow this. Currently: SPFPC: " << samplesPerFrame << ". points " <<
                "compression ratio: " << _visualizationPointsCompressionRatio << 
                ".";
    }
    
    initializeText();
    initializeGraphics(screenWidth, screenHeight);
    
    drawElectrodesNames(_nbChannels);
    
    int maxNbOfElementsPerVisibleChannel = 
            rectangleScreenSurface.w - electrodesRect.w;
    
    LOG(INFO) << "Maximum number of points visible per channel: " 
            << maxNbOfElementsPerVisibleChannel;
    
    _dataModel = new DataModel(_nbChannels, maxNbOfElementsPerVisibleChannel);
    
    //Calculates how much "time" is rendered on the screen.
    double millisecondsDisplayed = 
            ( (double)MILLISECONDS_PER_SECOND / (double) samplesPerSecond  ) *
            ( (double) maxNbOfElementsPerVisibleChannel ) * 
            (double) _visualizationPointsCompressionRatio;
    
    LOG(INFO) << "Milliseconds displayed on screen: " 
            << millisecondsDisplayed;
    
    _nanoSecondsDisplayed = (long) millisecondsDisplayed * 
            MICROSECONDS_PER_MILLISECOND * NANOSECONDS_PER_MICROSECOND;
    
    _timestamper.startTimer();
    
    _eventModel = new EventModel(_nanoSecondsDisplayed);
}

VisualizerWindow::~VisualizerWindow() {
    
    delete _dataModel;
    delete _eventModel;

    //Destroy window
    SDL_DestroyWindow( gWindow );
    SDL_DestroyRenderer(renderer);
    
    gWindow = NULL;
    renderer = NULL;
    
    //Quit SDL subsystems
    TTF_Quit();
    SDL_Quit();
}

void VisualizerWindow::initializeGraphics(int screenWidth, int screenHeight) {
    
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        LOG(FATAL) << "SDL could not initialize! SDL_Error: " 
                << SDL_GetError();
    }

    //Create window
    gWindow = SDL_CreateWindow( "Visualizer", SDL_WINDOWPOS_UNDEFINED, 
            SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight, 
            SDL_WINDOW_SHOWN );

    if( gWindow == NULL )
    {
        LOG(FATAL) << "Window could not be created! SDL_Error: " 
                << SDL_GetError();
    }

    //Get window surface
    gScreenSurface = SDL_GetWindowSurface( gWindow );
    if( gScreenSurface == NULL )
    {
        LOG(FATAL) << "Surface could not be created! SDL_Error: " 
                << SDL_GetError();
    }
    
    // Creates the renderer.
    renderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_SOFTWARE);
    if(!renderer) {
        LOG(FATAL) << "Could not create the renderer: " << SDL_GetError();
    }
    
    // Obtains and stores the clipping rectangle.
    SDL_GetClipRect(gScreenSurface, &rectangleScreenSurface);
}

void VisualizerWindow::initializeText() {
    
    // Initialize the TTF stuff
    if( TTF_Init() < 0 ) {
        LOG(FATAL) << "TTF could not initialize : " << TTF_GetError();
    }
    
    std::string theFont = 
            ConfigurationManager::getInstance()->getString("FONT");
    int theFontSize = 
            ConfigurationManager::getInstance()->getInt("FONT_SIZE");
    
    ttfFont = TTF_OpenFont(theFont.c_str(), theFontSize);
    if(!ttfFont) {
        LOG(FATAL) << "Couldn't load the font '" << theFont << "' with size " <<
                theFontSize;
    }
}

void VisualizerWindow::loadElectrodesNames() {
    electrodes = ConfigurationManager::getInstance()->getStringArray("ELECTRODES");
}

void VisualizerWindow::drawElectrodesNames(int nbChannels) {
    
    getDimensionsOfLargestElectrodeName(&_oneElectrodeRect);
    
    int requiredWidth = _oneElectrodeRect.w + 
            ELECTRODES_LEFT_OFFSET + ELECTRODES_RIGHT_OFFSET;
    
    if(requiredWidth > rectangleScreenSurface.w) {
        LOG(FATAL) << "There is not enough horizontal space to even display the"
                " electrodes.  Please modify the width of the displayed screen"
                " to at least " << requiredWidth << " pixels. Current"
                "width: " << rectangleScreenSurface.w << "pixels.";
    }
    
    int requiredHeight = _oneElectrodeRect.h * nbChannels + EVENTS_NAME_SPACE;
    if(requiredHeight > rectangleScreenSurface.h) {
        LOG(FATAL) << "There is not enough vertical space to even display all the"
                " electrodes.  Please modify the height of the displayed screen"
                " to at least " << requiredHeight << " pixels.";
    }

    // Let's reserve the horizontal space...
    
    electrodesRect.x = 0;
    electrodesRect.y = 0;
    
    electrodesRect.w = _oneElectrodeRect.w + ELECTRODES_LEFT_OFFSET + 
            ELECTRODES_RIGHT_OFFSET;
    
    // The vertical space is the same as the height of the screen!
    
    electrodesRect.h = rectangleScreenSurface.h;
    
    // Calculate the space for each electrode name.
    
    _verticalElectrodeSpace = rectangleScreenSurface.h / nbChannels;
    
    // Mark the limit...
    
    if ( SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE) != 0 ) {
        LOG(FATAL) << SDL_GetError();
    }
    
    if(SDL_RenderClear( renderer ) != 0) {
        LOG(FATAL) << SDL_GetError();
    }
    
    if ( SDL_SetRenderDrawColor(renderer, 120, 120, 120, SDL_ALPHA_OPAQUE) != 0 ) {
        LOG(FATAL) << SDL_GetError();
    }
    
    if ( SDL_RenderDrawLine(renderer,
                       electrodesRect.x + electrodesRect.w,
                       electrodesRect.y,
                       electrodesRect.x + electrodesRect.w,
                       electrodesRect.h) != 0 )
    {
        LOG(FATAL) << SDL_GetError();
    }
    
    // Write the electrodes names.
    
    SDL_Color greyColor = {120, 120, 120}; 
    
    SDL_Rect destRect;
    destRect.x = ELECTRODES_LEFT_OFFSET;
    destRect.y = ELECTRODES_TOP_OFFSET;
    
    for(unsigned int i = 0; i < electrodes.size(); i++) {
        
        SDL_Surface *textSurface = TTF_RenderText_Solid(ttfFont, 
                electrodes[i].c_str(), greyColor);

        if(!textSurface) {
            LOG(INFO) << "Couldnt't display electrode name.";
            
        } else {
            
            SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, 
                textSurface);
            
            if(!texture) {
                LOG(FATAL) << "Could not convert texture.";
            }
            
            if (SDL_QueryTexture(texture, NULL, NULL, 
                    &(destRect.w), &(destRect.h)) != 0) {
                
                LOG(FATAL) << SDL_GetError();
            }
            
            if( SDL_RenderCopy(renderer,
                   texture,
                   NULL,
                   &destRect) != 0 ) 
            {
                LOG(FATAL) << SDL_GetError();
            }
            
            SDL_DestroyTexture(texture);
            SDL_FreeSurface(textSurface);
            
            destRect.y += _verticalElectrodeSpace;
        }
    }
    
    SDL_RenderPresent(renderer);
}

void VisualizerWindow::getDimensionsOfLargestElectrodeName(SDL_Rect* largestDims) {
    
    // Calculates layout
    largestDims->h = -1;
    largestDims->w = -1;
    
    std::string largestName;
    
    // With the following, we find the largest required dimensions to display
    // an electrode's name.  This will help us with the layout.
    
    for(unsigned int i = 0; i < electrodes.size(); i++) {
        SDL_Rect tempRect;
        TTF_SizeText(ttfFont, electrodes[i].c_str(), &(tempRect.w), &(tempRect.h));
        
        if(tempRect.w > largestDims->w) {
            largestDims->w = tempRect.w;
            largestName = electrodes[i];
        }
        
        if(tempRect.h > largestDims->h) {
            largestDims->h = tempRect.h;
        }      
    }
    
    LOG(INFO) << "Largest electrode name: '" << largestName << "' : (" 
            << largestDims->w << " x " << largestDims->h << ")";
}

void VisualizerWindow::work() {

    SDL_Event e;
    
    //Handle events on queue
    while( SDL_PollEvent( &e ) != 0 )
    {
        // Do nothing with the events.
    }
    
    if(_redrawRequired) {
        drawEverything();
        _redrawRequired = false;
    }
}

void VisualizerWindow::addDataFrame(double* dataChunk, int nbElementsInFrame) {
    
    //  With the number of elements in a frame and the number of channels in a
    // frame, we get the number of elements in each channel.
    
    unsigned int nbElementsPerChannel = nbElementsInFrame / _nbChannels;
    
    int newNumberOfChannels = 0;
    int newNumberOfElementsPerChannel = 0;
    
    // We compress the points, giving us the points that will need to be written.
    
    float* newConvertedDataChunk = VisualizationCompressor::compress(dataChunk, _nbChannels, 
            nbElementsPerChannel, _visualizationPointsCompressionRatio,
            &newNumberOfChannels, 
            &newNumberOfElementsPerChannel);
    
    _dataModel->addDataFrame(newConvertedDataChunk, newNumberOfChannels, 
            newNumberOfElementsPerChannel);
    
    delete[] newConvertedDataChunk;
    
    _redrawRequired = true;
}

void VisualizerWindow::addEvent(int eventCode) {

    NanosecondsTime nanoTimestamp = _timestamper.getCurrentNanoTime();
    Event event(eventCode, nanoTimestamp);
    
    _eventModel->purgeOldEvents(nanoTimestamp);
    _eventModel->addEvent(event);
    
    _redrawRequired = true;
}

void VisualizerWindow::drawEverything() {
    
    // Delimit a zone for drawing.
    
    SDL_Rect drawingZone;
    drawingZone.x = electrodesRect.x + electrodesRect.w;
    drawingZone.y = electrodesRect.y;
    drawingZone.h = rectangleScreenSurface.h - EVENTS_NAME_SPACE;
    drawingZone.w = _dataModel->getSize();
    
    // Gets the electrode's y position and the interstice between those.
    
    int topElectrodePosition = ELECTRODES_TOP_OFFSET + 
            _oneElectrodeRect.h / 2 + drawingZone.y;
    int electrodeYPos = topElectrodePosition;
    
    // Draw all of the lines and data points.
    
    if( SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE) != 0) {
        LOG(FATAL) << SDL_GetError();
    }
    
    // Fills the rendering space with white.
    
    SDL_RenderFillRect(renderer, &drawingZone);
    
    // Within this loop, we draw the electrodes lines and the electrodes themselves.
    
    for(unsigned int i = 0; i < _nbChannels; i++) {
        
        if( SDL_SetRenderDrawColor(renderer, 120, 120, 120, SDL_ALPHA_OPAQUE) != 0) {
            LOG(FATAL) << SDL_GetError();
        }
        
        // Renders the horizontal lines for the electrode.
        
        if( SDL_RenderDrawLine(renderer,
                           drawingZone.x,
                           electrodeYPos,
                           rectangleScreenSurface.w,
                           electrodeYPos) != 0) {
            
            LOG(FATAL) << SDL_GetError();
        }
        
        // Render the electrodes.
        
        if( SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE) != 0) {
            LOG(FATAL) << SDL_GetError();
        }
        
        // Convert points to positions.

        SDL_Point* pointsForChannel = new SDL_Point[_dataModel->getSize()];
        
        std::deque<float>::const_reverse_iterator it = 
                _dataModel->getIteratorForChannel(i);
        
        std::deque<float>::const_reverse_iterator itEnd = 
                _dataModel->getIteratorEndForChannel(i);
        
        int counter = 0;
        
        // Draw the signal for the current channel.
        
        while(it != itEnd) {
            pointsForChannel[counter].x = drawingZone.x + counter;
            pointsForChannel[counter].y = electrodeYPos - 
                    (*it / (double)_maxVoltValue) * 
                    _verticalElectrodeSpace * 3.0;
            
            counter++;
            it++;
        }
        
        // Render the points for a single channel.
        
        SDL_RenderDrawLines(renderer, pointsForChannel, _dataModel->getSize());
        
        electrodeYPos += _verticalElectrodeSpace;
        
        delete[] pointsForChannel;
    }
    
    if( SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE) != 0) {
        LOG(FATAL) << SDL_GetError();
    }
    
    // Clear the event's names
    SDL_Rect eventsNamesRect;
    eventsNamesRect.x = 0;
    eventsNamesRect.y = rectangleScreenSurface.h - EVENTS_NAME_SPACE;
    eventsNamesRect.w = rectangleScreenSurface.w - eventsNamesRect.x;
    eventsNamesRect.h = EVENTS_NAME_SPACE;
    
    SDL_RenderFillRect(renderer, &eventsNamesRect);
    
    if( SDL_SetRenderDrawColor(renderer, 60, 60, 60, SDL_ALPHA_OPAQUE) != 0) {
        LOG(FATAL) << SDL_GetError();
    }
    
    // Draw the events.
    auto eventIt = _eventModel->getIterator();
    auto eventItEnd = _eventModel->getEnd();
    
    SDL_Color darkGrey = {60, 60, 60};
    
    SDL_Rect textRect;
    
    while(eventIt != eventItEnd) {
        // Determine X position where we need to write.
        
        NanosecondsTime currentTime = _timestamper.getCurrentNanoTime();
        NanosecondsTime eventTime = eventIt->getTimestamp();
        
        double percentageOfScreenPosition = ( (double)(currentTime - eventTime) 
                / (double)_nanoSecondsDisplayed );
        
        int xPos =  percentageOfScreenPosition * 
                (double)(rectangleScreenSurface.w - electrodesRect.w) + drawingZone.x;
        
        // Draws the vertical line in green.
        if( SDL_RenderDrawLine(renderer,
                xPos,
                drawingZone.y,
                xPos,
                drawingZone.h) != 0) {
            
            LOG(FATAL) << SDL_GetError();
        }
        
        // Builds the text
        int eventCode = eventIt->getEventCode();
        std::stringstream ss;
        
        //double milliTime = (double)eventTime / (double)(NANOSECONDS_PER_SECOND);
        ss << eventCode /*<< "(" << milliTime << " s)"*/;
        
        // Creates the texture
        
        SDL_Surface *textSurface = TTF_RenderText_Solid(ttfFont, 
                ss.str().c_str(), darkGrey);

        if(!textSurface) {
            LOG(INFO) << "Couldnt't display electrode name.";
            
        } else {
            
            SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, 
                textSurface);
            
            if(!texture) {
                LOG(FATAL) << "Could not convert texture.";
            }
            
            if (SDL_QueryTexture(texture, NULL, NULL, 
                    &(textRect.w), &(textRect.h)) != 0) {
                
                LOG(FATAL) << SDL_GetError();
            }
            
            textRect.x = xPos - (textRect.w / 2);
            textRect.y = eventsNamesRect.y;
            
            if( SDL_RenderCopy(renderer,
                   texture,
                   NULL,
                   &textRect) != 0 ) 
            {
                LOG(FATAL) << SDL_GetError();
            }
            
            SDL_DestroyTexture(texture);
            SDL_FreeSurface(textSurface);
        }
        
        eventIt++;
    }
    
    
    // Draw the points
    
    SDL_RenderPresent(renderer);
}
