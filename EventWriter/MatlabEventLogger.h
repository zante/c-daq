/* 
 * File:   MatlabEventLogger.h
 * Author: lemelino
 *
 * Created on January 13, 2015, 4:43 PM
 */

#ifndef MATLABEVENTLOGGER_H
#define	MATLABEVENTLOGGER_H

#include "EventLogger.h"

class MatlabEventLogger : public EventLogger {
public:
    MatlabEventLogger(const std::string& fileToOpen);
    MatlabEventLogger(const MatlabEventLogger& orig);
    virtual ~MatlabEventLogger();
    
    void writeEvent(int eventCode, 
            NanosecondsTime timestampInNanoseconds);

private:
};

#endif	/* MATLABEVENTLOGGER_H */

