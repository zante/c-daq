/* 
 * File:   MatlabWriter.h
 * Author: lemelino
 *
 * Created on December 31, 2014, 3:18 PM
 */

#ifndef MATLABWRITER_H
#define	MATLABWRITER_H

#include <string>
#include <vector>
#include <matio.h>
#include "DataAccumulator.h"

class MatlabWriter {
public:
    static void saveResults(std::string fileName, double* dataMatrix, 
            unsigned int numberOfChannels, unsigned long samplesPerChannel);
    static void saveResults(std::string fileName, float* dataMatrix, 
            unsigned int numberOfChannels, unsigned long samplesPerChannel);
    
private:
    static mat_t* openFile(std::string fileName);
    static void closeFile(mat_t* matlabFilePointer);
    static void writeResults(mat_t* matlabFilePointer, double* dataMatrix, 
            unsigned int numberOfChannels, unsigned long samplesPerChannel);
    static void writeResults(mat_t* matlabFilePointer, float* dataMatrix, 
            unsigned int numberOfChannels, unsigned long samplesPerChannel);
};

#endif	/* MATLABWRITER_H */

