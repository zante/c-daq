/* 
 * File:   DataProcessor.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:31 PM
 */

#ifndef DATAPROCESSOR_H
#define	DATAPROCESSOR_H

#include <zmq.hpp>
#include <string>
#include <vector>
#include "../Common/MessageDef.h"
#include "../Common/CommonDef.h"
#include <matio.h>

#include "DataAccumulator.h"

class DataProcessor {
public:
    /**
     * Constructor
     */
    DataProcessor(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress);
    
    /**
     * Copy Constructor.  Disabled.
     * @param orig
     */
    DataProcessor(const DataProcessor& orig);
    
    /**
     * Destructor
     */
    virtual ~DataProcessor();
    
    /**
     * Main function.  Receives and writes the data received by this
     * module.
     */
    void process();
    
    /**
     * Handles the change of state.
     * @param state New state to change to.
     */
    void handleStateChange(StateMessage state);
    
private:
    
    /**
     * Creates and connects the data pipe in order to receive data.
     */
    void setupDataPipe(std::string dataSocketAddress);
    
    /**
     * Creates a new file name according to the current time.
     * @return The new file name to create.
     */
    std::string generateFileName();
    
    /**
     * Verifies that the filesystem is in a correct state to write the
     * data to a new file.  Mostly, it verifies that there is enough space
     * on the disk to record for a long time (configurable).
     * @return true if the filesystem is OK. false otherwise.
     */
    bool isFilesystemOK(std::string fileNameToTest);
    
    /**
     * Creates an aptly named file and opens it for writing.
     * @return 
     */
    mat_t* openFile(std::string fileName);
    
    /**
     * Closes the file that was written to.
     * @return 
     */
    void closeFile(mat_t* matlabFilePointer);
    
    /**
     * Receive the incoming data chunks.
     */
    void receiveData();
    
    /**
     * Processes (writes) the received data chunks.
     */
    void processDataFrame(double* dataChunk, int nbElementsInBuffer);
    
    /**
     * Function executed when we need to begin writing data.
     */
    void start();
    
    /**
     * Function executed when we need to pause the recording.
     */
    void pause();
    
    /**
     * Function executed when we need to stop writing data.
     */
    void stop();
    
    /**
     * Function executed when we need to save data (upon calling stop(), usually).
     */
    void saveResults();
    
    /**
     * ZMQ Socket used to receive data.
     */
    zmq::socket_t _cSubscriberSocket;
    
    int _iNumberOfChannels;
    
    int _iSamplesPerFrame;
    
    bool _isRecording;
    
    bool _shouldTranscodeToFloat;
    
#ifdef SHOULD_TRANSCODE_TO_FLOAT
    DataAccumulator<float>* _dataAccumulator;
#else
    DataAccumulator<double>* _dataAccumulator;
#endif  
    std::string generatedFileName;
};

#endif	/* DATAPROCESSOR_H */

