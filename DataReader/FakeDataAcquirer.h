/* 
 * File:   DataProcessor.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:31 PM
 */

#ifndef FAKEDATAACQUIRER_H_
#define	FAKEDATAACQUIRER_H_

#include <zmq.hpp>

class FakeDataAcquirer {
public:
    /**
     * Constructor
     */
    FakeDataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress);
    
    /**
     * Copy Constructor.  Disabled.
     * @param orig
     */
    FakeDataAcquirer(const FakeDataAcquirer& orig);
    
    /**
     * Destructor
     */
    virtual ~FakeDataAcquirer();
    
    /**
     * Main function.  Receives and writes the data received by this
     * module.
     */
    void process();
    
private:
    
    /**
     * Creates and connects the data pipe in order to receive data.
     */
    void setupDataAcquisition();
    
    /**
     * Prepares a buffer to transmit a single frame of acquired data.
     */
    double* prepareTransmissionBuffer();
    
    /**
     * Fills the transmission buffer.
     */
    void fillTransmissionBuffer(double* transmissionBuffer);
    
    /**
     * Sends the data frame.
     */
    void sendDataFrame(double* transmissionBuffer);
    
    /**
     * ZMQ Socket used to receive data.
     */
    zmq::socket_t _cDataPublisherSocket;
    
    //TaskHandle taskHandle;
    
    /**
     * Number of channels to receive from.
     */
    int numberOfChannels;
    
    /**
     * Samples per second
     */
    int samplesPerFrame;
    
    double minVoltage;
    
    double maxVoltage;
    
    float _fCurrentFrameNumber;
    
    int _samplingRate;
    
};

#endif	/* FAKEDATAACQUIRER_H_ */

