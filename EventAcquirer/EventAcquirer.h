/* 
 * File:   EventAcquirer.h
 * Author: lemelino
 *
 * Created on January 2, 2015, 10:51 PM
 */

#ifndef EVENTACQUIRER_H
#define	EVENTACQUIRER_H

#include <zmq.hpp>
#include <string>

class EventAcquirer {
public:
    EventAcquirer(zmq::context_t& cZmqContext,
        const std::string& cEventSocketAddress);
    EventAcquirer(const EventAcquirer& orig);
    virtual ~EventAcquirer();
    
    /**
     * Main function.  Receives and writes the data received by this
     * module.
     */
    void process();
    
protected:
    
    virtual void initAcquisitionDevice() = 0;
    
    virtual void tearDownAcquisitionDevice() = 0;
    
    /**
     * 
     * @return -1 if no new code, code if there is a new one.
     */
    virtual bool hasNewCode() = 0;
    
    virtual int getNewCode() = 0;
    
    void sendEventCode(int code);
    
private:
    
    /**
     * ZMQ Socket used to publish events.
     */
    zmq::socket_t _cEventPublisherSocket;
    
    int _currentCode;
};

#endif	/* EVENTACQUIRER_H */

