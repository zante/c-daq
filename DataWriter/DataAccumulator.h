/* 
 * File:   DataAccumulator.h
 * Author: lemelino
 *
 * Created on December 31, 2014, 5:32 PM
 */

#ifndef DATAACCUMULATOR_H
#define	DATAACCUMULATOR_H

#include <glog/logging.h>
#include <string.h>
#include <iostream>
#include "../Common/CommonDef.h"
#include "DataAccumulator.h"

/**
 * This class is used to build the big array that contains all data that
 * needs to be written to the MATLAB File.  This is a necessary evil, as the
 * library does not support a stream writing system.
 */
template <class T> class DataAccumulator {
public:
    /**
     * Constructor.  Allocates a a huge array of (numberOfChannels * 
     * maxNumberOfSamplesPerChannel) elements.
     * @param numberOfChannels Number of recording channels.
     * @param maxNumberOfSamplesPerChannel.  Max number of individual samples
     * that we can acquire during one recording.
     * @param nbElementsInFrame Number of elements in a single data frame.
     */
    DataAccumulator(unsigned int numberOfChannels, 
            unsigned int maxNumberOfSamplesPerChannel,
            unsigned int nbElementsInFrame);
    
    /**
     * Disabled copy constructor.
     * @param orig
     */
    DataAccumulator(const DataAccumulator& orig);
    
    virtual ~DataAccumulator();
    
    /**
     * Appends a new data frame at the end of the array.
     * @param newFrame Pointer to the data frame that was received.
     * @param nbElementsInFrame Number of elements in the new data frame.
     * @return True if we have reached max capacity of the buffer, and false otherwise.
     */
    bool appendDataFrame(T* newFrame, unsigned int nbElementsInFrame);
    
    /**
     * Returns a pointer to the internal array, as it is too big to be copied.
     * NOTE: the user is responsible for deallocating the memory occupied
     * by the array.
     * 
     * @param nbChannels pointer to a variable that will be modified in order
     * to return the number of channels inside the returned matrix.
     * @param nbElementsPerChannel pointer to a variable that will be modified
     * in order to return the length of each channel in number of individual 
     * samples.
     * @return Pointer to the new matrix containing the ordered info.  The
     * matrix will have been shrunk during the process in order to occupy as little
     * memory as possible.
     */
    T* retrieveAllDataAsArray(unsigned int* nbChannels, 
            unsigned long* nbElementsPerChannel);
private:
    
    /**
     * Outputs the internal array and the compressed matrix in order to verify
     * data manually.
     * @param numberOfChannels Number of recording channels
     * @param nbBytesWrittenPerChannel Nb of bytes written in each channel of 
     * the internal array.
     * @param newSmallerArray Pointer to compressed matrix.
     */
    void outputTestData(unsigned int numberOfChannels, 
        unsigned int nbBytesWrittenPerChannel,
        T* newSmallerArray);
    
    /**
     * 
     */
    bool checkProgression();
    
    /**
     * Internal data array.
     */
    T** _internalArray;
    
    T* _preallocatedBlock;
    
    /**
     * Number of elements in a single data frame.
     */
    unsigned int _nbElementsInOneFrame;
    
    /**
     * Number of bytes currently written in the array.  Used in order to
     * know where th write next.
     */
    unsigned long _nbBytesWrittenPerChannel;
    
    /**
     * Number of channels being recorded.
     */
    unsigned int _numberOfChannels;
    
    /**
     * Maximum number of samples (individual points) that a channel may contain.
     */
    unsigned long _maxNumberOfSamplesPerChannel;
    
    /**
     * Number of individual samples that a channel contains in a single frame.
     */
    unsigned int _nbElementsPerChannelInOneFrame;
    
    unsigned long _significantBytesProgressIncrement;
    
    int _lastSignificantBytesIncrementReached;
};

template<class T> DataAccumulator<T>::DataAccumulator(unsigned int numberOfChannels, 
            unsigned int maxNumberOfSamplesPerChannel, 
            unsigned int nbElementsInFrame)
{   
    
    // Initializes the number of written bytes.
    _nbBytesWrittenPerChannel = 0;

    // Number of elements reserved in total.
    unsigned int reservedNumberOfElements = 
            maxNumberOfSamplesPerChannel * numberOfChannels;
    
    // Initializes the number of recording channels.
    _numberOfChannels = numberOfChannels;
    
    // Initializes the number of elements in a single frame.
    _nbElementsInOneFrame = nbElementsInFrame;
    
    // Initializes the maximum number of elements per channel
    _maxNumberOfSamplesPerChannel = maxNumberOfSamplesPerChannel;
    
    // Initializes the number of samples in a single channel in a single frame.
    _nbElementsPerChannelInOneFrame = _nbElementsInOneFrame / _numberOfChannels;
    
    // Remember the last bytes increment reached.
    _lastSignificantBytesIncrementReached = 0;
    
    // We want progress in 10% increments of the total byte length of a channel.
    _significantBytesProgressIncrement = 
            _maxNumberOfSamplesPerChannel * sizeof(T) / 
            BYTES_PROGRESSION_PERCENTAGE_INCREMENT;
    
    LOG(INFO) << "Allocating space for " << reservedNumberOfElements << 
            " elements (" << sizeof(T) * reservedNumberOfElements << 
            " bytes), (" << 
            (reservedNumberOfElements / (int)_nbElementsInOneFrame) <<
            " frames)...";    
    
    // Reserves the new array.  It is two dimensional, thus, we build an array
    // of pointers to arrays of doubles.
    _internalArray = new T*[_numberOfChannels];
        
    // Test that the main array has been allocated.
    if(_internalArray == 0) {
        LOG(FATAL) << "Could not fully allocate Data Accumulator array.";
    }
    
    for(unsigned int i = 0; i < _numberOfChannels; ++i) {
        
        _internalArray[i] = new T[maxNumberOfSamplesPerChannel];
        
        // Test that each array has been successfully allocated.
        if(_internalArray[i] == 0) {
            LOG(FATAL) << "Could not fully allocate Data Accumulator array.";
        }
    }
    
    LOG(INFO) << "Also preallocating memory block for the smaller data matrix...";
    _preallocatedBlock = new T[maxNumberOfSamplesPerChannel * _numberOfChannels];
    
    if(_preallocatedBlock == 0) {
        LOG(FATAL) << "Could not fully allocate block for smaller array.";
    }
    
    LOG(INFO) << "Allocating space for " << maxNumberOfSamplesPerChannel * 
            _numberOfChannels << 
            " elements (" << sizeof(T) * maxNumberOfSamplesPerChannel * 
            _numberOfChannels << 
            " bytes)..."; 
}

template<class T> DataAccumulator<T>::~DataAccumulator() {
    
    if(_preallocatedBlock != 0) {
        delete[] _preallocatedBlock;
        _preallocatedBlock = 0;
    }
    
    // We delete the internal array.
    for(unsigned int i = 0; i < _numberOfChannels; ++i) {        
        delete[] _internalArray[i];
    }
    delete[] _internalArray;
}
    
template<class T> bool DataAccumulator<T>::appendDataFrame(T* newFrame, 
        unsigned int nbElementsInFrame) {

    // We assume that the values received a grouped by channels; thus, for
    // example, the first 10 values are for the first channel, the 10 next
    // values are for the second channel.
    
    //  With the number of elements in a frame and the number of channels in a
    // frame, we get the number of elements in each channel.
    unsigned int nbElementsInChannel = nbElementsInFrame / _numberOfChannels;
    
    // We get the number of elements already written in each channel.
    unsigned int nbWrittenElementsInChannel = _nbBytesWrittenPerChannel / sizeof(T);
    
    // This is a quite fine function to copy data! We might want to optimize it.
    for(unsigned int i = 0; i < _numberOfChannels; i++) {
        for(unsigned int j = 0; j < nbElementsInChannel; j++) {
            
            T* destPtr = &(_internalArray[i][nbWrittenElementsInChannel + j]);
            T* srcPtr = &(newFrame[i * nbElementsInChannel + j]);
            //LOG(INFO) << "At destination: " << *destPtr;
            //LOG(INFO) << "At source: " << *srcPtr;
            
            *destPtr = *srcPtr;
           // LOG(INFO) << "At destination(final): " << *destPtr;
        }
    }
    
    // Accumulates the number of bytes written.
    _nbBytesWrittenPerChannel += _nbElementsPerChannelInOneFrame * sizeof(T);
    
    //std::cout << "Number of bytes written: " << 
    //        _nbBytesWrittenPerChannel << std::endl;

    return checkProgression();
}

template<class T> T* DataAccumulator<T>::retrieveAllDataAsArray( 
        unsigned int* nbChannels, unsigned long* nbElementsPerChannel) {
    
    // Fills out the parameters that were passed.
    
    *nbChannels = _numberOfChannels;
    *nbElementsPerChannel = _nbBytesWrittenPerChannel / sizeof(T);
    
    // We calculate metrics for logging.
    
    unsigned long sizeOfMatrixInBytes = _maxNumberOfSamplesPerChannel * 
            (*nbChannels) * sizeof(T);
    
    unsigned long compressedMatrixSizeInBytes = (*nbElementsPerChannel) * 
            (*nbChannels) * sizeof(T);
    
    unsigned long nbFramesAcquired = *nbElementsPerChannel / _nbElementsPerChannelInOneFrame;
    
    // Log message
    
    LOG(INFO) << "Current size of matrix: " << sizeOfMatrixInBytes << 
            " bytes (" << sizeOfMatrixInBytes / 1000000 << "MB)";
    LOG(INFO) << "Number of channels: " << *nbChannels;
    LOG(INFO) << "Elements per channel: " << *nbElementsPerChannel;
    LOG(INFO) << "Frames acquired: " << nbFramesAcquired; 
    LOG(INFO) << "Compressed matrix size: " << compressedMatrixSizeInBytes <<
            " bytes (" << compressedMatrixSizeInBytes / 1000000 << "MB)";
    LOG(INFO) << "Compression rate: " << ((float)sizeOfMatrixInBytes / 
            (float)compressedMatrixSizeInBytes) << "x.";
    
    LOG(INFO) << "Preallocated block: " << sizeOfMatrixInBytes << 
            " bytes (" << sizeOfMatrixInBytes / 1000000 << "MB)";
    
    // ********** copy data to a smaller matrix... **********

    LOG(INFO) << "Creating, allocating and copying the reduced matrix...";
    
    LOG(INFO) << "Deleting preallocated block...";
    if(_preallocatedBlock != 0) {
        delete[] _preallocatedBlock;
        _preallocatedBlock = 0;
    }
 
    // Reserves the new array.   
    T* newSmallerArray = new T[_numberOfChannels * (*nbElementsPerChannel)];

    // Tests that allocation is successful.
    if(newSmallerArray == 0) {
        LOG(FATAL) << "Could not fully allocate Data Accumulator array.";
    }
    
    // Copy the old data to the new matrix.
    for(unsigned int i = 0; i < _numberOfChannels; ++i) {
        memcpy(newSmallerArray + (i * _nbBytesWrittenPerChannel/sizeof(T)), 
                _internalArray[i], _nbBytesWrittenPerChannel);
    }
    
    //outputTestData(_numberOfChannels, nbBytesWrittenPerChannel, newSmallerArray);
    
    // Assigns the pointer to the internal array.
    
    LOG(INFO) << "Done!";
    
    return newSmallerArray;
}

template<class T> bool DataAccumulator<T>::checkProgression() {
    
    int bytesPerChannel = _maxNumberOfSamplesPerChannel * sizeof(T);
    
    int currentProgressIncrement = 
            (int)(((float)_nbBytesWrittenPerChannel / (float)bytesPerChannel) * 
            (float)BYTES_PROGRESSION_PERCENTAGE_INCREMENT);
    
    if(currentProgressIncrement > _lastSignificantBytesIncrementReached) {
        _lastSignificantBytesIncrementReached = currentProgressIncrement;
        
        LOG(WARNING) << currentProgressIncrement * 10 << 
                "% of buffer size reached!";
        
        if(_lastSignificantBytesIncrementReached == 5) {
            LOG(INFO) << "============================================";
            LOG(INFO) << "WARNING !! WARNING !! WARNING !! WARNING !! ";
            LOG(INFO) << "============================================";
            LOG(INFO) << "You have reached 50% of the maximum         ";
            LOG(INFO) << "recording capacity of the program. Upon     ";
            LOG(INFO) << "reaching 100% of the maximum capacity, the  ";
            LOG(INFO) << "program will terminate the current          ";
            LOG(INFO) << "recording.                                  " ;    
            LOG(INFO) << "============================================";
            LOG(INFO) << "WARNING !! WARNING !! WARNING !! WARNING !! ";
            LOG(INFO) << "============================================";
        }
        
        else if(_lastSignificantBytesIncrementReached == 9) {
            LOG(INFO) << "============================================";
            LOG(INFO) << "WARNING !! WARNING !! WARNING !! WARNING !! ";
            LOG(INFO) << "============================================";
            LOG(INFO) << "You have reached 90% of the maximum         ";
            LOG(INFO) << "recording capacity of the program. Shutdown ";
            LOG(INFO) << "is imminent!                                ";    
            LOG(INFO) << "============================================";
            LOG(INFO) << "WARNING !! WARNING !! WARNING !! WARNING !! ";
            LOG(INFO) << "============================================";
        }
        
        else if(_lastSignificantBytesIncrementReached == 10) {
            LOG(INFO) << "============================================";
            LOG(INFO) << "WARNING !! WARNING !! WARNING !! WARNING !! ";
            LOG(INFO) << "============================================";
            LOG(INFO) << "              SHUTTING DOWN                 ";    
            LOG(INFO) << "============================================";
            LOG(INFO) << "WARNING !! WARNING !! WARNING !! WARNING !! ";
            LOG(INFO) << "============================================";
            
            return false;
        }
    }
    
    return true;
}

template<class T> void DataAccumulator<T>::outputTestData(
        unsigned int numberOfChannels, 
        unsigned int nbBytesWrittenPerChannel,
        T* newSmallerArray) {
    
    for(unsigned int i = 0; i < numberOfChannels; ++i) {
        std::cout << "Small array: ";
        for(unsigned int j = 0; j < nbBytesWrittenPerChannel / sizeof(T); j++) {
            std::cout << newSmallerArray[i * (nbBytesWrittenPerChannel / sizeof(T)) + j] << " ";
        }
        std::cout << std::endl;
    }
    
    for(unsigned int i = 0; i < numberOfChannels; i++) {
        std::cout << "Ending: ";
        for(unsigned int j = 0; j < nbBytesWrittenPerChannel /sizeof(T); j++) {
            std::cout << _internalArray[i][j] << " "; 
        }
        std::cout << std::endl;
    }   
}


#endif	/* DATAACCUMULATOR_H */

