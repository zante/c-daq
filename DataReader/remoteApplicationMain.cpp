/* 
 * File:   newmain.cpp
 * Author: root
 *
 * Created on January 29, 2015, 4:51 PM
 */

#include <zmq.hpp>
#include <glog/logging.h>
#include <string>
#include <cstdlib>
#include "../Common/CommonDef.h"
#include "DataReaderModule.h"
#include "RemoteDataAcquirer.h"
#include <signal.h>
#include "../Common/ConfigurationManager.h"


sig_atomic_t exitSignaled = 0;

void my_handler (int param)
{
  exitSignaled = 1;
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    signal (SIGINT, my_handler);
    signal (SIGKILL, my_handler);
    
    // Setup the google logging library to display all errors
    // on the console instead of inside log files.
    google::InitGoogleLogging(argv[0]);
    FLAGS_logtostderr = 1;
    FLAGS_stderrthreshold = 0;
    
    if(argc < 2) {
        LOG(FATAL) << "Requires the ZMQ Socket address to be passed via argv.";
    }
    
    // Creates the ZMQ Context that will be reused in all other modules.
    zmq::context_t cZmqContext(1);
    
    // Configuration for the nodes: this is the address on which all messaging
    // nodes shall bind or connect.
    std::string cMessagesSocketAddress(MESSAGE_SOCKET_ADDRESS);
    std::string cDataSinkSocketAddress(argv[1]);
    
    //Creates the configuration manager.
    ConfigurationManager* cConfigManager = ConfigurationManager::getInstance();
    cConfigManager->getString("RECORDING_DIRECTORY");
    
    RemoteDataAcquirer rda(cZmqContext,
            cDataSinkSocketAddress);
    
    while(exitSignaled == 0) {
        rda.process();
    }

    return 0;
}

