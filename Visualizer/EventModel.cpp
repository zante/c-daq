/* 
 * File:   EventModel.cpp
 * Author: lemelino
 * 
 * Created on January 21, 2015, 2:49 PM
 */

#include <glog/logging.h>
#include "EventModel.h"

EventModel::EventModel(NanosecondsTime maxNanosToLive) {
    
    _maxNanosToLive = maxNanosToLive;
}

EventModel::~EventModel() {
}

void EventModel::addEvent(Event &event) {
    _eventsList.emplace_front(event);
    
    LOG(INFO) << "Added event (" << event.getEventCode() << ',' << event.getTimestamp()
            << ". Now contains " << _eventsList.size() << " elements.";
}

void EventModel::purgeOldEvents(NanosecondsTime currentTime) {
    auto it = _eventsList.begin();
    
    int counter = 0;
    
    while(it != _eventsList.end()) {
        NanosecondsTime timeEventAdded = it->getTimestamp();
        
        // We erase all of the older elements, as they are overdue!
        if((currentTime - timeEventAdded) > _maxNanosToLive) {
            _eventsList.erase(it, _eventsList.end());
            break;
        }
        
        it++;
        counter++;
    }
}

std::list<Event>::const_iterator EventModel::getIterator() {
    return _eventsList.cbegin();
}

std::list<Event>::const_iterator EventModel::getEnd() {
    return _eventsList.cend();
}

