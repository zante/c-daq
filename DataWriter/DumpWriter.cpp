/* 
 * File:   MatlabWriter.cpp
 * Author: lemelino
 * 
 * Created on December 31, 2014, 3:18 PM
 */

#include <glog/logging.h>
#include "DumpWriter.h"
#include "../Common/ConfigurationManager.h"

void DumpWriter::saveResults(std::string fileName, double* dataMatrix, 
        unsigned int numberOfChannels, unsigned long samplesPerChannel) {
    
    if(samplesPerChannel > 0) {
        LOG(INFO) << "There is data to save! Saving to file '" <<
                fileName << "'...";
    
        ConfigurationManager::getInstance()->getString("RECORDING_DIRECTORY");
        std::string fullPathToFile = ConfigurationManager::getInstance()->
                getString("RECORDING_DIRECTORY") + fileName;

        LOG(INFO) << "Full path to open: '" << fullPathToFile << "'.";

        std::ofstream fileStream;
        fileStream.open(fullPathToFile.c_str(), std::ofstream::out);
        
        if(fileStream.is_open()) {
            
            for(unsigned int i = 0; i < numberOfChannels; ++i) {
                for(unsigned int j = 0; j < samplesPerChannel; ++j) {
                    fileStream << dataMatrix[i * samplesPerChannel + j] << " ";
                }
                fileStream << std::endl;
            }
            
            fileStream.close();
            
        }
        else {
            LOG(FATAL) << "Matlab file pointer was already NULL.  Exiting.";
        }

        LOG(INFO) << "Successfully saved data!";
    } else {
        LOG(INFO) << "No data to save. ";
    }
}

void DumpWriter::saveResults(std::string fileName, float* dataMatrix, 
        unsigned int numberOfChannels, unsigned long samplesPerChannel) {
    
    if(samplesPerChannel > 0) {
        LOG(INFO) << "There is data to save! Saving to file '" <<
                fileName << "'...";
    
        ConfigurationManager::getInstance()->getString("RECORDING_DIRECTORY");
        std::string fullPathToFile = ConfigurationManager::getInstance()->
                getString("RECORDING_DIRECTORY") + fileName;

        LOG(INFO) << "Full path to open: '" << fullPathToFile << "'.";

        std::ofstream fileStream;
        fileStream.open(fullPathToFile.c_str(), std::ofstream::out | std::ofstream::app);

        if(fileStream.is_open()) {
            
            for(unsigned int i = 0; i < numberOfChannels; ++i) {
                for(unsigned int j = 0; j < samplesPerChannel; ++j) {
                    fileStream << dataMatrix[i * samplesPerChannel + j] << " ";
                }
                fileStream << std::endl;
            }
            
            fileStream.close();
        }
        else {
            LOG(FATAL) << "Matlab file pointer was already NULL.  Exiting.";
        }

        LOG(INFO) << "Successfully saved data!";
    } else {
        LOG(INFO) << "No data to save. ";
    }
}
