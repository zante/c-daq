/* 
 * File:   ComediDataAcquirer.cpp
 * Author: acquisition
 * 
 * Created on March 12, 2015, 2:54 PM
 */

#include "ComediDataAcquirer.h"

#include <glog/logging.h>
#include <vector>
#include <string>
#include <sstream>
#include "../Common/ConfigurationManager.h"
#include "../Common/CommonDef.h"
#include "ComediWrapper.h"

#define REFERENCE_LEVEL AREF_GROUND;




ComediDataAcquirer::ComediDataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress) :
_cDataPublisherSocket(cZmqContext, ZMQ_PUB)
{
    // Bind to the data socket.
    
    _cDataPublisherSocket.bind(cDataSocketAddress.c_str());
    if(zmq_errno() == EADDRINUSE) {
        LOG(INFO) << "Cannot bind to address '" << cDataSocketAddress <<
                "'...";
    }
    
    channelRangeInfo = NULL;
    
    //Configures the data acquisition.
    
    setupDataAcquisition();
}

ComediDataAcquirer::~ComediDataAcquirer() {
    
    Comedi::stopCommand(comediDevice, comediSubdevice);
    
    if(comediDevice != NULL) {
        Comedi::closeDevice(comediDevice);
    }
    
    delete[] channelRangeInfo;
    delete[] channelMaximums;
    
    comediDevice = NULL;
}

void ComediDataAcquirer::setupDataAcquisition() {

    std::string deviceToOpen = ConfigurationManager::getInstance()->
        getString("COMEDI_DEVICE");
    
    comediDevice = Comedi::openDevice(deviceToOpen);
    
    Comedi::setClippedDataBehaviour();
    
    // Get channel list
    
    const int baseChannel = ConfigurationManager::getInstance()->
        getInt("COMEDI_BASE_CHANNEL");
    
    numberOfChannels = ConfigurationManager::getInstance()->
        getInt("NUMBER_OF_CHANNELS");
    
    comediSubdevice = Comedi::getSubdevice(comediDevice);  
    
    const int samplesPerSecond = ConfigurationManager::getInstance()->
        getInt("SAMPLES_PER_SECOND");
    
    const int range = ConfigurationManager::getInstance()->
        getInt("COMEDI_RANGE");
    
    const int samplePeriodInNanoseconds = NANOSECONDS_PER_SECOND / samplesPerSecond;
    
    samplesPerFrame = ConfigurationManager::getInstance()->getInt("SAMPLES_PER_FRAME");

    // Obtains channel list
    
    unsigned int* channelList = Comedi::getChannelList(baseChannel, numberOfChannels, AREF_GROUND, range);
    
    LOG(INFO) << "Channel list: ";
    for(int i = 0; i < numberOfChannels; i++) {
        LOG(INFO) << "\t ( " << channelList[i] << " )";
    }
    
    // Obtains range info
    
    channelRangeInfo = Comedi::getChannelRangeInfo(comediDevice, 
            comediSubdevice, baseChannel, numberOfChannels, range);
    
    channelMaximums = Comedi::getChannelMaximums(comediDevice, 
            comediSubdevice, baseChannel, numberOfChannels);
    
    LOG(INFO) << "Channel range info: ";
    for(int i = 0; i < numberOfChannels; i++) {
        
        const char* unitName = NULL;
        
        switch(channelRangeInfo->unit) {
            case UNIT_volt: unitName = "V"; break;
            case UNIT_mA: unitName = "mA"; break;
            case UNIT_none: break;
        }
        
        LOG(INFO) << "\t ( " << channelRangeInfo->min << " -> " <<
                channelRangeInfo->max << " ) [" << unitName << "]";
    }
    
    // Builds the command used for the acquisition.
    
    comedi_cmd* startCommand = Comedi::prepareAcquisitionCommand(comediDevice, 
        comediSubdevice, numberOfChannels, channelList, samplePeriodInNanoseconds);

    // Tests it!
    if( !Comedi::testCommand(comediDevice, startCommand) ) {
        LOG(FATAL) << "Command test failed.";
    } 
    
    Comedi::dump_command(startCommand);
            
    if(! Comedi::launchCommand(comediDevice, startCommand) ) {
        LOG(FATAL) << "Could not launch the acquisition command.";
    }
    else {
        LOG(INFO) << "Acquisition launched.";
    }
    
    bytesPerSample = Comedi::getBytesPerSample(comediDevice, comediSubdevice);
   
    delete startCommand;
    
}

void ComediDataAcquirer::process() {
    
    // We acquire exactly one frame at a time.
    //double dataBuffer[samplesPerFrame * numberOfChannels];
    unsigned int frameSizeInBytes = samplesPerFrame * numberOfChannels * sizeof(double);
    //unsigned int frameSizeInElements = sizeof(dataBuffer) / sizeof(double);
    
    Comedi::setNonBlocking(comediDevice);
    
    // Retrieves the transmission buffer from the function.
    //double* transmissionBuffer = prepareTransmissionBuffer();
    
    // Creates a buffer able to store data for a single frame.
    //TODO: This might need to be optimized.
    double* transmissionBuffer = new double[numberOfChannels * samplesPerFrame];

    // Checks if the buffer was correctly assigned (Not NULL).
    if(transmissionBuffer == 0) {
        LOG(FATAL) << "Could not allocate memory for transmission buffer.";
    }
    
    // Sets the buffer to all zeros.
    // TODO: Remove this it it takes too much CPU.
    memset(transmissionBuffer, 0.0, numberOfChannels * samplesPerFrame);
    
    while( Comedi::processAvailableData(comediDevice, comediSubdevice, 
            frameSizeInBytes, channelRangeInfo, channelMaximums, transmissionBuffer, samplesPerFrame * numberOfChannels) ) {
        
        // Sends the acquired data.
        sendDataFrame(transmissionBuffer);
        
        memset(transmissionBuffer, 0.0, numberOfChannels * samplesPerFrame);
    }

    delete[] transmissionBuffer;
    transmissionBuffer = 0;

}

double* ComediDataAcquirer::prepareTransmissionBuffer() {
    
    // Creates a buffer able to store data for a single frame.
    //TODO: This might need to be optimized.
    double* transmissionBuffer = new double[numberOfChannels * samplesPerFrame];

    // Checks if the buffer was correctly assigned (Not NULL).
    if(transmissionBuffer == 0) {
        LOG(FATAL) << "Could not allocate memory for transmission buffer.";
    }
    
    // Sets the buffer to all zeros.
    // TODO: Remove this it it takes too much CPU.
    memset(transmissionBuffer, 0.0, numberOfChannels * samplesPerFrame);
    
    // Returns prepared buffer.
    return transmissionBuffer;
}

void ComediDataAcquirer::fillTransmissionBuffer(double* transmissionBuffer, size_t bufferSizeInBytes) {
    
    // Here:
    // We read <samplesPerFrame> per channel
    // Wait indefinitely to obtain all of the samples (We do not want unfinished
    // frames)
    // We group values per channel. (thus, 10 values for channel 1, 
    // 10 values for channel 2))
    // We send data to the transmissionBuffer
    // We indicate that the size of the transmissionBuffer is of 
    // <samplesPerFrame * numberOfChannels>.
    // We retrieve the number of retrieved samples into <numberOfSamplesRead>.
    // Pass null, as it is a reserved field (unused).
    
    //unsigned int readBytes = Comedi::readAcquiredFrame(comediDevice, bufferSizeInBytes, bytesPerSample, transmissionBuffer);
    
    
    // Verifies that we read as many samples as we needed to.
    // TODO: Can be optimized out someday.
    
}

void ComediDataAcquirer::sendDataFrame(double* transmissionBuffer) {
    //LOG(INFO) << "Sending data frame...";
    
    // Creates the message, reserving enough space for the whole buffer that 
    // was received.
    
    int bufferSizeInBytes = sizeof(double) * samplesPerFrame * numberOfChannels;
    
    //LOG(INFO) << bufferSizeInBytes;
    
    zmq::message_t msg((std::size_t) bufferSizeInBytes);
    
    // Copies the integer inside the state message.
    
    memcpy(msg.data(), transmissionBuffer, (std::size_t) bufferSizeInBytes);
    
    // Send away!
    
    _cDataPublisherSocket.send(msg);
}