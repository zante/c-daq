/* 
 * File:   MessageDef.h
 * Author: lemelino
 *
 * Created on December 25, 2014, 11:34 PM
 */

#ifndef MESSAGEDEF_H
#define	MESSAGEDEF_H

/**
 * Definition of all messages that can be sent by the controller to the
 * other modules.
 */
enum StateMessage {START, PAUSE, STOP, EXIT, SCALING};

#endif	/* MESSAGEDEF_H */

