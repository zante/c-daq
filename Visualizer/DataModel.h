/* 
 * File:   DataModel.h
 * Author: lemelino
 *
 * Created on January 20, 2015, 9:40 PM
 */

#ifndef DATAMODEL_H
#define	DATAMODEL_H

#include <vector>
#include <deque>

class DataModel {
public:
    DataModel(int nbChannels, int maxNbElementsPerChannel);
    DataModel(const DataModel& orig);
    virtual ~DataModel();
    
    void addDataFrame(float* dataChunk, unsigned int nbChannels, unsigned int nbElementsPerChannel);
    size_t getSize();
    
    std::deque<float>::const_reverse_iterator getIteratorForChannel(int noChannel);
    std::deque<float>::const_reverse_iterator getIteratorEndForChannel(int noChannel);
    
private:

    std::vector<std::deque<float>*> _internalArray;
    size_t _maxNbElementsPerChannel;
    unsigned int _nbChannels;
    size_t _currentNbOfElementsPerChannel;
};

#endif	/* DATAMODEL_H */

