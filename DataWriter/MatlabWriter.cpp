/* 
 * File:   MatlabWriter.cpp
 * Author: lemelino
 * 
 * Created on December 31, 2014, 3:18 PM
 */

#include <glog/logging.h>
#include "MatlabWriter.h"
#include "../Common/ConfigurationManager.h"

#define MY_COMPRESSION MAT_COMPRESSION_ZLIB

void MatlabWriter::saveResults(std::string fileName, double* dataMatrix, 
        unsigned int numberOfChannels, unsigned long samplesPerChannel) {
    
    if(samplesPerChannel > 0) {
        LOG(INFO) << "There is data to save! Saving to file '" <<
                fileName << "'...";
    
        mat_t* matfilePointer = openFile(fileName);
        writeResults(matfilePointer, dataMatrix, numberOfChannels, samplesPerChannel);
        closeFile(matfilePointer);

        LOG(INFO) << "Successfully saved data!";
    } else {
        LOG(INFO) << "No data to save. ";
    }
}

void MatlabWriter::saveResults(std::string fileName, float* dataMatrix, 
        unsigned int numberOfChannels, unsigned long samplesPerChannel) {
    
    if(samplesPerChannel > 0) {
        LOG(INFO) << "There is data to save! Saving to file '" <<
                fileName << "'...";
    
        mat_t* matfilePointer = openFile(fileName);
        writeResults(matfilePointer, dataMatrix, numberOfChannels, samplesPerChannel);
        closeFile(matfilePointer);

        LOG(INFO) << "Successfully saved data!";
    } else {
        LOG(INFO) << "No data to save. ";
    }
}

mat_t* MatlabWriter::openFile(std::string fileName) {
    ConfigurationManager::getInstance()->getString("RECORDING_DIRECTORY");
    std::string fullPathToFile = ConfigurationManager::getInstance()->
            getString("RECORDING_DIRECTORY") + fileName;
    
    LOG(INFO) << "Full path to open: '" << fullPathToFile << "'.";
    
    mat_t *matlabFilePointer;
    
    matlabFilePointer = Mat_CreateVer(fullPathToFile.c_str(), NULL,MAT_FT_DEFAULT);
    if(matlabFilePointer == NULL) {
        LOG(FATAL) << "Problems opening MATLAB file in write mode.";
    }
    
    return matlabFilePointer; 
}

void MatlabWriter::writeResults(mat_t* matlabFilePointer, double* dataMatrix, 
        unsigned int numberOfChannels, unsigned long samplesPerChannel) {
    
    matvar_t *matVar;
    size_t dims[2] = {numberOfChannels, samplesPerChannel};
    const char* varName = "data";
    
    matVar = Mat_VarCreate(varName, MAT_C_DOUBLE, MAT_T_DOUBLE, 
            2, dims, dataMatrix, 0);
    
    if(matVar == NULL) {
        LOG(FATAL) << "Could not create MatLab variable '" << varName << "'.";
    } else {
        Mat_VarWrite(matlabFilePointer, matVar, MY_COMPRESSION);
    }
}

void MatlabWriter::writeResults(mat_t* matlabFilePointer, float* dataMatrix, 
        unsigned int numberOfChannels, unsigned long samplesPerChannel) {
    
    matvar_t *matVar;
    size_t dims[2] = {numberOfChannels, samplesPerChannel};
    const char* varName = "data";
    
    matVar = Mat_VarCreate(varName, MAT_C_DOUBLE, MAT_T_DOUBLE, 
            2, dims, dataMatrix, 0);
    
    if(matVar == NULL) {
        LOG(FATAL) << "Could not create MatLab variable '" << varName << "'.";
    } else {
        Mat_VarWrite(matlabFilePointer, matVar, MY_COMPRESSION);
    }
    
    Mat_VarFree(matVar);
}

void MatlabWriter::closeFile(mat_t* matlabFilePointer) {
    if(matlabFilePointer != NULL) {
        Mat_Close(matlabFilePointer);
        matlabFilePointer = NULL;
    }
    else {
        LOG(FATAL) << "Matlab file pointer was already NULL.  Exiting.";
    }
}

