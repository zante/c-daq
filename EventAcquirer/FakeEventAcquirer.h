/* 
 * File:   ParallelPortEventAcquirer.h
 * Author: lemelino
 *
 * Created on January 2, 2015, 11:15 PM
 */

#ifndef FAKEEVENTACQUIRER_H
#define	FAKEEVENTACQUIRER_H

#include "EventAcquirer.h"


class FakeEventAcquirer : public EventAcquirer {
public:
    FakeEventAcquirer(zmq::context_t& cZmqContext,
        const std::string& cEventSocketAddress);
    FakeEventAcquirer(const ParallelPortEventAcquirer& orig);
    virtual ~FakeEventAcquirer();
private:
    
    void initAcquisitionDevice();
    
    void tearDownAcquisitionDevice();

    bool hasNewCode();
    
    int getNewCode();
    
    int _cCurrentCode;
    
    int _iterationCounter;
};

#endif	/* FAKEEVENTACQUIRER_H */

