/* 
 * File:   ParallelPortEventAcquirer.cpp
 * Author: lemelino
 * 
 * Created on January 2, 2015, 11:15 PM
 */

#include <unistd.h>
#include <glog/logging.h>
#include "ParallelPortEventAcquirer.h"
#include "FakeEventAcquirer.h"

FakeEventAcquirer::FakeEventAcquirer(zmq::context_t& cZmqContext,
        const std::string& cEventSocketAddress) :
EventAcquirer::EventAcquirer(cZmqContext, cEventSocketAddress) {
    
    initAcquisitionDevice();
    
    _cCurrentCode = 0;
    _iterationCounter = 0;
}

FakeEventAcquirer::~FakeEventAcquirer() {
    
    tearDownAcquisitionDevice();
}

void FakeEventAcquirer::initAcquisitionDevice() {
}

void FakeEventAcquirer::tearDownAcquisitionDevice() {
}

bool FakeEventAcquirer::hasNewCode() {
    
    // Sleeps for 100 milliseconds.
    usleep(100000);
    
    if(_iterationCounter % 10 == 9) {
        _cCurrentCode++;
        _iterationCounter = 0;
        return true;
    }
    
    _iterationCounter++;
    
    return false;
}

int FakeEventAcquirer::getNewCode() {
    return (int)_cCurrentCode;
}

