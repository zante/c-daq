/* 
 * File:   DataWriter.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:20 PM
 */

#include <glog/logging.h>
#include "DataWriterModule.h"

DataWriterModule::DataWriterModule(zmq::context_t& cZmqContext, 
        const std::string& cMessageSocketAddress, const std::string& cDataSocketAddress) {
    
    _cCurrentState = STOP;
    
    _cDataProcessor = new DataProcessor(cZmqContext, cDataSocketAddress);
    _cMessageProcessor = new MessageProcessor(*this, cZmqContext, cMessageSocketAddress);
}

DataWriterModule::DataWriterModule(const DataWriterModule& orig) {
}

DataWriterModule::~DataWriterModule() {
    if(_cDataProcessor != NULL) {
        delete _cDataProcessor;
        _cDataProcessor = NULL;
    }
    
    if(_cMessageProcessor != NULL) {
        delete _cMessageProcessor;
        _cMessageProcessor = NULL;
    }
}

void DataWriterModule::process() {
    _cDataProcessor->process();
    _cMessageProcessor->process();
}

void DataWriterModule::onExitMessage() {
    LOG(INFO) << "Exiting DataWriter!";
    _cDataProcessor->handleStateChange(EXIT);
    _cCurrentState = EXIT;
    this->setShouldExit();
}

void DataWriterModule::onPauseMessage(){
    _cDataProcessor->handleStateChange(PAUSE);
    _cCurrentState = PAUSE;
    LOG(INFO) << "Pausing DataWriter!";    
}

void DataWriterModule::onStartMessage(){
    _cDataProcessor->handleStateChange(START);
    _cCurrentState = START;
    LOG(INFO) << "Starting DataWriter!";    
}

void DataWriterModule::onStopMessage(){
    _cDataProcessor->handleStateChange(STOP);
    _cCurrentState = STOP;
    LOG(INFO) << "Stopping DataWriter!";    
}

StateMessage DataWriterModule::getCurrentState() {
    return _cCurrentState;
}

