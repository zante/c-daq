/* 
 * File:   CommonDef.h
 * Author: lemelino
 *
 * Created on December 27, 2014, 1:44 AM
 */

/**
 * This header is used to define various macros and variables that will be used
 * across the project.
 */

#ifndef COMMONDEF_H
#define	COMMONDEF_H

/**
 * Defines if we are in PRODUCTION_MODE or TESTING_MODE
 */
#define TESTING_MODE

/**
 * The path to the configuration file (used by the config manager).
 */
#define CONFIG_FILE "config.json"

/**
 * String defining the message socket address.
 */
#define MESSAGE_SOCKET_ADDRESS "inproc://messages"

/**
 * String defining the data socket address.
 */
#define DATA_SOCKET_ADDRESS "inproc://data"

/**
 * String defining the external events socket address.
 */
#define EVENTS_SOCKET_ADDRESS "inproc://events"

/**
 * String defining the filename's format.
 */
#define FILE_NAME_FORMAT "%Y_%a_%b_%d_%H-%M-%S"

/**
 * Number of seconds in a minute.
 */
#define SECONDS_PER_MINUTE 60

#define MILLISECONDS_PER_SECOND 1000

#define MICROSECONDS_PER_MILLISECOND 1000

#define NANOSECONDS_PER_MICROSECOND 1000

#define MICROSECONDS_PER_SECOND 1000000

#define NANOSECONDS_PER_SECOND 1000000000

#define BYTES_PROGRESSION_PERCENTAGE_INCREMENT 10

/**
 * Size of the unit (double) that stores the data extracted from
 * the acquisition card.
 */
//#define SHOULD_TRANSCODE_TO_FLOAT


#ifdef SHOULD_TRANSCODE_TO_FLOAT
#define NUMBER_OF_BYTES_PER_DATA_POINT sizeof(float)
#else
#define NUMBER_OF_BYTES_PER_DATA_POINT sizeof(double)
#endif

typedef unsigned long long NanosecondsTime;
typedef unsigned long long MicrosecondsTime;

#endif	/* COMMONDEF_H */

