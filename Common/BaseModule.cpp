/* 
 * File:   BaseModule.cpp
 * Author: lemelino
 * 
 * Created on November 15, 2014, 12:05 AM
 */

#include <glog/logging.h>
#include "BaseModule.h"


BaseModule::BaseModule() {
    _bShouldExit = false;
}

BaseModule::BaseModule(const BaseModule& orig) {
}

BaseModule::~BaseModule() {
}

void BaseModule::launch() {
    
    while(!_bShouldExit) {
        process();
    }
}

void BaseModule::setShouldExit() {
    _bShouldExit = true;
}

bool BaseModule::isShouldExit() {
    return _bShouldExit;
}


