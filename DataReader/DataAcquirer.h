/* 
 * File:   DataProcessor.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:31 PM
 */

#ifndef DATAACQUIRER_H_
#define	DATAACQUIRER_H_

#include <zmq.hpp>

class DataAcquirer {
public:
    /**
     * Constructor
     */
    DataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress);
    
    /**
     * Copy Constructor.  Disabled.
     * @param orig
     */
    DataAcquirer(const DataAcquirer& orig);
    
    /**
     * Destructor
     */
    virtual ~DataAcquirer();
    
    /**
     * Main function.  Receives and writes the data received by this
     * module.
     */
    void process();
    
private:
    
    /**
     * Creates and connects the data pipe in order to receive data.
     */
    void setupDataAcquisition();
    
    /**
     * Prepares a buffer to transmit a single frame of acquired data.
     */
    double* prepareTransmissionBuffer();
    
    /**
     * Fills the transmission buffer.
     */
    void fillTransmissionBuffer(double* transmissionBuffer);
    
    /**
     * Sends the data frame.
     */
    void sendDataFrame(double* transmissionBuffer);
    
    /**
     * ZMQ Socket used to receive data.
     */
    zmq::socket_t _cDataPublisherSocket;
    
    /**
     * Handle for the NI-DAQ acquisition task.
     */
    //TaskHandle taskHandle;
    
    /**
     * Number of channels to receive from.
     */
    int numberOfChannels;
    
    /**
     * Samples per second
     */
    int samplesPerFrame;
    
};

#endif	/* DATAACQUIRER_H_ */

