/* 
 * File:   ConfigurationManager.h
 * Author: lemelino
 *
 * Created on December 27, 2014, 1:35 AM
 */

#ifndef CONFIGURATIONMANAGER_H
#define	CONFIGURATIONMANAGER_H

#include <vector>
#include <string>
#include <glog/logging.h>
#include "../includes/rapidjson/document.h"

class ConfigurationManager {
public:
    static ConfigurationManager* getInstance();
    
    //Disabled constructor
    ConfigurationManager(const ConfigurationManager& orig);
    
    virtual ~ConfigurationManager();
    
    /**
     * Returns true if the configuration manager has the key
     * loaded.
     * @param key Key associated to the value.
     * @return True if the key exists, false otherwise.
     */
    bool contains(std::string key);
    
    /**
     * Returns a String value associated to the passed key.
     * @param key Key associated to the value.
     * @return The String associated to the key.
     */
    std::string getString(std::string key);
    
    /**
     * Returns an Integer value associated to the passed key.
     * @param key Key associated to the value.
     * @return The Integer associated to the key.
     */
    int getInt(std::string key);
    
    /**
     * Returns a Double value associated to the passed key.
     * @param key Key associated to the value.
     * @return The Double associated to the key.
     */
    double getDouble(std::string key);
    
    bool getBoolean(std::string key);
    
    std::vector<std::string> getStringArray(std::string key);
private:
    /**
     * Private constructor, which requires a path to the config file.
     * @param configFile The configuration file path.
     */
    ConfigurationManager(const std::string& configFile);
    
    /**
     * Extracts the contents of the file and return it as a string.
     * @param configFile Path to the file to be read.
     * @return The contents of the config file.
     */
    std::string readFile(const std::string& configFile);
    
    /**
     * Gets a rapidjson::Value associated to the specified key.
     * @param key Key.
     * @return The value.
     */
    rapidjson::Value& getValue(std::string& key);
    
    /**
     * Singleton instance of the class.
     */
    static ConfigurationManager* _cInstance;
    
    /**
     * JSON Document that contains the parsed JSON string.
     */
    rapidjson::Document jsonDoc;
};

#endif	/* CONFIGURATIONMANAGER_H */

