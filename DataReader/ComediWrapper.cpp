/* 
 * File:   ComediWrapper.cpp
 * Author: acquisition
 * 
 * Created on March 19, 2015, 3:03 PM
 */

#include "ComediWrapper.h"

#include <errno.h>
#include <string>
#include <glog/logging.h>

namespace Comedi {
    
    // TYPES and ENUMS
    
    const char *cmdTestMessages[]= {
	"success",
	"invalid source",
	"source conflict",
	"invalid argument",
	"argument conflict",
	"invalid chanlist",
    };
    
    unsigned int START_SRC[] = {TRIG_INVALID, TRIG_NOW, TRIG_FOLLOW, TRIG_EXT, TRIG_INT, TRIG_ANY};
    const char* START_SRC_STR[] = {"TRIG_INVALID", "TRIG_NOW", "TRIG_FOLLOW", "TRIG_EXT", "TRIG_INT", "TRIG_ANY"};
    
    unsigned int SCAN_BEGIN_SRC[] = {TRIG_INVALID, TRIG_TIMER, TRIG_FOLLOW, TRIG_EXT, TRIG_ANY};
    const char* SCAN_BEGIN_SRC_STR[] = {"TRIG_INVALID", "TRIG_TIMER", "TRIG_FOLLOW", "TRIG_EXT", "TRIG_ANY"};
    
    unsigned int CONVERT_SRC[] = {TRIG_INVALID, TRIG_TIMER, TRIG_EXT, TRIG_NOW, TRIG_ANY};
    const char* CONVERT_SRC_STR[] = {"TRIG_INVALID", "TRIG_TIMER", "TRIG_EXT", "TRIG_NOW", "TRIG_ANY"};
    
    unsigned int SCAN_END_SRC[] = {TRIG_INVALID, TRIG_COUNT, TRIG_ANY};
    const char* SCAN_END_SRC_STR[] = {"TRIG_INVALID", "TRIG_COUNT", "TRIG_ANY"};
    
    unsigned int STOP_SRC[] = {TRIG_INVALID, TRIG_COUNT, TRIG_NONE, TRIG_ANY};
    const char* STOP_SRC_STR[] = {"TRIG_INVALID", "TRIG_COUNT", "TRIG_NONE", "TRIG_ANY"};
    
    unsigned int FLAGS[] = {TRIG_RT, TRIG_WAKE_EOS, TRIG_ROUND_NEAREST, 
        TRIG_ROUND_DOWN, TRIG_ROUND_UP, TRIG_ROUND_UP_NEXT, TRIG_DITHER, 
        TRIG_DEGLITCH, TRIG_WRITE, TRIG_BOGUS, TRIG_CONFIG};
    const char* FLAGS_STR[] = {"TRIG_RT", "TRIG_WAKE_EOS", "TRIG_ROUND_NEAREST", 
        "TRIG_ROUND_DOWN", "TRIG_ROUND_UP", "TRIG_ROUND_UP_NEXT", "TRIG_DITHER", 
        "TRIG_DEGLITCH", "TRIG_WRITE", "TRIG_BOGUS", "TRIG_CONFIG"};
    
    std::string startSrcToString(unsigned int startSrc) {
        
        for(unsigned int x = 0; x < sizeof(START_SRC); x++) {
            if(START_SRC[x] == startSrc) {
                return std::string(START_SRC_STR[x]);
            }
        }
        
        LOG(WARNING) << "Could not find code " << startSrc;
        return std::string("UNKNOWN");
    }
    
    std::string scanBeginSrcToString(unsigned int scanBeginSrc) {
        
        for(unsigned int x = 0; x < sizeof(SCAN_BEGIN_SRC); x++) {
            if(SCAN_BEGIN_SRC[x] == scanBeginSrc) {
                return std::string(SCAN_BEGIN_SRC_STR[x]);
            }
        }
        
        LOG(WARNING) << "Could not find code " << scanBeginSrc;
        return std::string("UNKNOWN");
    }
    
    std::string convertSrcToString(unsigned int convertSrc) {
        
        for(unsigned int x = 0; x < sizeof(CONVERT_SRC); x++) {
            if(CONVERT_SRC[x] == convertSrc) {
                return std::string(CONVERT_SRC_STR[x]);
            }
        }
        
        LOG(WARNING) << "Could not find code " << convertSrc;
        return std::string("UNKNOWN");
    }
    
    std::string scanEndSrcToString(unsigned int scanEndSrc) {
        
        for(unsigned int x = 0; x < sizeof(SCAN_END_SRC); x++) {
            if(SCAN_END_SRC[x] == scanEndSrc) {
                return std::string(SCAN_END_SRC_STR[x]);
            }
        }
        
        LOG(WARNING) << "Could not find code " << scanEndSrc;
        return std::string("UNKNOWN");
    }
    
    std::string stopSrcToString(unsigned int stopSrc) {
        
        for(unsigned int x = 0; x < sizeof(STOP_SRC); x++) {
            if(STOP_SRC[x] == stopSrc) {
                return std::string(STOP_SRC_STR[x]);
            }
        }
        
        LOG(WARNING) << "Could not find code " << stopSrc;
        return std::string("UNKNOWN");
    }
    
    std::string flagsToString(unsigned int flags) {
        std::string longStr("");
        for(unsigned int x = 0; x < sizeof(FLAGS); x++) {
            if(FLAGS[x] & flags) {
                longStr += std::string(STOP_SRC_STR[x]) + ", ";
            }
        }
        
        if(longStr.size() >= 2) {
            return longStr.substr(0, longStr.size() - 2);
        } else {
            return "NONE";
        }
    }
    
    // FUNCTIONS
    
    comedi_t* openDevice(std::string& comediDevicePath) {

        LOG(INFO) << "Opening device '" << comediDevicePath << "' ...";

        comedi_t* dev = comedi_open(comediDevicePath.c_str());

        if(!dev){
                LOG(FATAL) << comedi_strerror(errno);
        } else {
            LOG(INFO) << "Operation was successful.";
        }

        return dev;
    }

    void closeDevice(comedi_t* deviceToClose) {

        LOG(INFO) << "Closing device '" << deviceToClose << "' ...";

        int rc = comedi_close(deviceToClose);

        if(rc != 0){
                LOG(FATAL) << "Could not close Comedi device.";
        } else {
            LOG(INFO) << "Operation was successful.";
        }

        deviceToClose = NULL;
    }

    void setClippedDataBehaviour() {
        // Print numbers for clipped inputs
        comedi_set_global_oor_behavior(COMEDI_OOR_NUMBER);
    }
    
    int getSubdevice(comedi_t* device) {
        return comedi_find_subdevice_by_type(device, COMEDI_SUBD_AI, 0);
    }

    unsigned int* getChannelList(const int baseChannel, 
            const int numberOfChannels, const int referenceLevel, const int range) {

        unsigned int* channelList = new unsigned int[numberOfChannels];

        for(int i = 0; i < numberOfChannels; i++){
            channelList[i] = CR_PACK(baseChannel + i, range, referenceLevel);
        }

        return channelList;
    }

    comedi_range* getChannelRangeInfo(comedi_t* comediDevice, 
            const int comediSubDevice, const int baseChannel, const int numberOfChannels, const int range) {

        comedi_range* range_info = comedi_get_range(comediDevice, comediSubDevice, 
                    baseChannel, 0);

        return range_info;
    }

    lsampl_t* getChannelMaximums(comedi_t* comediDevice, 
            const int comediSubDevice, const int baseChannel, const int numberOfChannels) {

        lsampl_t* maxData = new lsampl_t[numberOfChannels];

        for(int i = 0; i < numberOfChannels; i++){
                maxData[i] = comedi_get_maxdata(comediDevice, comediSubDevice, 
                        baseChannel + i);
        }

        return maxData;
    }

    comedi_cmd* prepareAcquisitionCommand(comedi_t* comediDevice, 
            unsigned int subdevice, unsigned int nbChannels, unsigned int* channelList, 
            unsigned int scanPeriodInNanoseconds) 
    {

        // Creates and nulls the comedi command.
        comedi_cmd* newCommand = new comedi_cmd();
        memset(newCommand, 0, sizeof(*newCommand));

        // This comedilib function will get us a generic timed
        // * command for a particular board.  If it returns -1,
        // * that's bad.
        int rc = comedi_get_cmd_generic_timed(comediDevice, subdevice, newCommand, 
                nbChannels, scanPeriodInNanoseconds);

        if(rc != 0){
                LOG(WARNING) << "comedi_get_cmd_generic_timed failed.";
                return NULL;
        }
        
        newCommand->stop_src = TRIG_NONE;
        newCommand->stop_arg = 0;

        /* Modify parts of the command */
        newCommand->chanlist = channelList;
        newCommand->chanlist_len = nbChannels;

        return newCommand;
    }

    bool testCommand(comedi_t* comediDevice, comedi_cmd* command) {

        // Makes sure that there is a comamnd to test!
        if( !command ) {
            LOG(FATAL) << "Command is NULL.";
        }

        int rc = comedi_command_test(comediDevice, command);

        if(rc < 0){

            if(errno == EIO){
                LOG(FATAL) << "This subdevice does not support commands.";
            }

            LOG(WARNING) << "Comedi_command_test failed. " << comedi_strerror(errno);
        }

        rc = comedi_command_test(comediDevice, command);

        if(rc < 0){
            LOG(FATAL) << "Comedi_command_test failed. " << comedi_strerror(errno);
        }

        LOG(INFO) << "Second comedi_command_test returned " << rc << ": (" 
                << cmdTestMessages[rc] << ")";

        if(rc != 0){
                LOG(FATAL) << "Error preparing command.";
        }

        return rc >= 0;
    }

    bool launchCommand(comedi_t* comediDevice, comedi_cmd* command) {

        // start the command
        int rc = comedi_command(comediDevice, command);

        if(rc < 0){
                LOG(WARNING) << "Error launching command: " << rc;
        }

        return (rc >= 0);
    }
    
    size_t getBytesPerSample(comedi_t* comediDevice, int subdevice) {
        
        size_t bytesPerSample;
        
        unsigned int subdev_flags = comedi_get_subdevice_flags(comediDevice, subdevice);
        
        if(subdev_flags & SDF_LSAMPL) {
            bytesPerSample = sizeof(lsampl_t);
        } else {
            bytesPerSample = sizeof(sampl_t);
        }
        
        return bytesPerSample;
    }
    
    void setNonBlocking(comedi_t* comediDevice) {
        int fd = comedi_fileno(comediDevice);
        int flags = fcntl(fd, F_GETFL, 0);
        fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    }
    
    bool processAvailableData(comedi_t* comediDevice, int subdevice, 
            size_t frameSizeInBytes, comedi_range* range_info, lsampl_t* channelMaximums, double* frameBuffer,
            int expectedSamplesRead) {
        
        bool rVal = true;
        
        size_t bytesPerSample = getBytesPerSample(comediDevice, subdevice);
        
        //Declare temporary buffer for getting all the information...
        //Since we convert lsampl_t (unsigned int) to double, we only need to allocate
        // half as much space.
        size_t bufSize = (frameSizeInBytes/sizeof(double)) * bytesPerSample;
        char* tempBuffer = new char[bufSize];
        memset(tempBuffer, 0, bufSize);
        
        /*if(bytesPerSample == sizeof(lsampl_t)) {
            for(unsigned int i = 0; i < bufSize /sizeof(lsampl_t); i++) {
                //LOG(INFO) << i << " " << ((lsampl_t *)tempBuffer)[i];
            }
        }
        else {
            for(unsigned int i = 0; i < bufSize /sizeof(sampl_t); i++) {
                //LOG(INFO) << i << " " << ((sampl_t *)tempBuffer)[i];
            }
        }*/
        
        //Read the required amount of information.
        int bytesRead = read(comedi_fileno(comediDevice), tempBuffer, bufSize);
        
        /*if(bytesPerSample == sizeof(lsampl_t)) {
            for(unsigned int i = 0; i < bufSize /sizeof(lsampl_t); i++) {
                //LOG(INFO) << i << " " << ((lsampl_t *)tempBuffer)[i];
            }
        }
        else {
            for(unsigned int i = 0; i < bufSize /sizeof(sampl_t); i++) {
                //LOG(INFO) << i << " " << ((sampl_t *)tempBuffer)[i];
            }
        }*/

        // If we have a EAGAIN, no data to read.  Skip the rest.
        if(bytesRead < 0 && errno == EAGAIN) {
            rVal = false;
        }
        // If we have an error, crash everything.
        else if(bytesRead < 0) {
            LOG(FATAL) << "Could not read data from the acquisition card.  An error occurred.";
        }
        // Otherwise, convert the bytes to the right format.
        else {
            
            //Check.
            int nbSamplesRead = bytesRead / bytesPerSample;
            if(nbSamplesRead != expectedSamplesRead) {
                /*LOG(INFO) << "Wrong number of samples read: <" << nbSamplesRead <<
                        "> read, but expected to have read <" << expectedSamplesRead << ">.";*/
                rVal = false;
            }
            else {
                
                if(bytesPerSample == sizeof(lsampl_t)) {
                    lsampl_t rawValue;
                    double physical_value;

                    for(unsigned int i = 0; i < bytesRead / sizeof(lsampl_t); i++) {
                        rawValue = ((lsampl_t *)tempBuffer)[i];
                        physical_value = comedi_to_phys(rawValue, &(range_info[0]), channelMaximums[0]);
                        frameBuffer[i] = physical_value;
                        //LOG(INFO) << frameBuffer[i] << " " << i;
                    }
                }
                else {
                    sampl_t rawValue;
                    double physical_value;

                    for(unsigned int i = 0; i < bytesRead / sizeof(sampl_t); i++) {
                        rawValue = ((sampl_t *)tempBuffer)[i];
                        physical_value = comedi_to_phys(rawValue, &(range_info[0]), channelMaximums[0]);
                        frameBuffer[i] = physical_value;
                        //LOG(INFO) << frameBuffer[i] << " " << i;
                    }
                }
            }
        }
        
        // Delete buffer
        if(tempBuffer != 0) {
            delete[] tempBuffer;
            tempBuffer = 0;
        }

        // Thus modified, the frameBuffer of double is returned.
        
        return rVal;
    }
    
    //TODO
    /*void get_conversion_polynomial(std::string& calibrationFilePath) {
        comedi_calibration_t* comedi_parse_calibration_file(calibrationFilePath.c_str());
        
        
    }*/
    
    void dump_command(comedi_cmd* startCommand) {
        LOG(INFO) << "Will use the following parameters to acquire data: "
            << "\n    Flags: " << Comedi::flagsToString(startCommand->flags)
            << "\n    Subdevice: " << startCommand->subdev
            << "\n    Number of data bytes: " << startCommand->data_len
            << "\n    Data address: " << startCommand->data
            << "\n    Number of channels: " << startCommand->chanlist_len
            << "\n    Channel list address: " << startCommand->chanlist
            << "\n    Scan start event: " << Comedi::scanBeginSrcToString(startCommand->scan_begin_src)
            << "\n    Scan start parameters: " << startCommand->scan_begin_arg
            << "\n    Scan end event: " << Comedi::scanEndSrcToString(startCommand->scan_end_src)
            << "\n    Scan end parameters: " << startCommand->scan_end_arg
            << "\n    Acquisition start event (start_src): " << Comedi::startSrcToString(startCommand->start_src)
            << "\n    acquisition-starting parameters (start_arg): " << startCommand->start_arg
            << "\n    Acquisition termination event (stop_src): " << Comedi::stopSrcToString(startCommand->stop_src)
            << "\n    Acquisition termiantion parameters (stop_arg): " << startCommand->stop_arg
            << "\n    Conversion start event (convert_src): " << Comedi::convertSrcToString(startCommand->convert_src)
            << "\n    Conversion start parameters (convert_src): " << startCommand->convert_arg;
        
        /*printf("Will use the following parameters to acquire data: \
            \n    Flags: << %i\
            \n    Subdevice: << %i\
            \n    Number of data bytes: << %i\
            \n    Data address: << %p\
            \n    Number of channels: << %i\
            \n    Channel list address: << %p\
            \n    Scan start event: << %i\
            \n    Scan start parameters: << %i\
            \n    Scan end event: << %i\
            \n    Scan end parameters: << %i\
            \n    Acquisition start event (start_src): << %i\
            \n    acquisition-starting parameters (start_arg): << %i\
            \n    Acquisition termination event (stop_src): << %i\
            \n    Acquisition termination parameters (stop_arg): << %i\
            \n    Conversion start event (convert_src): << %i\
            \n    Conversion start parameters (convert_arg): %i",
                    startCommand->flags,
                    startCommand->subdev,
                    startCommand->data_len,
                    startCommand->data,
                    startCommand->chanlist_len,
                    startCommand->chanlist,
                    startCommand->scan_begin_src,
                    startCommand->scan_begin_arg,
                    startCommand->scan_end_src,
                    startCommand->scan_end_arg,
                    startCommand->start_src,
                    startCommand->start_arg,
                    startCommand->stop_src,
                    startCommand->stop_arg,
                    startCommand->convert_src,
                    startCommand->convert_arg);*/
    }
    
    void stopCommand(comedi_t* device, int subdevice) {
        comedi_cancel(device, subdevice);
    }
            
}

