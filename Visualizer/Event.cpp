/* 
 * File:   Event.cpp
 * Author: lemelino
 * 
 * Created on January 21, 2015, 2:50 PM
 */

#include "Event.h"

Event::Event(int eventCode, NanosecondsTime timestamp) {
    _eventCode = eventCode;
    _timestamp = timestamp;
}

Event::Event(const Event& orig) {
    _eventCode = orig._eventCode;
    _timestamp = orig._timestamp;
}

Event::~Event() {
}

NanosecondsTime Event::getTimestamp() const {
    return _timestamp;
    
}

int Event::getEventCode() const {
    return _eventCode;
}

