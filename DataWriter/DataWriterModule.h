/* 
 * File:   DataWriter.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:20 PM
 */

#ifndef DATAWRITER_H
#define	DATAWRITER_H

#include "DataProcessor.h"
#include "../Common/BaseCommunicatingModule.h"
#include "../Common/MessageProcessor.h"

class DataWriterModule : public BaseCommunicatingModule {
public:
    
    /**
     * Constructor
     */
    DataWriterModule(zmq::context_t& cZmqContext, const std::string& cMessagesSocketAddress, 
            const std::string& cDataSocketAddress);
    
    /**
     * Disabled copy constructor.
     * @param orig
     */
    DataWriterModule(const DataWriterModule& orig);
    
    /**
     * Destructor.
     */
    virtual ~DataWriterModule();
    
    
    void onExitMessage();
    void onPauseMessage();
    void onStartMessage();
    void onStopMessage();
    StateMessage getCurrentState();

private:
    
    /**
     * Processes the data that was received and writes it and processes
     * control messages if any.  This is done by calling the composing objects
     * in order to have them process their own things.
     */
    void process();

    /**
     * The module that processes the data to be written to a file.
     */
    DataProcessor* _cDataProcessor;
    
    /**
     * The module that processes the incoming messages.
     */
    MessageProcessor* _cMessageProcessor;
    
    StateMessage _cCurrentState;
};

#endif	/* DATAWRITER_H */

