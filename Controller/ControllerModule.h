/* 
 * File:   ControllerModule.h
 * Author: lemelino
 *
 * Created on November 15, 2014, 9:32 PM
 */

#ifndef CONTROLLERMODULE_H
#define	CONTROLLERMODULE_H

#include <zmq.hpp>
#include <memory>

#include <string>
#include "../Common/BaseModule.h"
//#include "ControllerWindow.h"
#include "ControllerCommunicator.h"

class ControllerModule : public BaseModule {
public:
    ControllerModule(zmq::context_t& _cZmqContext, std::string cSocketAddress);
    ControllerModule(const ControllerModule& orig);
    virtual ~ControllerModule();
private:
    
    /**
     * Because we need to share this.
     */
    std::shared_ptr<ControllerCommunicator> _cControllerCommunicator;
    
    /**
     * Processes the user's input, refreshes the screen and fires the
     * correct signal to the other modules.
     */
    void process();
};

#endif	/* CONTROLLERMODULE_H */

