/* 
 * File:   BaseCommunicatingModule.h
 * Author: lemelino
 *
 * Created on December 24, 2014, 7:10 PM
 */

#ifndef BASECOMMUNICATINGMODULE_H
#define	BASECOMMUNICATINGMODULE_H

#include "BaseModule.h"
#include "MessageDef.h"

/**
 * This class should be the base class of all other modules that rely on
 * messages to react. Thus, all modules except the Controller should make use
 * of this.
 * 
 * We define an interface to react to the various messages sent by the
 * controller.
 */
class BaseCommunicatingModule : public BaseModule {
public:
    BaseCommunicatingModule();
    BaseCommunicatingModule(const BaseCommunicatingModule& orig);
    virtual ~BaseCommunicatingModule();
    
    virtual void onStartMessage() = 0;
    virtual void onStopMessage() = 0;
    virtual void onPauseMessage() = 0;
    virtual void onExitMessage() = 0;
    
    /**
     * Should return the module's current state.
     * @return The module's current state.
     */
    virtual StateMessage getCurrentState() = 0; 
};

#endif	/* BASECOMMUNICATINGMODULE_H */

