/* 
 * File:   DataProcessor.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:31 PM
 */

#include "FakeDataAcquirer.h"
#include <glog/logging.h>
#include <vector>
#include <string>
#include <sstream>
#include <time.h>
#include <random>
#include <iostream>
#include "../Common/ConfigurationManager.h"
#include "DataAcquirer.h"

FakeDataAcquirer::FakeDataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress) :
_cDataPublisherSocket(cZmqContext, ZMQ_PUB)
{
    
    _fCurrentFrameNumber = 0.0;
    
    _samplingRate = ConfigurationManager::getInstance()->getInt("SAMPLES_PER_SECOND");
    
    // Bind to the data socket.
    
    _cDataPublisherSocket.bind(cDataSocketAddress.c_str());
    
    // Sets up the fake data acquisition.
    
    setupDataAcquisition();
}

void FakeDataAcquirer::setupDataAcquisition() {
    
    /**
     * Number of channels / electrodes to use.  
     * Make sure that they are are sequentially plugged into the
     * amplifier beforehand, starting from '0'.
     */
    numberOfChannels = ConfigurationManager::getInstance()->
        getInt("NUMBER_OF_CHANNELS");
    LOG(INFO) << "Number of DAQ channels: " << numberOfChannels;
    
    // Measures the maximal/minimal values that should be measured. (in Volts)
    minVoltage = ConfigurationManager::getInstance()->
        getDouble("MIN_VOLTAGE");
    maxVoltage = ConfigurationManager::getInstance()->
        getDouble("MAX_VOLTAGE");
    
    LOG(INFO) << "Max/Min voltages: " << minVoltage << "/" << 
            maxVoltage << " Volts";
    
    /**
     * Sampling rate used.
     */
    samplesPerFrame = ConfigurationManager::getInstance()->
        getInt("SAMPLES_PER_FRAME");
    LOG(INFO) << "Samples per frame: " << samplesPerFrame;
    
    /**
     * Samples per second.
     */
    const int samplesPerSecond = ConfigurationManager::getInstance()->
            getInt("SAMPLES_PER_SECOND");
    LOG(INFO) << "Samples per second: " << samplesPerSecond;
    
    /**
     * If more than one National Instruments devices are plugged in, 
     * you might need to specify 'Dev2' or 'Dev3'
     */
    const std::string deviceToUse = ConfigurationManager::getInstance()->getString("NI_DEVICE_TO_USE");
    LOG(INFO) << "Device in use: FAKED";
    
    LOG(INFO) << "USING FAKE DATA ACQUIRER! NOT FOR PRODUCTION USE!";
}

FakeDataAcquirer::~FakeDataAcquirer() {
}

void FakeDataAcquirer::process() {
    
    // Prepares a transmission buffer.
    double* transmissionBuffer = prepareTransmissionBuffer();
    
    // Fills the transmission buffer with random data.
    fillTransmissionBuffer(transmissionBuffer);
    
    // Sends the frame.
    sendDataFrame(transmissionBuffer);
    
    // Deletes the buffer.
    delete[] transmissionBuffer;
}

double* FakeDataAcquirer::prepareTransmissionBuffer() {
    double* transmissionBuffer = new double[numberOfChannels * samplesPerFrame];

    if(transmissionBuffer == 0) {
        LOG(FATAL) << "Could not allocate memory for transmission buffer.";
    }
    
    // Zero out the buffer.
    
    memset(transmissionBuffer, 0.0, numberOfChannels * samplesPerFrame);
    
    return transmissionBuffer;
}

void FakeDataAcquirer::fillTransmissionBuffer(double* transmissionBuffer) {
    // Here, we generate random data!
    
    int nbSamples = samplesPerFrame * numberOfChannels;
    for(int i = 0; i < nbSamples; i++) {
        
        // Transmits the frame number on all elements of the buffer.
        transmissionBuffer[i] = _fCurrentFrameNumber;
    }
    
    // Finishes by incrementing the frame number.
    _fCurrentFrameNumber += 0.01;
    
    if(_fCurrentFrameNumber > 5.0) {
        _fCurrentFrameNumber = 0.0;
    }
    
    int oneSec = 1000000; //In microseconds
    int waitTine = (double)oneSec / ((double)_samplingRate / (double)samplesPerFrame);
    
    usleep(waitTine);
}

void FakeDataAcquirer::sendDataFrame(double* transmissionBuffer) {
    
    // Creates the message, reserving enough space for the whole buffer.
    
    int bufferSizeInBytes = sizeof(double) * samplesPerFrame * numberOfChannels;
    
    zmq::message_t msg((std::size_t) bufferSizeInBytes);
    
    // Copies the integer inside the state message.
    
    memcpy(msg.data(), transmissionBuffer, (std::size_t) bufferSizeInBytes);
    
    // Send away!
    
    _cDataPublisherSocket.send(msg);
}