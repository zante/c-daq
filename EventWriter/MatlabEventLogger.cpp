/* 
 * File:   MatlabEventLogger.cpp
 * Author: lemelino
 * 
 * Created on January 13, 2015, 4:43 PM
 */

#include <glog/logging.h>
#include <iomanip>
#include <fstream>

#include "MatlabEventLogger.h"

MatlabEventLogger::MatlabEventLogger(const std::string& fileToOpen) : 
    EventLogger(fileToOpen){
    
    outFileStream << "Latency\tType\tPosition\n";
    outFileStream.flush();
    
    if(!outFileStream.good()) {
        LOG(FATAL) << "Problem writing header information";
    }
}

MatlabEventLogger::~MatlabEventLogger() {
    outFileStream.flush();
    
    if(!outFileStream.good()) {
        LOG(FATAL) << "Problem writing header information";
    }
}

void MatlabEventLogger::writeEvent(int eventCode, 
            NanosecondsTime timestampInNanoseconds) {
    
    double timestampInSeconds = (double)timestampInNanoseconds / 
    (double)NANOSECONDS_PER_SECOND;
    
    // We write the latency in seconds, a string code for the event (E1),
    // and the numeric event code.
    
    outFileStream << std::setprecision(6) << timestampInSeconds << "\t" << "E" 
            << eventCode 
            << "\t" << eventCode << "\n";
    
    // Flushes the output stream.
    
    outFileStream.flush();
}

