/* 
 * File:   ConfigurationManager.cpp
 * Author: lemelino
 * 
 * Created on December 27, 2014, 1:35 AM
 */

#include <fstream>
#include <iostream>
#include <streambuf>

#include "ConfigurationManager.h"

#include "CommonDef.h"

// Singleton instance
ConfigurationManager* ConfigurationManager::_cInstance = 0;

/**
 * Returns the singelton instance of the class.
 * @return The Configuration manager.
 */
ConfigurationManager* ConfigurationManager::getInstance() {
    
    if(_cInstance == 0) {
        _cInstance = new ConfigurationManager(CONFIG_FILE);
    }
    
    return _cInstance;
}

ConfigurationManager::ConfigurationManager(const std::string& strConfigFile) {
    
    // Reads the whole file to a string
    
    std::string configString = readFile(strConfigFile);
    
    // Parses the string to build the document.
    
    jsonDoc.Parse(configString.c_str());
    
    // Checks for parsing errors.
    
    if(jsonDoc.HasParseError()) {
        LOG(ERROR) << "Error parsing configuration file: " << 
                jsonDoc.GetParseError();
    }
}

ConfigurationManager::~ConfigurationManager() {
}

std::string ConfigurationManager::readFile(const std::string& strConfigFile) {
    
    // Creates Input stream.
    std::ifstream cConfigFile(strConfigFile);
    
    // Final string to be returned.
    std::string strJsonContents;
    
    //Makes sure that the file has been opened.  Otherwise, log fatally.
    if(!cConfigFile.is_open()) {
        LOG(FATAL) << "Could not open config file '" << strConfigFile << "'.";
    }
    
    // Reserves the space for the string to be read.
    cConfigFile.seekg(0, std::ios::end);   
    strJsonContents.reserve(cConfigFile.tellg());
    cConfigFile.seekg(0, std::ios::beg);

    // Copy the contents of the file to the string.
    strJsonContents.assign((std::istreambuf_iterator<char>(cConfigFile)),
                            std::istreambuf_iterator<char>());

    // Checks of reading was successful.
    if (!cConfigFile)
      LOG(FATAL) << "Error: only " << cConfigFile.gcount() << " could be read from "
              "configuration file";
    
    // Closes the file.
    cConfigFile.close();
    
    return strJsonContents;
}

bool ConfigurationManager::contains(std::string key) {
    // Verifies that the key and value pair exist.
    return jsonDoc.HasMember(key.c_str());
}

rapidjson::Value& ConfigurationManager::getValue(std::string& key) {
    
    // Verifies that the key and value pair exist.
    if(!contains(key)) {
        LOG(FATAL) << "Cannot retrieve value '" << key << "' : key does not exist";
    }
    
    // Return the raw value item.  If it does not exist, the log will already
    // have crashed the program.
    return jsonDoc[key.c_str()];
}

std::string ConfigurationManager::getString(std::string key) {
    rapidjson::Value& val = getValue(key);
    return val.GetString();
}

int ConfigurationManager::getInt(std::string key) {
    rapidjson::Value& val = getValue(key);
    return val.GetInt();
}

double ConfigurationManager::getDouble(std::string key) {
    rapidjson::Value& val = getValue(key);
    return val.GetDouble();
}

bool ConfigurationManager::getBoolean(std::string key) {
    rapidjson::Value& val = getValue(key);
    return val.GetBool();
}

std::vector<std::string> ConfigurationManager::getStringArray(std::string key) {
    rapidjson::Value& val = getValue(key);
    std::vector<std::string> returnArray;
    
    for(rapidjson::SizeType i = 0; i < val.Size(); i++) {
        std::string strToAdd(val[i].GetString());
        returnArray.push_back(strToAdd);
    }
    
    return returnArray;
}
