/* 
 * File:   VisualizerWindow.h
 * Author: lemelino
 *
 * Created on January 15, 2015, 3:13 PM
 */

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "../Common/Timestamper.h"
#include "../Common/CommonDef.h"
#include "DataModel.h"
#include "EventModel.h"

#ifndef VISUALIZERWINDOW_H
#define	VISUALIZERWINDOW_H

#define ELECTRODES_LEFT_OFFSET 2
#define ELECTRODES_RIGHT_OFFSET 8
#define ELECTRODES_TOP_OFFSET 2

#define EVENTS_NAME_SPACE 25

class VisualizerWindow {
public:
    VisualizerWindow();
    VisualizerWindow(const VisualizerWindow& orig);
    virtual ~VisualizerWindow();

    void addDataFrame(double* dataChunk, int nbElementsInBuffer);
    void addEvent(int eventCode);
    void work();
    
private:
    
    void loadElectrodesNames();
    void initializeGraphics(int screenWidth, int screenHeight);
    void initializeText();
    void drawElectrodesNames(int nbChannels);
    void getDimensionsOfLargestElectrodeName(SDL_Rect* largestDimensions);
    
    void drawEverything();

    unsigned int _nbChannels;
    unsigned int _visualizationPointsCompressionRatio;
    int _verticalElectrodeSpace;
    
    //The window we'll be rendering to.
    SDL_Window* gWindow;
    SDL_Surface* gScreenSurface;
    SDL_Renderer* renderer;
    TTF_Font* ttfFont;
    
    SDL_Rect rectangleScreenSurface;
    SDL_Rect electrodesRect;
    SDL_Rect _oneElectrodeRect;
    
    double _maxVoltValue;
    double _minVoltValue;
    
    Timestamper _timestamper;
    NanosecondsTime _nanoSecondsDisplayed;
    
    std::vector<std::string> electrodes;
    DataModel* _dataModel;
    EventModel* _eventModel;
    
    bool _redrawRequired;
};

#endif	/* VISUALIZERWINDOW_H */

