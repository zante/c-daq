/* 
 * File:   DataWriter.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:20 PM
 */

#include <glog/logging.h>
#include <zmq.hpp>
#include "VisualizerModule.h"
#include "../Common/ConfigurationManager.h"

VisualizerModule::VisualizerModule(zmq::context_t& cZmqContext, 
        const std::string& cMessageSocketAddress, 
        const std::string& cDataSocketAddress,
        const std::string& cEventsSocketAddress) :
        
_cDataSubscriberSocket(cZmqContext, ZMQ_SUB),
_cEventSubscriberSocket(cZmqContext, ZMQ_SUB) {
    
    _iNumberOfChannels = ConfigurationManager::getInstance()
            ->getInt("NUMBER_OF_CHANNELS");
    _iSamplesPerFrame = ConfigurationManager::getInstance()
            ->getInt("SAMPLES_PER_FRAME");
    
    _cCurrentState = STOP;
    
    _cMessageProcessor = new MessageProcessor(*this, cZmqContext, cMessageSocketAddress);
    
    setupDataPipe(cDataSocketAddress);
    setupEventPipe(cEventsSocketAddress);
    
    //TODO: Re-enable!
    /*_cEventModel = new EventModel();
    _cDataModel = new DataModel();
    */
    
    _window = new VisualizerWindow;
}

VisualizerModule::~VisualizerModule() {
    
    delete _window;
    _window = NULL;
    
    _cDataSubscriberSocket.close();
    _cEventSubscriberSocket.close();
    
    if(_cMessageProcessor != NULL) {
        delete _cMessageProcessor;
        _cMessageProcessor = NULL;
    }
}

void VisualizerModule::process() {

    _cMessageProcessor->process();
    
    handleNewData();
    handleNewEvents();
    
    _window->work();
}

void VisualizerModule::onExitMessage() {
    LOG(INFO) << "Exiting Visualizer!";
    //_cEventProcessor->handleStateChange(EXIT);
    _cCurrentState = EXIT;
    this->setShouldExit();
}

void VisualizerModule::onPauseMessage(){
    //_cEventProcessor->handleStateChange(PAUSE);
    _cCurrentState = PAUSE;
    LOG(INFO) << "Pausing Visualizer!";    
}

void VisualizerModule::onStartMessage(){
    //_cEventProcessor->handleStateChange(START);
    _cCurrentState = START;
    LOG(INFO) << "Starting Visualizer!";    
}

void VisualizerModule::onStopMessage(){
    //_cEventProcessor->handleStateChange(STOP);
    _cCurrentState = STOP;
    LOG(INFO) << "Stopping Visualizer!";    
}

StateMessage VisualizerModule::getCurrentState() {
    return _cCurrentState;
}

void VisualizerModule::setupDataPipe(const std::string& dataSocketAddress) {
    
    _cDataSubscriberSocket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    
    // Connect to the Publisher.    
    _cDataSubscriberSocket.connect(dataSocketAddress.c_str());
}

void VisualizerModule::setupEventPipe(const std::string& eventSocketAddress) {
    
    _cEventSubscriberSocket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    
    // Connect to the Publisher.    
    _cEventSubscriberSocket.connect(eventSocketAddress.c_str());
}

void VisualizerModule::handleNewData() {
        
    // Allocates space to receive a data frame message.
    int nbElementsInBuffer = _iNumberOfChannels * _iSamplesPerFrame;
    
    // Calculates the size of the array in bytes from the number of elements.
    int sizeOfDataFrame = sizeof(double) * nbElementsInBuffer;
    
    double* receivedBuffer = new double[nbElementsInBuffer];

    // Allocates message with given size.
    zmq::message_t msg(sizeOfDataFrame);
    
    // Tries to receive a message.
    bool hasMessage = _cDataSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    
    // While there are messages to process...
    while(hasMessage) {
        
        // Copy the contents of the message to a buffer.
        memcpy((void*)receivedBuffer, msg.data(), sizeOfDataFrame);
        
        // Process the received contents.
        processDataFrame(receivedBuffer, nbElementsInBuffer);
        
        // Tries to receive a new message.
        hasMessage = _cDataSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    }
    
    // Error testing.
    if( !hasMessage && errno == EAGAIN) {
        //Ignore, there was nothing to receive.
    }
    else if(!hasMessage){
        //We have encountered an error.
        throw std::string("Problem receiving data.");
    }
    
    // Deletes the buffer.
    delete[] receivedBuffer;
}

void VisualizerModule::handleNewEvents() {
    int receivedEventCode = -1;
    
    int sizeOfMessage = sizeof(int);

    // Allocates message with given size.
    zmq::message_t msg(sizeOfMessage);
    
    // Tries to receive a message.
    bool hasMessage = _cEventSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    
    // While there are messages to process...
    while(hasMessage) {
        
        // Copy the contents of the message to a buffer.
        memcpy((void*)(&receivedEventCode), msg.data(), sizeOfMessage);
        
        LOG(INFO) << "Received: <" << receivedEventCode << ">";
        
        // Process the received contents.
        processSingleEvent(receivedEventCode);
        
        // Tries to receive a new message.
        hasMessage = _cEventSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    }
    
    // Error testing.
    if( !hasMessage && errno == EAGAIN) {
        //Ignore, there was nothing to receive.
    }
    else if(!hasMessage){
        //We have encountered an error.
        throw std::string("Problem receiving events.");
    }
}

void VisualizerModule::processDataFrame(double* dataChunk, 
        int nbElementsInBuffer) {

    // Add frame to data model.
    _window->addDataFrame(dataChunk, nbElementsInBuffer);
}

void VisualizerModule::processSingleEvent(int eventCode) {

    _window->addEvent(eventCode);
}