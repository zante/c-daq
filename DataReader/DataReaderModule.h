/* 
 * File:   DataWriter.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:20 PM
 */

#ifndef DATAREADER_H
#define	DATAREADER_H

#include "../Common/BaseCommunicatingModule.h"
#include "../Common/MessageProcessor.h"
#include "FakeDataAcquirer.h"
#include "DataAcquirer.h"
#include "ComediDataAcquirer.h"

class DataReaderModule : public BaseCommunicatingModule {
public:
    
    /**
     * Constructor
     */
    DataReaderModule(zmq::context_t& cZmqContext, 
            const std::string& cMessagesSocketAddress, 
            const std::string& cDataSocketAddress);
    
    /**
     * Disabled copy constructor.
     * @param orig
     */
    DataReaderModule(const DataReaderModule& orig);
    
    /**
     * Destructor.
     */
    virtual ~DataReaderModule();
    
    
    void onExitMessage();
    void onPauseMessage();
    void onStartMessage();
    void onStopMessage();
    
    StateMessage getCurrentState();

private:
    
    /**
     * Processes the data that was received and writes it and processes
     * control messages if any.  This is done by calling the composing objects
     * in order to have them process their own things.
     */
    void process();

    /**
     * The module that processes the data to be written to a file.
     */
    
#ifdef TESTING_MODE
    FakeDataAcquirer* _cDataAcquirer;
#else
    ComediDataAcquirer* _cDataAcquirer;
#endif
    
    /**
     * The module that processes the incoming messages.
     */
    MessageProcessor* _cMessageProcessor;
    
    /**
     * Current module's state.
     */
    StateMessage _cCurrentState;
};

#endif	/* DATAREADER_H */

