/* 
 * File:   Timestamper.cpp
 * Author: lemelino
 * 
 * Created on January 11, 2015, 10:14 PM
 */

#include <glog/logging.h>
#include "Timestamper.h"

Timestamper::Timestamper() {
    _timestamperState = T_STOPPED;
}

Timestamper::~Timestamper() {
    if(_timestamperState == T_STARTED) {
        stopTimer();
    }
}

void Timestamper::startTimer() {
    
    if(_timestamperState == T_STOPPED) {
        timespec startTime;
        clock_gettime(CLOCK_REALTIME, &startTime);
        
        _startTimeTimeInNano = timespecToNanoseconds(startTime);

        LOG(INFO) << "Starting clock at time " << _startTimeTimeInNano << " ns.";
        
        _timestamperState = T_STARTED;
    }
    else {
        LOG(FATAL) << "Error, timestamper is already started!";
    }
}

void Timestamper::resetTimer() {
    
    if(_timestamperState == T_STARTED) {
        timespec startTime;
        clock_gettime(CLOCK_REALTIME, &startTime);
        
        NanosecondsTime nsNewStartTime = timespecToNanoseconds(startTime);

        LOG(INFO) << "Resetting clock from time " << _startTimeTimeInNano << 
                " ns to " << nsNewStartTime << " ns.";

        _startTimeTimeInNano = nsNewStartTime;
    }
    else {
        LOG(FATAL) << "Error, timestamper is stopped, cannot reset!";
    }
}

void Timestamper::stopTimer() {
    
    if(_timestamperState == T_STARTED) {
        timespec stopTime;
        clock_gettime(CLOCK_REALTIME, &stopTime);
        
        NanosecondsTime nsStopTime = timespecToNanoseconds(stopTime);

        LOG(INFO) << "Stopping clock at time " << nsStopTime << " ns.";

        _startTimeTimeInNano = 0;
        
        _timestamperState = T_STOPPED;
    }
    else {
        LOG(FATAL) << "Error, timestamper is stopped, cannot stop again!";
    }
}

bool Timestamper::isStarted() {
    return (_timestamperState == T_STARTED);
}

NanosecondsTime Timestamper::getCurrentNanoTime() {
    
    if(_timestamperState == T_STARTED) {
        timespec currentTimeTs;
        clock_gettime(CLOCK_REALTIME, &currentTimeTs);
        
        NanosecondsTime nanoCurrentTime = timespecToNanoseconds(currentTimeTs);

        return timespecToNanosecondsSinceStart(nanoCurrentTime, _startTimeTimeInNano);
    }
    else {
        return 0;
    }
}

NanosecondsTime Timestamper::timespecToNanosecondsSinceStart (
        NanosecondsTime currentTime, 
        NanosecondsTime startTimeInNano) {
    
    return currentTime - startTimeInNano;
}

NanosecondsTime Timestamper::timespecToNanoseconds(timespec& ts) {
    
    NanosecondsTime timeToNano = ts.tv_nsec + 
            ts.tv_sec * NANOSECONDS_PER_SECOND;
    
    return timeToNano;
}
