/* 
 * File:   DataModel.cpp
 * Author: lemelino
 * 
 * Created on January 20, 2015, 9:40 PM
 */

#include <glog/logging.h>

#include "DataModel.h"

DataModel::DataModel(int nbChannels, int maxNbElementsPerChannel) {
    for(int i = 0; i < nbChannels; i++) {
        std::deque<float>* doubleEnded = new std::deque<float>;
        this->_internalArray.push_back(doubleEnded);
    }
    
    this->_nbChannels = nbChannels;
    this->_maxNbElementsPerChannel = maxNbElementsPerChannel;
    this->_currentNbOfElementsPerChannel = 0;
}

DataModel::~DataModel() {
    for(unsigned int i = 0; i < _nbChannels; i++) {
        delete this->_internalArray[i];
    }
}

void DataModel::addDataFrame(float* dataChunk, 
        unsigned int nbChannels, 
        unsigned int nbElementsPerChannel) {
    
    bool overflowing = false;
    
    if(_nbChannels != nbChannels) {
        LOG(FATAL) << "Number of channels mismatch.";
    }
    
    // Checks if we would go over the bounds.
    unsigned int futureNbOfElements = 
            _currentNbOfElementsPerChannel + nbElementsPerChannel;
    
    // IF we go overbound
    if(futureNbOfElements > _maxNbElementsPerChannel) {
        unsigned int elementsToPop = futureNbOfElements - _maxNbElementsPerChannel;
        
        // Pop a few elements
        for(unsigned int j = 0; j < _nbChannels; j++) {
            for(unsigned int i = 0; i < elementsToPop; i++) {
                _internalArray[j]->pop_front();
            }
        }
        
        overflowing = true;
    }
    
    // Append the new elements.
    for(unsigned int i = 0; i < nbChannels; i++) {
        for(unsigned int j = 0; j < nbElementsPerChannel; j++) {
            this->_internalArray[i]->push_back( dataChunk[ i * nbElementsPerChannel + j ] );
        }
    }
    
    // Add the number of elements to the object.
    if(overflowing) {
        this->_currentNbOfElementsPerChannel = _maxNbElementsPerChannel;
    } else {
        this->_currentNbOfElementsPerChannel += nbElementsPerChannel;
    }
}

std::deque<float>::const_reverse_iterator DataModel::getIteratorForChannel(int noChannel) {
    return _internalArray[noChannel]->crbegin();
}

std::deque<float>::const_reverse_iterator DataModel::getIteratorEndForChannel(int noChannel) {
    return _internalArray[noChannel]->crend();
}

size_t DataModel::getSize() {
    return _currentNbOfElementsPerChannel;
}

