/* 
 * File:   BaseModule.h
 * Author: lemelino
 *
 * Created on November 15, 2014, 12:05 AM
 */

#ifndef BASEMODULE_H
#define	BASEMODULE_H

//#include <boost/thread/thread.hpp>

/**
 * Base for all of the other modules.  Implements a basic interface
 * to start, stop, execute a main loop and terminate itself.
 */
class BaseModule {
public:
    BaseModule();
    BaseModule(const BaseModule& orig);
    virtual ~BaseModule();
    
    /**
     * Used to launch the main loop of the module.
     */
    void launch();
    
    /**
     * Used to tell the module to terminate.
     */
    void setShouldExit();
    
    /**
     * Tells if the module has been set to terminate.
     * @return 
     */
    bool isShouldExit();
    
private:

    /**
     * Executes a single iteration of processing.
     */
    virtual void process() = 0;

    bool _bShouldExit;
};

#endif	/* BASEMODULE_H */

