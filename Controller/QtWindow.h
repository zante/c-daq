/* 
 * File:   QtWindow.h
 * Author: root
 *
 * Created on January 31, 2015, 2:57 PM
 */

#ifndef QTWINDOW_H
#define	QTWINDOW_H

#include <qt5/QtWidgets/QMainWindow>
#include <qt5/QtWidgets/QLabel>
#include <qt5/QtWidgets/QPushButton>
#include <qt5/QtWidgets/QVBoxLayout>
#include <qt5/QtWidgets/QSlider>
#include <memory>

#include "ControllerCommunicator.h"

enum ControllerState {STOPPED, RECORDING, PAUSED};

class QtWindow : public QMainWindow {
    Q_OBJECT
public:
    QtWindow(std::shared_ptr<ControllerCommunicator> controllerCommunicator);
    QtWindow(const QtWindow& orig);
    virtual ~QtWindow();

    private slots:
        
    //Signal handlers
    void on_startButton_clicked();
    void on_stopButton_clicked();
    void on_scaleSlider_changed();
    
private:
    bool canChangeState(ControllerState newState);
    bool changeState(ControllerState newState);
    
    QLabel* _stateLabel;
    QPushButton* _startButton;
    QPushButton* _stopButton;
    QLabel* _scaleLabel;
    QSlider* _scaleSlider;

    // Because we need to share this.
    std::shared_ptr<ControllerCommunicator> _cControllerCommunicator;
    
    // Current state of the system.
    ControllerState _cControllerState;
};

#endif	/* QTWINDOW_H */

