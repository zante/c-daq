/* 
 * File:   DataWriter.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:20 PM
 */

#include <glog/logging.h>

#include "../Common/CommonDef.h"
#include "DataReaderModule.h"
//#include "FakeDataAcquirer.h"
//#include "DataAcquirer.h"
//#include "ZmqDataAcquirer.h"

DataReaderModule::DataReaderModule(zmq::context_t& cZmqContext, 
        const std::string& cMessageSocketAddress, 
        const std::string& cDataSocketAddress) {
    
    // Begins in the 'stopped' state.
    _cCurrentState = STOP;
    
    // Initializes a data acquisition module.
    
#ifdef TESTING_MODE
    _cDataAcquirer = new FakeDataAcquirer(cZmqContext, cDataSocketAddress);
#else
    _cDataAcquirer = new ComediDataAcquirer(cZmqContext, cDataSocketAddress);
        
#endif
    
    // Initializes message handler.
    _cMessageProcessor = new MessageProcessor(*this, cZmqContext, cMessageSocketAddress);
}

DataReaderModule::~DataReaderModule() {
    
    // Deletes the data acquisition module.
    if(_cDataAcquirer != NULL) {
        delete _cDataAcquirer;
        _cDataAcquirer = NULL;
    }
    
    // Deletes the message processing module.
    if(_cMessageProcessor != NULL) {
        delete _cMessageProcessor;
        _cMessageProcessor = NULL;
    }
}

void DataReaderModule::process() {
    _cDataAcquirer->process();
    _cMessageProcessor->process();
}

void DataReaderModule::onExitMessage() {
    LOG(INFO) << "Exiting DataReader!";
    _cCurrentState = EXIT;
    this->setShouldExit();
}

void DataReaderModule::onPauseMessage(){
    LOG(INFO) << "Pausing DataReader!";  
    _cCurrentState = PAUSE;
}

void DataReaderModule::onStartMessage(){
    LOG(INFO) << "Starting DataReader!";
    _cCurrentState = START;
}

void DataReaderModule::onStopMessage(){
    LOG(INFO) << "Stopping DataReader!";   
    _cCurrentState = STOP;
}

StateMessage DataReaderModule::getCurrentState() {
    return _cCurrentState;
}
