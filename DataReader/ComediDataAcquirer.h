/* 
 * File:   ComediDataAcquirer.h
 * Author: acquisition
 *
 * Created on March 12, 2015, 2:54 PM
 */

#ifndef COMEDIDATAACQUIRER_H
#define	COMEDIDATAACQUIRER_H

#include <errno.h>
#include <string>
#include <comedilib.h>

#include "DataAcquirer.h"

class ComediDataAcquirer {
public:
    /**
     * Constructor
     */
    ComediDataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress);
    
    /**
     * Copy Constructor.  Disabled.
     * @param orig
     */
    ComediDataAcquirer(const ComediDataAcquirer& orig);
    
    /**
     * Destructor
     */
    virtual ~ComediDataAcquirer();
    
    /**
     * Main function.  Receives and writes the data received by this
     * module.
     */
    void process();
    
private:
    
    /**
     * Creates and connects the data pipe in order to receive data.
     */
    void setupDataAcquisition();
    
    /**
     * Prepares a buffer to transmit a single frame of acquired data.
     */
    double* prepareTransmissionBuffer();
    
    /**
     * Fills the transmission buffer.
     */
    void fillTransmissionBuffer(double* transmissionBuffer, size_t bufferSizeInBytes);
    
    /**
     * Sends the data frame.
     */
    void sendDataFrame(double* transmissionBuffer);
    
    /**
     * ZMQ Socket used to receive data.
     */
    zmq::socket_t _cDataPublisherSocket;
    
    /**
     * Number of channels to receive from.
     */
    int numberOfChannels;
    
    /**
     * Samples per second
     */
    int samplesPerFrame;
    int samplesPerSecond;
    
    /**
     * Comedi device file
     */
    comedi_t *comediDevice;
    
    size_t bytesPerSample;
    
    int comediSubdevice;
    
    comedi_range* channelRangeInfo;
    lsampl_t* channelMaximums;
    
};

#endif	/* COMEDIDATAACQUIRER_H */

