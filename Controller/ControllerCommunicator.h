/* 
 * File:   ControllerCommunicator.h
 * Author: lemelino
 *
 * Created on November 16, 2014, 8:17 PM
 */

#ifndef CONTROLLERCOMMUNICATOR_H
#define	CONTROLLERCOMMUNICATOR_H

#include <zmq.hpp>
#include <string>
#include "../Common/MessageDef.h"

class ControllerCommunicator {
public:
    ControllerCommunicator(zmq::context_t& cZmqContext, std::string& cSocketAddress);
    ControllerCommunicator(const ControllerCommunicator& orig);
    virtual ~ControllerCommunicator();
    
    /**
     * Sends the STOP message to all other modules via the messaging socket.
     */
    void sendStopMessage();
    
    /**
     * Sends the PAUSE message to all other modules via the messaging socket.
     */
    void sendPauseMessage();
    
    /**
     * Sends the START message to all other modules via the messaging socket.
     */
    void sendStartMessage();
    
    /**
     * Sends the EXIT message to all other modules via the messaging socket.
     */
    void sendExitMessage();
    
    void sendScaleMessage(int scalingFactor);

private:
    
    /**
     * Executes the sending of the integer via the messaging socket.
     * @param stateMessage Message code to be sent to the other modules.
     */
    void _sendMessage(StateMessage stateMessage);
    
    /**
     * ZMQ Publisher socket.
     */
    zmq::socket_t _cPublisherSocket;
};

#endif	/* CONTROLLERCOMMUNICATOR_H */

