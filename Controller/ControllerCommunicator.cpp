/* 
 * File:   ControllerCommunicator.cpp
 * Author: lemelino
 * 
 * Created on November 16, 2014, 8:17 PM
 */

#include <glog/logging.h>
#include <cstddef>

#include "ControllerCommunicator.h"
#include "../Common/MessageDef.h"

ControllerCommunicator::ControllerCommunicator(zmq::context_t& cZmqContext, 
        std::string& cSocketAddress) :
    _cPublisherSocket(cZmqContext, ZMQ_PUB) {
    
    // Binds the ZMQ context.
    
    _cPublisherSocket.bind(cSocketAddress.c_str());
}

ControllerCommunicator::~ControllerCommunicator() {
}

void ControllerCommunicator::sendStopMessage() {
    LOG(INFO) << "Sending the STOP message.";
    
    _sendMessage(STOP);
}

void ControllerCommunicator::sendPauseMessage() {
    LOG(INFO) << "Sending the PAUSE message.";
    
    _sendMessage(PAUSE);
}

void ControllerCommunicator::sendStartMessage(){
    LOG(INFO) << "Sending the START message.";
    
    _sendMessage(START);
}

void ControllerCommunicator::sendExitMessage(){
    LOG(INFO) << "Sending the EXIT message.";
    
    _sendMessage(EXIT);
}

void ControllerCommunicator::sendScaleMessage(int scaleFactor) {
    LOG(INFO) << "Sending a scaling message.";
    
    _sendMessage(SCALING);
    _sendMessage(scaleFactor);
}

void ControllerCommunicator::_sendMessage(StateMessage stateMessage) {
    
    LOG(INFO) << "Sending <" << stateMessage << ">...";
    
    // Creates the message, reserving enough space for the integer.
    
    zmq::message_t msg((std::size_t) sizeof(stateMessage));
    
    // Copies the integer inside the state message.
    
    memset(msg.data(), stateMessage, (std::size_t) sizeof(stateMessage));
    
    // Send away!
    
    _cPublisherSocket.send(msg);
}
