/* 
 * File:   MatlabWriter.h
 * Author: lemelino
 *
 * Created on December 31, 2014, 3:18 PM
 */

#ifndef DUMPWRITER_H_
#define	DUMPWRITER_H_

#include <string>
#include <fstream>
#include <iostream>

class DumpWriter {
public:
    static void saveResults(std::string fileName, double* dataMatrix, 
            unsigned int numberOfChannels, unsigned long samplesPerChannel);
    static void saveResults(std::string fileName, float* dataMatrix, 
            unsigned int numberOfChannels, unsigned long samplesPerChannel);
};

#endif	/* DUMPWRITER_H_ */

