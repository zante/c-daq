/* 
 * File:   DataWriter.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:20 PM
 */

#ifndef VISUALIZER_MODULE_H
#define	VISUALIZER_MODULE_H

#include "VisualizerModule.h"
#include "../Common/BaseCommunicatingModule.h"
#include "../Common/MessageProcessor.h"
#include "../Common/Timestamper.h"
#include "VisualizerWindow.h"

class VisualizerModule : public BaseCommunicatingModule {
public:
    
    /**
     * Constructor
     */
    VisualizerModule(zmq::context_t& cZmqContext, 
        const std::string& cMessageSocketAddress, 
        const std::string& cDataSocketAddress,
        const std::string& cEventsSocketAddress);
    
    /**
     * Disabled copy constructor.
     * @param orig
     */
    VisualizerModule(const VisualizerModule& orig);
    
    /**
     * Destructor.
     */
    virtual ~VisualizerModule();
    
    
    void onExitMessage();
    void onPauseMessage();
    void onStartMessage();
    void onStopMessage();
    StateMessage getCurrentState();

private:
    
    /**
     * Processes the data that was received and writes it and processes
     * control messages if any.  This is done by calling the composing objects
     * in order to have them process their own things.
     */
    void process();
    
    void setupDataPipe(const std::string& dataSocketAddress);
    void setupEventPipe(const std::string& eventSocketAddress);
    
    void handleNewData();
    void processDataFrame(double* dataChunk, 
            int nbElementsInBuffer);
    
    void handleNewEvents();
    void processSingleEvent(int eventCode);
    
    void drawEverything();
    
    int _iNumberOfChannels;
    int _iSamplesPerFrame;

    zmq::socket_t _cDataSubscriberSocket;
    zmq::socket_t _cEventSubscriberSocket;

    MessageProcessor* _cMessageProcessor;
    
    StateMessage _cCurrentState;
    VisualizerWindow* _window;
};

#endif	/* VISUALIZER_MODULE_H */

