/* 
 * File:   Window.h
 * Author: lemelino
 *
 * Created on November 15, 2014, 10:49 PM
 */

#ifndef WINDOW_H
#define	WINDOW_H

#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include "ControllerCommunicator.h"
#include <memory>

enum ControllerState {STOPPED, RECORDING, PAUSED};

class ControllerWindow : public Gtk::Window {
public:
    ControllerWindow(std::shared_ptr<ControllerCommunicator> controllerCommunicator);
    virtual ~ControllerWindow();
private:
    
    //Signal handlers
    void on_startButton_clicked();
    void on_pauseButton_clicked();
    void on_stopButton_clicked();
    
    /**
     * Verifies that we can change from the current state to the one specified.
     * @param cState State that we wish to change to.
     * @return True if we can change states, false otherwise.
     */
    bool canChangeState(ControllerState cState);
    
    /**
     * Tries to change state to the the one specified.
     * @param cState State to change to.
     * @return True if operation was successful, false otherwise.
     */
    bool changeState(ControllerState cState);
    
    //Member widgets
    Gtk::Box _cBox;
    Gtk::Button _cStartButton;
    Gtk::Button _cPauseButton;
    Gtk::Button _cStopButton;
    Gtk::Label _cStateLabel;
    
    // Current state of the system.
    ControllerState _cControllerState;
    
    // Because we need to share this.
    std::shared_ptr<ControllerCommunicator> _cControllerCommunicator;
};

#endif	/* WINDOW_H */

