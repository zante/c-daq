#!/bin/sh

#CMake
sudo apt-get -y install cmake

# Google Logger
sudo apt-get -y install libgoogle-glog-dev libgoogle-glog-doc libgoogle-glog0

## GTK+ (C++ bindings)
#sudo apt-get -y install libgtkmm-3.0-1 libgtkmm-3.0-dbg libgtkmm-3.0-dev libgtkmm-3.0-doc
#sudo apt-get -y install libglibmm-2.4-1c2a libglibmm-2.4-dbg libglibmm-2.4-dev libglibmm-2.4-doc

# QT (C++)
sudo apt-get -y install qt5-default libqt5core5a libqt5gui5 libqt5widgets5

# ZMQ for communication tasks
sudo apt-get -y install libzmq3-dbg libzmq3-dev

# For matlab matrix files
sudo apt-get -y install libmatio-dev libmatio-doc libmatio2

# For the Visualizer
sudo apt-get -y install libsdl2-*

# For tools
sudo apt-get -y install valgrind kcachegrind

# Comedi drivers
sudo apt-get -y install libcomedi-dev

# Realtime kernel
sudo apt-get -y install linux-tools-lowlatency linux-image-lowlatency linux-headers-lowlatency

echo "Adding ni_pcmio and comedi drivers to /etc/modules..."

sudo touch /etc/modules
sudo su -c "echo 'comedi' >> /etc/modules"
sudo su -c "echo 'ni_pcimio' >> /etc/modules"

echo "Done!"

