/* 
 * File:   remoteDataAcquirer.h
 * Author: root
 *
 * Created on January 31, 2015, 12:49 PM
 */

#ifndef REMOTEDATAACQUIRER_H
#define	REMOTEDATAACQUIRER_H


#include <zmq.hpp>
#include "NIDAQmxBase.h"

class RemoteDataAcquirer {
public:
    /**
     * Constructor
     */
    RemoteDataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress);
    
    /**
     * Copy Constructor.  Disabled.
     * @param orig
     */
    RemoteDataAcquirer(const RemoteDataAcquirer& orig);
    
    /**
     * Destructor
     */
    virtual ~RemoteDataAcquirer();
    
    /**
     * Main function.  Receives and writes the data received by this
     * module.
     */
    void process();
    
private:
    
    /**
     * Creates and connects the data pipe in order to receive data.
     */
    void setupDataAcquisition();
    
    /**
     * Prepares a buffer to transmit a single frame of acquired data.
     */
    double* prepareTransmissionBuffer();
    
    /**
     * Fills the transmission buffer.
     */
    void fillTransmissionBuffer(double* transmissionBuffer);
    
    /**
     * Sends the data frame.
     */
    void sendDataFrame(double* transmissionBuffer);
    
    /**
     * ZMQ Socket used to receive data.
     */
    zmq::socket_t _cDataPublisherSocket;
    
    /**
     * Handle for the NI-DAQ acquisition task.
     */
    TaskHandle taskHandle;
    
    /**
     * Number of channels to receive from.
     */
    int numberOfChannels;
    
    /**
     * Samples per second
     */
    int samplesPerFrame;
    
};

#endif	/* REMOTEDATAACQUIRER_H */

