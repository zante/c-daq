/* 
 * File:   DataWriter.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:20 PM
 */

#include <glog/logging.h>
#include "EventAcquisitionModule.h"
#include "../Common/CommonDef.h"
#include "ParallelPortEventAcquirer.h"
#include "FakeEventAcquirer.h"

EventAcquisitionModule::EventAcquisitionModule(zmq::context_t& cZmqContext, 
        const std::string& cMessageSocketAddress, const std::string& cEventAcquisitionSocket) {
    
    // Initializes a data acquisition module.
    
    // Initializes message handler.
    _cMessageProcessor = new MessageProcessor(*this, cZmqContext, cMessageSocketAddress);
    
#ifdef TESTING_MODE
    LOG(INFO) << "FAKING EVENT ACQUISITION.  NOT FOR PRODUCTION USE.";
    //_cEventAcquirer = new FakeEventAcquirer(cZmqContext, cEventAcquisitionSocket);
    _cEventAcquirer = new ParallelPortEventAcquirer(cZmqContext, 
            cEventAcquisitionSocket);
#else
    _cEventAcquirer = new ParallelPortEventAcquirer(cZmqContext, cEventAcquisitionSocket);
#endif
}

EventAcquisitionModule::~EventAcquisitionModule() {
    
    // Deletes the message processing module.
    if(_cMessageProcessor != NULL) {
        delete _cMessageProcessor;
        _cMessageProcessor = NULL;
    }
    
    
    if(_cEventAcquirer != NULL) {
        delete _cEventAcquirer;
        _cEventAcquirer = NULL;
    }
}

void EventAcquisitionModule::process() {
    _cMessageProcessor->process();
    _cEventAcquirer->process();
}

void EventAcquisitionModule::onExitMessage() {
    LOG(INFO) << "Exiting EventAcquirer!";
    _cCurrentState = EXIT;
    this->setShouldExit();
}

void EventAcquisitionModule::onPauseMessage(){
    LOG(INFO) << "Pausing EventAcquirer!";  
    _cCurrentState = PAUSE;
}

void EventAcquisitionModule::onStartMessage(){
    LOG(INFO) << "Starting EventAcquirer!";
    _cCurrentState = START;
}

void EventAcquisitionModule::onStopMessage(){
    LOG(INFO) << "Stopping DataReader!";   
    _cCurrentState = STOP;
}

StateMessage EventAcquisitionModule::getCurrentState() {
    return _cCurrentState;
}
