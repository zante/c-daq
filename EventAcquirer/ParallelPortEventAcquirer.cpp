/* 
 * File:   ParallelPortEventAcquirer.cpp
 * Author: lemelino
 * 
 * Created on January 2, 2015, 11:15 PM
 */

#include <iomanip>
#include <bitset>

#include "ParallelPortEventAcquirer.h"
#include "../Common/ConfigurationManager.h"

ParallelPortEventAcquirer::ParallelPortEventAcquirer(zmq::context_t& cZmqContext,
        const std::string& cEventSocketAddress) :
EventAcquirer::EventAcquirer(cZmqContext, cEventSocketAddress) {
    
    _baseAddress = 0;
    _cNumberWasStableForXFrames = 0;
    _cEvaluatingCode = 0;

    _NB_STABILIZATION_FRAMES = ConfigurationManager::getInstance()->
            getInt("PARALLEL_PORT_NB_STABILIZATION_FRAMES");
    
    LOG(INFO) << "Number of stabilization frames: " << 
            _NB_STABILIZATION_FRAMES << ".";
    
    initAcquisitionDevice();
}

ParallelPortEventAcquirer::~ParallelPortEventAcquirer() {
    tearDownAcquisitionDevice();
}

void ParallelPortEventAcquirer::initAcquisitionDevice() {
    
    _baseAddress = ConfigurationManager::getInstance()->
            getInt("PARALLEL_PORT_BASE_ADDRESS");
    
    if(ConfigurationManager::getInstance()->
            contains(std::string("PARALLEL_PORT_VALUE_ADJUSTMENT"))) {
        
        _iValueAdjustment = ConfigurationManager::getInstance()->
            getInt("PARALLEL_PORT_VALUE_ADJUSTMENT");
    }
    else {
        _iValueAdjustment = 0;
    }
    
    LOG(INFO) << "Base address of parallel port: 0x" << 
            std::hex << _baseAddress;
    
    int result = ioperm(_baseAddress, 3, 1);
    if(result == -1) {
        LOG(FATAL) << "Error, this program requires super user privileges! "
                "Please run the command preceded by 'sudo ' (without the "
                "quotes).";
    }
    
    unsigned char data = inb(_baseAddress);
    _cCurrentCode = data;
    LOG(INFO) << "Initialized Parallel port. Initial reading: " << 
            (unsigned int)_cCurrentCode << ".";
    
}

void ParallelPortEventAcquirer::tearDownAcquisitionDevice() {
    
    int result = ioperm(_baseAddress, 3, 0);
    if(result == -1) {
        LOG(FATAL) << "Error releasing the parallel port.";
    }
}

bool ParallelPortEventAcquirer::hasNewCode() {
    
    char data = inb(_baseAddress);
    
    // Apply the value adjustment if required.
    if(_iValueAdjustment != 0) {
        data += (char) _iValueAdjustment;
    }
    
    if(data != _cCurrentCode) {
        
        if(data != _cEvaluatingCode) {
            
            LOG(INFO) << "Evaluating new number: " << (int)data;
            
            // Store the number in order to wait for it to stabilize.
            _cEvaluatingCode = data;
            
            // Reset the number to 0.
            _cNumberWasStableForXFrames = 0;
            
        }
        else if(_cNumberWasStableForXFrames >= _NB_STABILIZATION_FRAMES) {
    
            std::bitset<8> bytes(data);
            
            // We change the code, since it was stable for a long time.
            LOG(INFO) << "New code: " << bytes << " (" << (int)data << ")";
            
            _cCurrentCode = data;
            
            return true;
        }
        else {
            
            // We increment the number of frames the number was stable for.
            _cNumberWasStableForXFrames++;
            
            // DEBUG Print.
            //LOG(INFO) << (int)_cEvaluatingCode << " stayed stable (frame " << 
            //        _cNumberWasStableForXFrames << ").";
        }

    }
    
    return false;
}

int ParallelPortEventAcquirer::getNewCode() {
    return (int)_cCurrentCode;
}

