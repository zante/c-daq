/* 
 * File:   EventLogger.h
 * Author: lemelino
 *
 * Created on January 13, 2015, 4:29 PM
 */

#ifndef EVENTLOGGER_H
#define	EVENTLOGGER_H

#include "../Common/CommonDef.h"
#include <string>
#include <fstream>
#include <iostream>

class EventLogger {
public:
    EventLogger(const std::string& fileToOpen);
    EventLogger(const EventLogger& orig);
    virtual ~EventLogger();
    
    virtual void writeEvent(int eventCode, 
            NanosecondsTime timestampInNanoseconds) = 0;
    
protected:
    std::ofstream outFileStream;
};

#endif	/* EVENTLOGGER_H */

