/* 
 * File:   Transcoder.h
 * Author: lemelino
 *
 * Created on January 1, 2015, 7:31 PM
 */

#ifndef TRANSCODER_H
#define	TRANSCODER_H

class Transcoder {
public:
    
    /**
     * Function to transcode doubles to floats.
     * @param dataChunk Data Frame (double* buffer) to transcode.
     * @param nbElementsInBuffer Number of elements in buffer
     * @return Transcoded buffer.  User has responsibility of deallocating
     * the buffer.
     */
    static float* transcode(double* dataChunk, int nbElementsInBuffer);
};

#endif	/* TRANSCODER_H */

