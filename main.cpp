
#include <iostream>
#include <signal.h>
#include <thread>
#include <glog/logging.h>
#include <zmq.hpp>
#include <string>

#include "Common/ConfigurationManager.h"
#include "Controller/ControllerModule.h"
#include "DataWriter/DataWriterModule.h"
#include "DataReader/DataReaderModule.h"
#include "EventAcquirer/EventAcquisitionModule.h"
#include "EventWriter/EventWriterModule.h"
#include "Visualizer/VisualizerModule.h"
#include "Common/CommonDef.h"

int main(int argc, char** argv) {

    // Setup the google logging library to display all errors
    // on the console instead of inside log files.
    google::InitGoogleLogging(argv[0]);
    FLAGS_logtostderr = 1;
    FLAGS_stderrthreshold = 0;
    
    // Creates the ZMQ Context that will be reused in all other modules.
    zmq::context_t cZmqContext(1);
    
    //Creates the configuration manager.
    ConfigurationManager* cConfigManager = ConfigurationManager::getInstance();
    cConfigManager->getString("RECORDING_DIRECTORY");
    
    // Configuration for the nodes: this is the address on which all messaging
    // nodes shall bind or connect.
    std::string cMessagesSocketAddress(MESSAGE_SOCKET_ADDRESS);
    
    // Configuration for the nodes: this is the address on which all data processing
    // nodes shall bind or connect.
    std::string cDataSocketAddress(DATA_SOCKET_ADDRESS);
    
    // Configuration for the nodes: this is the address on which all external
    // event processing nodes shall bind or connect.
    std::string cEventSocketAddress(EVENTS_SOCKET_ADDRESS);
    
    // Initializes the Controller Module, then creates a thread that executes
    // the 'launch' function of the module.
    ControllerModule controller(cZmqContext, cMessagesSocketAddress);
    std::thread cmThread(&ControllerModule::launch, std::ref(controller));
    
    // Initializes the Data Reader Module, then creates a thread that executes
    // the 'launch' function of the module.
    DataReaderModule dataReader(cZmqContext, 
            cMessagesSocketAddress, 
            cDataSocketAddress);
    std::thread drmThread(&DataReaderModule::launch, std::ref(dataReader));
    
    // Initializes the Data Writer Module, then creates a thread that executes
    // the 'launch' function of the module.
    DataWriterModule dataWriter(cZmqContext, 
            cMessagesSocketAddress, 
            cDataSocketAddress);
    std::thread dwmThread(&DataWriterModule::launch, std::ref(dataWriter));
    
    EventAcquisitionModule eventAcquirer(cZmqContext, 
            cMessagesSocketAddress, 
            cEventSocketAddress);
    std::thread eamThread(&EventAcquisitionModule::launch, std::ref(eventAcquirer));
    
    EventWriterModule eventWriter(cZmqContext, 
            cMessagesSocketAddress, 
            cEventSocketAddress);
    std::thread ewmThread(&EventWriterModule::launch, std::ref(eventWriter));
    
    VisualizerModule visualizer(cZmqContext, 
            cMessagesSocketAddress, 
            cDataSocketAddress, 
            cEventSocketAddress);
    
    visualizer.launch();
    
    LOG(INFO) << "Waiting for Event Writer Module...";
    ewmThread.join();
    
    LOG(INFO) << "Waiting for Event Acquisition Module...";
    eamThread.join();
    
    LOG(INFO) << "Waiting for Data Writer Module...";
    dwmThread.join();
    
    LOG(INFO) << "Waiting for Data Reader Module...";
    drmThread.join();
    
    // Waits for the Module threads to terminate.
    LOG(INFO) << "Waiting for Controller Module...";
    cmThread.join();
       
    return 0;
}
