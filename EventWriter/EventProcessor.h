/* 
 * File:   DataProcessor.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:31 PM
 */

#ifndef EVENTPROCESSOR_H
#define	EVENTPROCESSOR_H

#include <zmq.hpp>
#include <string>

#include "EventLogger.h"
#include "../Common/Timestamper.h"
#include "../Common/MessageProcessor.h"

class EventProcessor {
public:
    /**
     * Constructor
     */
    EventProcessor(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress);
    
    /**
     * Copy Constructor.  Disabled.
     * @param orig
     */
    EventProcessor(const EventProcessor& orig);
    
    /**
     * Destructor
     */
    virtual ~EventProcessor();
    
    /**
     * Main function.  Receives and writes the data received by this
     * module.
     */
    void process();
    
    /**
     * Handles the change of state.
     * @param state New state to change to.
     */
    void handleStateChange(StateMessage state);
    
private:
    
    /**
     * Creates and connects the event pipe in order to receive data.
     */
    void setupEventPipe(const std::string& dataSocketAddress);
    
    /**
     * Creates a new file name according to the current time.
     * @return The new file name to create.
     */
    std::string generateFileName();
    
    /**
     * Verifies that the filesystem is in a correct state to write the
     * data to a new file.  Mostly, it verifies that there is enough space
     * on the disk to record for a long time (configurable).
     * @return true if the filesystem is OK. false otherwise.
     */
    bool isFilesystemOK(std::string fileNameToTest);
    
    /**
     * Creates an aptly named file and opens it for writing.
     * @return 
     */
    void openFile(std::string fileName);
    
    /**
     * Closes the file that was written to.
     * @return 
     */
    void closeFile();
    
    /**
     * Receive the incoming data chunks.
     */
    void receiveData();
    
    /**
     * Processes (writes) the received data chunks.
     */
    void processSingleEvent(int eventCode);
    
    /**
     * Function executed when we need to begin writing data.
     */
    void start();
    
    /**
     * Function executed when we need to pause the recording.
     */
    void pause();
    
    /**
     * Function executed when we need to stop writing data.
     */
    void stop();
    
    /**
     * Function executed when we need to save data (upon calling stop(), usually).
     */
    void finalizeResults();
    
    /**
     * ZMQ Socket used to receive data.
     */
    zmq::socket_t _cSubscriberSocket;
    
    int _iNumberOfEvents;
    
    bool _isRecording;
    
    Timestamper _timestamper;

    EventLogger* _eventLogger;
    
    std::string generatedFileName;
};

#endif	/* DATAPROCESSOR_H */

