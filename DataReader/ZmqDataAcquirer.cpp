/* 
 * File:   ZmqDataAcquirer.cpp
 * Author: root
 * 
 * Created on January 29, 2015, 4:16 PM
 */

#include <cstdlib>
#include <signal.h>
#include "ZmqDataAcquirer.h"
#include "zmq.hpp"
#include "../Common/ConfigurationManager.h"

ZmqDataAcquirer::ZmqDataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSinkAddress) :
_cDataSink(cZmqContext, ZMQ_PUB),
_cDataSource(cZmqContext, ZMQ_SUB){
    
    // Launch remote reader application.
    // TODO: Create the reader application.
    
    // Bind to the sink socket so that we can push data!
    _cDataSink.bind(cDataSinkAddress.c_str());
    
    // Begins listening on the source socket so that we can obtain data
    // from a remote application.
    _cDataSource.connect(REMOTE_ADDRESS);
    _cDataSource.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    
    // Obtains required configuration.
    
    _numberOfChannels = ConfigurationManager::getInstance()
            ->getInt("NUMBER_OF_CHANNELS");
    
    _samplesPerFrame = ConfigurationManager::getInstance()
            ->getInt("SAMPLES_PER_FRAME");
    
    _remoteApplicationPID = -1;
    startAcquisitionProcess(EXEC_PATH, REMOTE_ADDRESS);
}

bool ZmqDataAcquirer::startAcquisitionProcess(const std::string& execPath, 
        const std::string& socketAddress) {
    
    std::string launchStr(execPath + " " + socketAddress);
    LOG(INFO) << "Executing remote application : " << launchStr;

    int pid = fork();

    if ( pid == 0 ) {
            execl( execPath.c_str(), execPath.c_str(), socketAddress.c_str(),
                    (char *) 0);
    }
    
    _remoteApplicationPID = pid;
    LOG(INFO) << "PID : " << _remoteApplicationPID;
    
    if(_remoteApplicationPID == -1) {
        return false;
    } else {
        return true;
    }
}

ZmqDataAcquirer::~ZmqDataAcquirer() {
    if(_remoteApplicationPID != -1) {
        LOG(INFO) << "Terminating acquisition subprocess with PID " << 
                _remoteApplicationPID << "...";
        kill(_remoteApplicationPID, SIGTERM);
    }
}

void ZmqDataAcquirer::process() {
        
    // Allocates space to receive a data frame message.
    int nbElementsInBuffer = _numberOfChannels * _samplesPerFrame;
    
    // Calculates the size of the array in bytes from the number of elements.
    int sizeOfDataFrame = sizeof(double) * nbElementsInBuffer;
    
    // Allocates a new buffer.
    double* receivedBuffer = new double[nbElementsInBuffer];

    // Allocates message with given size.
    zmq::message_t msg(sizeOfDataFrame);
    
    // Tries to receive a message.
    bool hasMessage = _cDataSource.recv(&msg, ZMQ_NOBLOCK);
    
    // While there are messages to process...
    while(hasMessage) {
        _cDataSource.send(&msg, ZMQ_NOBLOCK);
        LOG(INFO) << "Relayed message!";
    }
    
    // Error testing.
    if( !hasMessage && errno == EAGAIN) {
        //Ignore, there was nothing to receive.
    }
    else if(!hasMessage){
        //We have encountered an error.
        throw std::string("Problem receiving data.");
    }
    
    // Deletes the buffer.
    delete[] receivedBuffer;
}


