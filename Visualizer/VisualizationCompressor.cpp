/* 
 * File:   VisualizationCompressor.cpp
 * Author: lemelino
 * 
 * Created on January 18, 2015, 4:25 PM
 */

#include <glog/logging.h>
#include <string.h>
#include "VisualizationCompressor.h"

float* VisualizationCompressor::compress(double* initialDataChunk,
        int nbChannels, 
        int nbElementsPerChannel, 
        int compressionRatio,
        int* toFillNumberOfChannels,
        int* toFillNumberOfElementsPerChannel) {
    
    int nbConversionsPerChannel = 
            nbElementsPerChannel / compressionRatio;
    
    float* toFillCompressedDataChunk = 
            new float[nbChannels * nbConversionsPerChannel];
            
    memset(toFillCompressedDataChunk, 0, nbChannels * nbConversionsPerChannel);
            
    *toFillNumberOfChannels = nbChannels;
    *toFillNumberOfElementsPerChannel = nbConversionsPerChannel;
    
    double* srcPtr = initialDataChunk;
    float* destPtr = toFillCompressedDataChunk;
    float* endLastPtr = &(toFillCompressedDataChunk[nbChannels * nbConversionsPerChannel]);
    
    while(destPtr <= endLastPtr) {
        
        // Add up everything together.
        double cumulativeResult = 0.0;
        for(int x = 0; x < compressionRatio; x++) {
            cumulativeResult += *(srcPtr + x);
        }
        
        // Divide the result by the number of elements.
        cumulativeResult /= (double)compressionRatio;
        
        // Write results
        *destPtr = cumulativeResult;
        
        // Increment pointer
        srcPtr += compressionRatio;
        destPtr ++;
    }
    
    return toFillCompressedDataChunk;
}

