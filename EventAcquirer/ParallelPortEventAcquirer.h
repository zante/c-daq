/* 
 * File:   ParallelPortEventAcquirer.h
 * Author: lemelino
 *
 * Created on January 2, 2015, 11:15 PM
 */

#ifndef PARALLELPORTEVENTACQUIRER_H
#define	PARALLELPORTEVENTACQUIRER_H

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/io.h>
#include <sys/ioctl.h>
#include <linux/ppdev.h>

#include "EventAcquirer.h"

//#define BASE_ADDRESS 0x378
//#define BASE_ADDRESS 0xEF00

class ParallelPortEventAcquirer : public EventAcquirer {
public:
    ParallelPortEventAcquirer(zmq::context_t& cZmqContext,
        const std::string& cEventSocketAddress);
    ParallelPortEventAcquirer(const ParallelPortEventAcquirer& orig);
    virtual ~ParallelPortEventAcquirer();
private:
    
    void initAcquisitionDevice();
    
    void tearDownAcquisitionDevice();

    bool hasNewCode();
    
    int getNewCode();
    
    
    size_t _baseAddress;
    
    int _iParPortFd;
    
    unsigned char _cCurrentCode;
    
    int _iValueAdjustment;
    
    // Evaluating a change of parallel port state
    
    unsigned char _cEvaluatingCode;
    
    int _cNumberWasStableForXFrames;
    
    int _NB_STABILIZATION_FRAMES;

};

#endif	/* PARALLELPORTEVENTACQUIRER_H */

