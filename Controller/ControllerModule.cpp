/* 
 * File:   ControllerModule.cpp
 * Author: lemelino
 * 
 * Created on November 15, 2014, 9:32 PM
 */

#include <QApplication>
#include <glog/logging.h>

#include "ControllerModule.h"
#include "QtWindow.h"

ControllerModule::ControllerModule(zmq::context_t& _cZmqContext, 
        std::string cSocketAddress) : 
        _cControllerCommunicator(new ControllerCommunicator(_cZmqContext, cSocketAddress))
{
}

ControllerModule::~ControllerModule() {
}

void ControllerModule::process() {
    // This is used to fake the parameters.
    int argc = 0;
    char** argv = 0;
    
    // Creates the GTK+ application.
    //Glib::RefPtr<Gtk::Application> _cApp = Gtk::Application::create(
    //        "ca.olivierlemelin.acquisition.controller");
    
    //Initializes the Controller Window.
    //ControllerWindow controllerWindow(_cControllerCommunicator);
    //_cApp->run(controllerWindow, argc, argv);
    
    QApplication app(argc, argv);
    app.setOrganizationName("Olivier Lemelin");
    app.setApplicationName("Controller");
    
    QtWindow controllerWindow(_cControllerCommunicator);
    
    app.exec();
    
    //If we reach this point, it is because the GUI has been terminated.
    setShouldExit();
}

