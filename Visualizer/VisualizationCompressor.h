/* 
 * File:   VisualizationCompressor.h
 * Author: lemelino
 *
 * Created on January 18, 2015, 4:25 PM
 */

#ifndef VISUALIZATIONCOMPRESSOR_H
#define	VISUALIZATIONCOMPRESSOR_H

class VisualizationCompressor {
public:
    static float* compress(double* initialDataChunk,
        int nbChannels, 
        int nbElementsPerChannel, 
        int compressionRatio,
        int* toFillNumberOfChannels,
        int* toFillNumberOfElementsPerChannel);
private:

};

#endif	/* VISUALIZATIONCOMPRESSOR_H */

