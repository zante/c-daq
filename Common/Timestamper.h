/* 
 * File:   Timestamper.h
 * Author: lemelino
 *
 * Created on January 11, 2015, 10:13 PM
 */

#ifndef TIMESTAMPER_H
#define	TIMESTAMPER_H

#include <sys/time.h>
#include "../Common/CommonDef.h"



enum TimestamperState {T_STARTED, T_STOPPED};

class Timestamper {
public:
    Timestamper();
    Timestamper(const Timestamper& orig);
    virtual ~Timestamper();
    
    void startTimer();
    void resetTimer();
    void stopTimer();
    
    bool isStarted();
    
    NanosecondsTime getCurrentNanoTime();
    
    static NanosecondsTime timespecToNanosecondsSinceStart(NanosecondsTime ts, 
            NanosecondsTime startTimeInNano);
    
    static NanosecondsTime timespecToNanoseconds(timespec& ts);
    
private:
    
    NanosecondsTime _startTimeTimeInNano;
    
    TimestamperState _timestamperState;
};

#endif	/* TIMESTAMPER_H */

