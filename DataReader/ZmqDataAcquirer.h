/* 
 * File:   ZmqDataAcquirer.h
 * Author: root
 *
 * Created on January 29, 2015, 4:16 PM
 */

#ifndef ZMQDATAACQUIRER_H
#define	ZMQDATAACQUIRER_H

#include "zmq.hpp"
#include "DataAcquirer.h"

#define REMOTE_ADDRESS "tcp://127.0.0.1:51040"
#define EXEC_PATH "./remote-nidaq-reader"

class ZmqDataAcquirer {
public:
    ZmqDataAcquirer(zmq::context_t& cZmqContext,
        const std::string& cDataSinkAddress);
    
    ZmqDataAcquirer(const ZmqDataAcquirer& orig);
    virtual ~ZmqDataAcquirer();

    /**
     * Main function.  Receives and writes the data received by this
     * module.
     */
    void process();
    
private:
    
    /**
     * Creates and connects the data pipe in order to receive data.
     */
    void setupDataAcquisition();
    
    /**
     * Prepares a buffer to transmit a single frame of acquired data.
     */
    double* prepareTransmissionBuffer();
    
    /**
     * Fills the transmission buffer.
     */
    void fillTransmissionBuffer(double* transmissionBuffer);
    
    /**
     * Sends the data frame.
     */
    void sendDataFrame(double* transmissionBuffer);
    
    bool startAcquisitionProcess(const std::string& execPath, 
        const std::string& socketAddress);
    
    /**
     * ZMQ Socket used to push data.
     */
    zmq::socket_t _cDataSink;
    
    /**
     * ZMQ Socket used to receive data.
     */
    zmq::socket_t _cDataSource;
    
    /**
     * Number of channels to receive from.
     */
    int _numberOfChannels;
    
    /**
     * Samples per second
     */
    int _samplesPerFrame;
    
    float _fCurrentFrameNumber;
    
    int _samplingRate;
    
    int _remoteApplicationPID;
};

#endif	/* ZMQDATAACQUIRER_H */

