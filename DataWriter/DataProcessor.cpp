/* 
 * File:   DataProcessor.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:31 PM
 */

#include <glog/logging.h>
#include <sys/statvfs.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <random>
#include <errno.h>
#include <iostream>

#include "../Common/BaseCommunicatingModule.h"
#include "../Common/ConfigurationManager.h"
#include "../Common/CommonDef.h"
#include "DataAccumulator.h"
#include "DataProcessor.h"
#include "MatlabWriter.h"
#include "Transcoder.h"
#include "DumpWriter.h"

DataProcessor::DataProcessor(zmq::context_t& cZmqContext,
        const std::string& cDataSocketAddress) :
_cSubscriberSocket(cZmqContext, ZMQ_SUB)
{    
    _dataAccumulator = NULL;
    _isRecording = false;
    _iNumberOfChannels = ConfigurationManager::getInstance()
            ->getInt("NUMBER_OF_CHANNELS");
    _iSamplesPerFrame = ConfigurationManager::getInstance()
            ->getInt("SAMPLES_PER_FRAME");
    
    setupDataPipe(cDataSocketAddress);
}

DataProcessor::~DataProcessor() {
    stop();
}

void DataProcessor::process() {
    
    // Allocates space to receive a data frame message.
    int nbElementsInBuffer = _iNumberOfChannels * _iSamplesPerFrame;
    
    // Calculates the size of the array in bytes from the number of elements.
    int sizeOfDataFrame = sizeof(double) * nbElementsInBuffer;
    
    double* receivedBuffer = new double[nbElementsInBuffer];

    // Allocates message with given size.
    zmq::message_t msg(sizeOfDataFrame);
    
    // Tries to receive a message.
    bool hasMessage = _cSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    
    // While there are messages to process...
    while(hasMessage) {
        
        // Copy the contents of the message to a buffer.
        memcpy((void*)receivedBuffer, msg.data(), sizeOfDataFrame);
        
        // Process the received contents.
        processDataFrame(receivedBuffer, nbElementsInBuffer);
        
        // Tries to receive a new message.
        hasMessage = _cSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    }
    
    // Error testing.
    if( !hasMessage && errno == EAGAIN) {
        //Ignore, there was nothing to receive.
    }
    else if(!hasMessage){
        //We have encountered an error.
        throw std::string("Problem receiving data.");
    }
    
    // Deletes the buffer.
    delete[] receivedBuffer;
}

void DataProcessor::setupDataPipe(std::string dataSocketAddress) {
    
    // Sets the subscription filter.
    
    _cSubscriberSocket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    
    // Connect to the Publisher.    
    _cSubscriberSocket.connect(dataSocketAddress.c_str());
}

bool DataProcessor::isFilesystemOK(std::string fileNameToTest) {

    // Gets the recordings directory.
    std::string recordingDir = ConfigurationManager::getInstance()->
            getString("RECORDING_DIRECTORY");
    
    int nbChannels = ConfigurationManager::getInstance()->
            getInt("NUMBER_OF_CHANNELS");
    
    int nbBytesPerPoint = NUMBER_OF_BYTES_PER_DATA_POINT;
    
    int samplesPerSecond = ConfigurationManager::getInstance()->
            getInt("SAMPLES_PER_SECOND");
    
    int secondsPerMinute = SECONDS_PER_MINUTE;
    
    int maxNumberOfMinutesOfRecording = ConfigurationManager::getInstance()->
            getInt("MAX_NUMBER_OF_MINUTES_OF_RECORDING");

    
    // Verifies that the recording directory exists.
    if(access(recordingDir.c_str(), F_OK ) != 0) {
        LOG(FATAL) << "Error: recording directory '" << recordingDir << "' does "
                "not exist.  It will need to be created manually.";
        return false;
    } 
    
    //Verifies that the directory is writeable.
    if(access(recordingDir.c_str(), R_OK | W_OK) != 0) {
        LOG(FATAL) << "Error: recording directory '" << recordingDir << "' is "
                "not writeable.  Permissions will need to be adjusted.";
        return false;
    }
    
    // Calculates required space and remaining space.
    unsigned long spaceRequiredInBytes = (unsigned long)nbBytesPerPoint * 
            (unsigned long)nbChannels *
            (unsigned long)samplesPerSecond * 
            (unsigned long)secondsPerMinute * 
            (unsigned long)maxNumberOfMinutesOfRecording;
    
    LOG(INFO) << "Will require " << (int)spaceRequiredInBytes << " bytes (" <<
            spaceRequiredInBytes / 1000000 << " MB)";
    
    // Gets filesystem information.
    struct statvfs st;
    if(statvfs(recordingDir.c_str(), &st) != 0) {
        LOG(FATAL) << "Could not obtain filesystem information from " << 
                recordingDir << ".  Errno: " << errno;
    }
    
    // Calculates free space on device.
    unsigned long freeSpaceInBytes = st.f_bsize * (unsigned long)st.f_bfree;
    
    LOG(INFO) << "Remaining space on device: " << freeSpaceInBytes 
            << " bytes (" << freeSpaceInBytes / 1000000 << " MB)";
    
    // Make sure that enough space remains available.
    if(freeSpaceInBytes < (unsigned long)spaceRequiredInBytes) {
        LOG(FATAL) << "Not enough space left for " << 
                maxNumberOfMinutesOfRecording << " minutes of recording. " <<
                "Exiting.";
        return false;
    }  
    
    // Checks to see if file name already exists.
    if( access( fileNameToTest.c_str(), F_OK ) != -1 ) {
        
        //File already exists!
        return false;
        
    } else {
        
        // File does not already exist, OK to create!
        return true;
    }
}

void DataProcessor::processDataFrame(double* dataChunk, 
        int nbElementsInBuffer) {

    // We only process the data frame if we are recording and the data 
    // accumulator exists!
    if(_isRecording  && _dataAccumulator != NULL) {
        
#ifdef SHOULD_TRANSCODE_TO_FLOAT
        
        // Transcode the message from doubles to floats to save some memory.
        float* transcoded = Transcoder::transcode(dataChunk, nbElementsInBuffer);
        
        // Here, we try to append a frame to the data accumulator.
        // It will return true if the operation was successful, and false if
        // the buffer has been filled.
        // If false is returned, the current recording is stopped, saved,
        // and then restarted in order to lose as little data as possible.
        
        if(!_dataAccumulator->appendDataFrame(transcoded, nbElementsInBuffer)) {
            // Restarting recording.
            LOG(INFO) << "Restarting the recording. Thus, this will be a multipart"
                    "recording that you will have to merge manually.";
            stop();
            start();
        }
        delete[] transcoded;
#else
        if(!_dataAccumulator->appendDataFrame(dataChunk, nbElementsInBuffer)) {
            // Restarting recording.
            LOG(INFO) << "Restarting the recording. Thus, this will be a multipart"
                    "recording that you will have to merge manually.";
            stop();
            start();
        }
#endif
    }
}

void DataProcessor::handleStateChange(StateMessage state) {
    switch(state) {
        case START:
            start();
            break;
        case PAUSE:
            _isRecording = false;
            break;
        case STOP:
            stop();
            break;
        case EXIT:
            stop();
            break;
        default:
            LOG(FATAL) << "Impossible state.";
    }
}

std::string DataProcessor::generateFileName() {
    
    // Obtains the local time (now)
    time_t t = time(NULL);
    struct tm *tmptr = localtime(&t);
    
    // Verify that the time was obtained correctly.
    if (tmptr == NULL)
    {
       LOG(FATAL) << "localtime() failed.";
    }
    
    // Seeds the RNG.
    srand(t);
    
    // Allocates objects to build name
    std::stringstream generatedFilename;
    const int MAX_LENGTH = 80;
    char buffer[MAX_LENGTH];
    
    // Builds name
    strftime(buffer, MAX_LENGTH, FILE_NAME_FORMAT, tmptr);
    generatedFilename << "recording_" << buffer << "_" << rand() % 100 << ".mat";
    std::string completeGeneratedFilename = generatedFilename.str();
    
    LOG(INFO) << "Generated file name for recording: '" << 
            completeGeneratedFilename << "'.";
    
    return completeGeneratedFilename;
}

void DataProcessor::saveResults() {
    
    // To save the results, the data accumulator must exist.  Otherwise,
    // we skip, as there is no data to save.
    if(_dataAccumulator != NULL) {

        // Variables we allocate to fill with data from the next function call.
        unsigned int nbChannels = 0;
        unsigned long nbElementsPerChannel = 0;
        
        LOG(INFO) << "Saving results...";
        
#ifdef SHOULD_TRANSCODE_TO_FLOAT
        float* dataMatrix = _dataAccumulator->retrieveAllDataAsArray (
                &nbChannels, &nbElementsPerChannel);
#else
        double* dataMatrix = _dataAccumulator->retrieveAllDataAsArray (
                &nbChannels, &nbElementsPerChannel);
#endif
         
#ifdef TESTING_MODE
        
        //LOG(INFO) << "FAKING DATA WRITE.  NOT FOR PRODUCTION USE.";
        MatlabWriter::saveResults(generatedFileName, dataMatrix, 
                nbChannels, nbElementsPerChannel);
        DumpWriter::saveResults(generatedFileName + ".dump", dataMatrix, 
                nbChannels, nbElementsPerChannel);
#else
                
        MatlabWriter::saveResults(generatedFileName, dataMatrix, 
                nbChannels, nbElementsPerChannel);
#endif
    
        LOG(INFO) << "Done!";
        
        // Deletes the buffer.
        delete[] dataMatrix;
        
    } else {
        LOG(INFO) << "No data to save.";
    }
}

void DataProcessor::start() {
    
    // Since with are beginning to record, we set the flag to true.
    _isRecording = true;
    
    // We generate a name for the new file.
    generatedFileName = generateFileName();
    
    // We check that the filesystem is able to receive the new file:
    // Enough space on disk, file does not already exist, directory is writeable...
    if(!isFilesystemOK(generatedFileName)) {
        LOG(FATAL) << "Error with filesystem: file with same name probably "
                "exists."; 
    }
    
    // If the data accumulator does not already exist, we create it.
    if(_dataAccumulator == NULL) {
        
        LOG(INFO) << "Trying to create new Data Accumulator...";
        
        int nbChannels = ConfigurationManager::getInstance()->
        getInt("NUMBER_OF_CHANNELS");
    
        int samplesPerSecond = ConfigurationManager::getInstance()->
                getInt("SAMPLES_PER_SECOND");

        int samplesPerFramePerChannel = ConfigurationManager::getInstance()->
                getInt("SAMPLES_PER_FRAME");
        
        int samplesPerFrame = samplesPerFramePerChannel * nbChannels;

        int secondsPerMinute = SECONDS_PER_MINUTE;

        int maxNumberOfMinutesOfRecording = ConfigurationManager::getInstance()->
                getInt("MAX_NUMBER_OF_MINUTES_OF_RECORDING");
        
        // We calculate the length (in nb elements) of each channel.
        unsigned long maxNumberOfSamplesPossible =
                (unsigned long)samplesPerSecond * 
                (unsigned long)secondsPerMinute * 
                (unsigned long)maxNumberOfMinutesOfRecording;
        
#ifdef SHOULD_TRANSCODE_TO_FLOAT
        
        // Properly create the data accumulator.
        _dataAccumulator = new DataAccumulator<float>(nbChannels, 
                maxNumberOfSamplesPossible, 
                samplesPerFrame);
#else
        _dataAccumulator = new DataAccumulator<double>(nbChannels, 
                    maxNumberOfSamplesPossible, 
                    samplesPerFrame);
#endif 
        if(_dataAccumulator == 0) {
            LOG(FATAL) << "Could not create the data accumulator.";
        }
        
        LOG(INFO) << "Success!";
    }
}

void DataProcessor::pause() {
    // We stop recording, but do not delete the data accumulator, as we will
    // continue recording later.
    _isRecording = false;
}

void DataProcessor::stop() {
    // We set the recording flag to false to stop handling received frames.
    _isRecording = false;
    
    //We save the recording session.
    saveResults();
    
    // We delete the data accumulator, as it is not needed anymore.
    LOG(INFO) << "Trying to delete Data Accumulator...";
    if(_dataAccumulator != NULL) {
        delete _dataAccumulator;
        _dataAccumulator = NULL;
        LOG(INFO) << "Done!";
    }
}
