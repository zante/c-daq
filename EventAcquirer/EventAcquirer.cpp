/* 
 * File:   EventAcquirer.cpp
 * Author: lemelino
 * 
 * Created on January 2, 2015, 10:51 PM
 */

#include <glog/logging.h>
#include "EventAcquirer.h"

EventAcquirer::EventAcquirer(zmq::context_t& cZmqContext,
        const std::string& cEventSocketAddress) :
_cEventPublisherSocket(cZmqContext, ZMQ_PUB) {
        
    // Bind to the data socket.
    
    _cEventPublisherSocket.bind(cEventSocketAddress.c_str());
}

void EventAcquirer::process() {
    
    if(hasNewCode()) {
        int newCode = getNewCode();
        sendEventCode(newCode);
    }
}

EventAcquirer::~EventAcquirer() {
}

void EventAcquirer::sendEventCode(int code) {
    
    LOG(INFO) << "Sending event with code <" << code << ">...";
    
    // Creates the message, reserving enough space for the integer.
    
    zmq::message_t msg((std::size_t) sizeof(int));
    
    // Copies the integer inside the state message.
    
    memcpy(msg.data(), (void*)(&code), (std::size_t) sizeof(int));
    
    // Send away!
    
    _cEventPublisherSocket.send(msg);
}
