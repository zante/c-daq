/* 
 * File:   ComediWrapper.h
 * Author: acquisition
 *
 * Created on March 19, 2015, 3:03 PM
 */

#ifndef COMEDIWRAPPER_H
#define	COMEDIWRAPPER_H

#include <string>
#include <comedilib.h>

namespace Comedi {
    
    std::string startSrcToString(unsigned int src);
    std::string scanBeginSrcToString(unsigned int src);
    std::string convertSrcToString(unsigned int src);
    std::string scanEndSrcToString(unsigned int src);
    std::string stopSrcToString(unsigned int src);
    std::string flagsToString(unsigned int flags);
    
    comedi_t* openDevice(std::string& comediDevicePath);
    void closeDevice(comedi_t* deviceToClose);
    
    void setClippedDataBehaviour();
    
    int getSubdevice(comedi_t* device);
    unsigned int* getChannelList(const int baseChannel, 
        const int numberOfChannels, const int referenceLevel, const int range);
    comedi_range* getChannelRangeInfo(comedi_t* comediDevice, 
        const int comediSubDevice, const int baseChannel, const int numberOfChannels, const int range);
    lsampl_t* getChannelMaximums(comedi_t* comediDevice, 
        const int comediSubDevice, const int baseChannel, const int numberOfChannels);
    
    comedi_cmd* prepareAcquisitionCommand(comedi_t* comediDevice, unsigned int subdevice, 
        unsigned int nbChannels, unsigned int* channelList, 
        unsigned int scanPeriodInNanoseconds);
    
    bool testCommand(comedi_t* comediDevice, comedi_cmd* command);
    bool launchCommand(comedi_t* comediDevice, comedi_cmd* command);
    void stopCommand(comedi_t* device, int subdevice);
    
    void setNonBlocking(comedi_t* comediDevice);
    
    size_t getBytesPerSample(comedi_t* comediDevice, int subdevice);
    unsigned int readAcquiredFrame(comedi_t* comediDevice, size_t bufferSize, size_t bytesPerSample, double* finalBuffer);
    //bool hasDataAvailable(comedi_t* comediDevice, int subdevice, size_t frameSizeInBytes);
    
    bool processAvailableData(comedi_t* comediDevice, int subdevice, 
            size_t frameSizeInBytes, comedi_range* range_info, lsampl_t* channelMaximums, 
            double* frameBuffer, int expectedSamplesRead);
    
    void dump_command(comedi_cmd* startCommand);
}

#endif	/* COMEDIWRAPPER_H */

