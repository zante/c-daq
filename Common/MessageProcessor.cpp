/* 
 * File:   MessageProcessor.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:31 PM
 */

#include <glog/logging.h>
#include <string>

#include "MessageProcessor.h"

MessageProcessor::MessageProcessor(BaseCommunicatingModule& moduleRef, 
            zmq::context_t& cZmqContext, 
            const std::string& cSocketAddress) : 
    _cModuleRef(moduleRef),
    _cSubscriberSocket(cZmqContext, ZMQ_SUB)
{
    // Sets the subscription filter.
    
    _cSubscriberSocket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    
    // Connect to the Publisher.
    
    _cSubscriberSocket.connect(cSocketAddress.c_str());
}

MessageProcessor::~MessageProcessor() {
}

void MessageProcessor::process() {

    // Allocates space to receive a State message.
    zmq::message_t msg(sizeof(StateMessage));
    StateMessage stateMsg = START;
    
    // Tries to receive a message.
    
    bool hasMessage = _cSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    
    // While there are messages to process...
    
    while(hasMessage) {
        
        // Copy the contents of the message to a buffer.

        memcpy((void*)&stateMsg, msg.data(), 1);
        
        // Process the received contents.

        processMessage(stateMsg);
        
        // Tries to receive a new message.
        
        hasMessage = _cSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
    }
    
    // Error testing.
    
    if( !hasMessage && errno == EAGAIN) {
        //Ignore, there was nothing to receive.
    }
    else if(!hasMessage){
        //We have encountered an error.
        throw std::string("Problem receiving data.");
    }
}

void MessageProcessor::processMessage(StateMessage stateMsg) {
    
    // Launches the right procedure depending on the received message.
    
    switch(stateMsg){
        case EXIT:
            _cModuleRef.onExitMessage();
            break;
        case PAUSE:
            _cModuleRef.onPauseMessage();
            break;
        case STOP:
            _cModuleRef.onStopMessage();
            break;
        case START:
            _cModuleRef.onStartMessage();
            break;
        case SCALING:
            
            // Receive next message
            zmq::message_t msg(sizeof(StateMessage));
            bool hasMessage = _cSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
            while(!hasMessage) {
                hasMessage = _cSubscriberSocket.recv(&msg, ZMQ_NOBLOCK);
            }
            
            memcpy((void*)&stateMsg, msg.data(), 1);
            //TODO
            
            
            _cModuleRef.onScalingMessage();
        default:
            throw std::string("Not a state message: cannot be processed!");
    }
}

