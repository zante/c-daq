/* 
 * File:   QtWindow.cpp
 * Author: root
 * 
 * Created on January 31, 2015, 2:57 PM
 */

#include <glog/logging.h>
#include "QtWindow.h"
#include <QString>
#include <sstream>

QtWindow::QtWindow(std::shared_ptr<ControllerCommunicator> controllerCommunicator) {
    
    _cControllerCommunicator = controllerCommunicator;
    _cControllerState = STOPPED;
    
    this->setGeometry(100, 100, 160, 160);
    this->setFixedSize(180, 160);
    
    _startButton = new QPushButton("Start", this);
    _startButton->setGeometry(10, 10, 80, 30);
    
    _stopButton = new QPushButton("Stop", this);
    _stopButton->setGeometry(10, 40, 80, 30);
    
    _stateLabel = new QLabel(this);
    _stateLabel->setText("Not Recording");
    _stateLabel->setGeometry(10, 120, 100, 30);
    
    _scaleLabel = new QLabel(this);
    _scaleLabel->setText("Scale: 100%");
    _scaleLabel->setGeometry(10, 75, 100, 30);
    
    _scaleSlider = new QSlider(Qt::Horizontal, this);
    _scaleSlider->setTickInterval(30);
    _scaleSlider->setGeometry(10, 100, 100, 20);
    _scaleSlider->setTickPosition(QSlider::TicksBelow);
    _scaleSlider->setMaximum(300);
    _scaleSlider->setMinimum(75);
    _scaleSlider->setValue(100);
    
    QPalette sample_palette;
    sample_palette.setColor(QPalette::WindowText, Qt::red);
    _stateLabel->setPalette(sample_palette);
    
    connect(_startButton, SIGNAL(released()), this, SLOT(on_startButton_clicked()));
    connect(_stopButton, SIGNAL(released()), this, SLOT(on_stopButton_clicked()));
    connect(_scaleSlider, SIGNAL(valueChanged(int)), this, SLOT(on_scaleSlider_changed()));
    
    this->show();
}

QtWindow::~QtWindow() {
    delete _scaleSlider;
    delete _stateLabel;
    delete _startButton;
    delete _stopButton;
    
    LOG(INFO) << "Close button has been clicked!  Quitting!";
    _cControllerCommunicator->sendExitMessage();
}

void QtWindow::on_startButton_clicked() {
    if(changeState(RECORDING)) {
        LOG(INFO) << "Start button clicked.";
        _cControllerCommunicator->sendStartMessage();
    }
}

void QtWindow::on_stopButton_clicked() {
    if(changeState(STOPPED)) {
        LOG(INFO) << "Stop button clicked.";
        _cControllerCommunicator->sendStopMessage();
    }
}

void QtWindow::on_scaleSlider_changed() {
    std::stringstream ss;
    ss << "Scale: " << _scaleSlider->value() << '%';
    QString sst(ss.str().c_str());
    _scaleLabel->setText(sst);
    
    _cControllerCommunicator->sendScaleMessage(_scaleSlider->value());
}

bool QtWindow::canChangeState(ControllerState newState) {
    switch(_cControllerState) {
        case STOPPED:
            return (newState == RECORDING) ? true : false;
            break;
        case PAUSED:
            return (newState == STOPPED || newState == RECORDING) ? true : false;
            break;
        case RECORDING:
            return (newState == STOPPED || newState == PAUSED) ? true : false;
            break;
        default:
            throw std::string("Wrong controller state!");
            break;
    }
}

bool QtWindow::changeState(ControllerState newState) {
    
    if(canChangeState(newState)) {
        QPalette sample_palette;
        switch(newState) {
            case STOPPED:
                sample_palette.setColor(QPalette::WindowText, Qt::red);
                _stateLabel->setPalette(sample_palette);
                _stateLabel->setText("Not recording");
                _cControllerState = STOPPED;
                break;
            case PAUSED:
                sample_palette.setColor(QPalette::WindowText, Qt::blue);
                _stateLabel->setPalette(sample_palette);
                _stateLabel->setText("Paused");
                _cControllerState = PAUSED;
                break;
            case RECORDING:
                sample_palette.setColor(QPalette::WindowText, Qt::green);
                _stateLabel->setPalette(sample_palette);
                _stateLabel->setText("Recording");
                _cControllerState = RECORDING;
                break;
            default:
                throw std::string("Wrong controller state!");
                break;
        } 
        
        return true;
        
    } else {
        LOG(INFO) << "Cannot change from state " << _cControllerState <<
                " to state " << newState << ".";
        
        return false;
    }
}
