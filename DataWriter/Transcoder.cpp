/* 
 * File:   Transcoder.cpp
 * Author: lemelino
 * 
 * Created on January 1, 2015, 7:31 PM
 */

#include "Transcoder.h"

float* Transcoder::transcode(double* dataChunk, int nbElementsInBuffer) {
    float* transcoded = new float[nbElementsInBuffer];
    for(int i = 0; i < nbElementsInBuffer; i++) {
        transcoded[i] = dataChunk[i];
    }
    
    return transcoded;
}

