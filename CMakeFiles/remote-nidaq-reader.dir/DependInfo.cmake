# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/acquisition/Desktop/c-daq/Common/BaseCommunicatingModule.cpp" "/home/acquisition/Desktop/c-daq/CMakeFiles/remote-nidaq-reader.dir/Common/BaseCommunicatingModule.cpp.o"
  "/home/acquisition/Desktop/c-daq/Common/BaseModule.cpp" "/home/acquisition/Desktop/c-daq/CMakeFiles/remote-nidaq-reader.dir/Common/BaseModule.cpp.o"
  "/home/acquisition/Desktop/c-daq/Common/ConfigurationManager.cpp" "/home/acquisition/Desktop/c-daq/CMakeFiles/remote-nidaq-reader.dir/Common/ConfigurationManager.cpp.o"
  "/home/acquisition/Desktop/c-daq/Common/MessageProcessor.cpp" "/home/acquisition/Desktop/c-daq/CMakeFiles/remote-nidaq-reader.dir/Common/MessageProcessor.cpp.o"
  "/home/acquisition/Desktop/c-daq/remote-nidaq-reader_automoc.cpp" "/home/acquisition/Desktop/c-daq/CMakeFiles/remote-nidaq-reader.dir/remote-nidaq-reader_automoc.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "include"
  "includes"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
