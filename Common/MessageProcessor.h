/* 
 * File:   MessageProcessor.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:31 PM
 */

#ifndef MESSAGEPROCESSOR_H
#define	MESSAGEPROCESSOR_H

#include <functional>
#include <string>
#include <zmq.hpp>

#include "BaseCommunicatingModule.h"
#include "../Common/MessageDef.h"

/**
 * This class is used to receive and process the control messages
 * used to tell what state the application should be in.
 * 
 * All modules except the Controller should contain an instance of this class,
 * as it is the only thing that allows us to terminate the program.
 */
class MessageProcessor {
public:
    /**
     * Constructor of the message processor.
     * @param moduleRef  Reference to the containing module, whish is used to
     * manipulate the module's state.
     * 
     * @param cZmqContext ZMQ Context used for the communication sockets.
     * 
     * @param cSocketAddress String representing the address socket.
     */
    MessageProcessor(BaseCommunicatingModule& moduleRef, 
            zmq::context_t& cZmqContext, 
            const std::string& cSocketAddress);
    
    /**
     * Disabled copy constructor
     * @param orig
     */
    MessageProcessor(const MessageProcessor& orig);
    
    /**
     * Destructor
     */
    virtual ~MessageProcessor();
    
    /**
     * Main loop of this module.  Executed to receive and process the
     * incoming messages.
     */
    void process();
    
private:
    
    /**
     * Receives and processes a single message.
     */
    void processMessage(StateMessage stateMsg);
    
    /**
     * Reference to the containing module, used to change the state upon
     * receiving a message.
     */
    BaseCommunicatingModule& _cModuleRef;
    
    /**
     * ZMQ Socket used to receive messages.
     */
    zmq::socket_t _cSubscriberSocket;
};

#endif	/* MESSAGEPROCESSOR_H */

