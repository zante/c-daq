/* 
 * File:   DataWriter.cpp
 * Author: lemelino
 * 
 * Created on November 14, 2014, 11:20 PM
 */

#include <glog/logging.h>
#include "EventWriterModule.h"

EventWriterModule::EventWriterModule(zmq::context_t& cZmqContext, 
        const std::string& cMessageSocketAddress, const std::string& cDataSocketAddress) {
    
    _cCurrentState = STOP;
    
    //TODO: Re-enable!
    _cEventProcessor = new EventProcessor(cZmqContext, cDataSocketAddress);
    _cMessageProcessor = new MessageProcessor(*this, cZmqContext, cMessageSocketAddress);
}

EventWriterModule::EventWriterModule(const EventWriterModule& orig) {
}

EventWriterModule::~EventWriterModule() {
    
    if(_cEventProcessor != NULL) {
        delete _cEventProcessor;
        _cEventProcessor = NULL;
    }
    
    if(_cMessageProcessor != NULL) {
        delete _cMessageProcessor;
        _cMessageProcessor = NULL;
    }
}

void EventWriterModule::process() {
    
    _cEventProcessor->process();
    _cMessageProcessor->process();
}

void EventWriterModule::onExitMessage() {
    LOG(INFO) << "Exiting EventWriter!";
    _cEventProcessor->handleStateChange(EXIT);
    _cCurrentState = EXIT;
    this->setShouldExit();
}

void EventWriterModule::onPauseMessage(){
    _cEventProcessor->handleStateChange(PAUSE);
    _cCurrentState = PAUSE;
    LOG(INFO) << "Pausing EventWriter!";    
}

void EventWriterModule::onStartMessage(){
    _cEventProcessor->handleStateChange(START);
    _cCurrentState = START;
    LOG(INFO) << "Starting EventWriter!";    
}

void EventWriterModule::onStopMessage(){
    _cEventProcessor->handleStateChange(STOP);
    _cCurrentState = STOP;
    LOG(INFO) << "Stopping EventWriter!";    
}

StateMessage EventWriterModule::getCurrentState() {
    return _cCurrentState;
}

