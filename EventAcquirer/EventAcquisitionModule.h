/* 
 * File:   DataWriter.h
 * Author: lemelino
 *
 * Created on November 14, 2014, 11:20 PM
 */

#ifndef EVENTACQUISITIONMODULE_H_
#define	EVENTACQUISITIONMODULE_H_

#include "../Common/BaseCommunicatingModule.h"
#include "../Common/MessageProcessor.h"
#include "EventAcquirer.h"

class EventAcquisitionModule : public BaseCommunicatingModule {
public:
    
    /**
     * Constructor
     */
    EventAcquisitionModule(zmq::context_t& cZmqContext, const std::string& cMessagesSocketAddress, 
            const std::string& cEventsSocketAddress);
    
    /**
     * Disabled copy constructor.
     * @param orig
     */
    EventAcquisitionModule(const EventAcquisitionModule& orig);
    
    /**
     * Destructor.
     */
    virtual ~EventAcquisitionModule();
    
    
    void onExitMessage();
    void onPauseMessage();
    void onStartMessage();
    void onStopMessage();
    
    StateMessage getCurrentState();

private:
    
    /**
     * Processes the data that was received and writes it and processes
     * control messages if any.  This is done by calling the composing objects
     * in order to have them process their own things.
     */
    void process();

    // TODO: Event Acquirer (Base class)
    
    /**
     * The module that processes the incoming messages.
     */
    MessageProcessor* _cMessageProcessor;
    
    /**
     * Current module's state.
     */
    StateMessage _cCurrentState;
    
    EventAcquirer* _cEventAcquirer;
};

#endif	/* EVENTACQUISITIONMODULE_H_ */

